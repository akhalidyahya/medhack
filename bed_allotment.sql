-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table medhack.bed
CREATE TABLE IF NOT EXISTS `bed` (
  `bed_id` int(11) NOT NULL AUTO_INCREMENT,
  `bed_number` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`bed_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table medhack.bed: 3 rows
/*!40000 ALTER TABLE `bed` DISABLE KEYS */;
INSERT INTO `bed` (`bed_id`, `bed_number`, `type`, `status`, `description`) VALUES
	(3, 'A02', 1, 'AVAILABLE', 'ruangan kedua');
INSERT INTO `bed` (`bed_id`, `bed_number`, `type`, `status`, `description`) VALUES
	(2, 'A01', 1, 'AVAILABLE', 'asd');
INSERT INTO `bed` (`bed_id`, `bed_number`, `type`, `status`, `description`) VALUES
	(4, 'A03', 1, 'AVAILABLE', 'Deskripsi');
/*!40000 ALTER TABLE `bed` ENABLE KEYS */;

-- Dumping structure for table medhack.bed_allotment
CREATE TABLE IF NOT EXISTS `bed_allotment` (
  `bed_allotment_id` int(11) NOT NULL AUTO_INCREMENT,
  `bed_id` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `allotment_timestamp` datetime DEFAULT NULL,
  `discharge_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`bed_allotment_id`) USING BTREE,
  KEY `idx_bed_id` (`bed_id`),
  KEY `idx_patient_id` (`patient_id`),
  KEY `idx_allotment_time` (`allotment_timestamp`),
  KEY `idx_discharge_time` (`discharge_timestamp`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table medhack.bed_allotment: 3 rows
/*!40000 ALTER TABLE `bed_allotment` DISABLE KEYS */;
INSERT INTO `bed_allotment` (`bed_allotment_id`, `bed_id`, `patient_id`, `allotment_timestamp`, `discharge_timestamp`) VALUES
	(3, 2, 3, '2021-07-01 01:01:00', '2021-07-03 01:02:00');
INSERT INTO `bed_allotment` (`bed_allotment_id`, `bed_id`, `patient_id`, `allotment_timestamp`, `discharge_timestamp`) VALUES
	(2, 3, 1, '2021-07-13 18:45:00', '2021-07-17 19:30:00');
INSERT INTO `bed_allotment` (`bed_allotment_id`, `bed_id`, `patient_id`, `allotment_timestamp`, `discharge_timestamp`) VALUES
	(4, 4, 4, '2021-07-15 03:00:00', '2021-07-18 08:00:00');
/*!40000 ALTER TABLE `bed_allotment` ENABLE KEYS */;

-- Dumping structure for table medhack.bed_types
CREATE TABLE IF NOT EXISTS `bed_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '0',
  `key` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table medhack.bed_types: ~1 rows (approximately)
/*!40000 ALTER TABLE `bed_types` DISABLE KEYS */;
INSERT INTO `bed_types` (`id`, `name`, `key`) VALUES
	(1, 'Exclusive Cabin', 'exclusive_cabin');
/*!40000 ALTER TABLE `bed_types` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
