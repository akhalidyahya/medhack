let selected_user = '';
let selected_room = '';
if ($('#calendar_room').length) {
    var settings = {
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        editable: false,
        dayMaxEventRows: parseInt(app.options.calendar_events_limit) + 1,
        views: {
            day: {
                dayMaxEventRows: false
            }
        },
        initialView: app.options.default_view_calendar,
        moreLinkClick: function(info) {
            calendar.gotoDate(info.date)
            calendar.changeView('dayGridDay');

            setTimeout(function() {
                $('.fc-popover-close').click();
            }, 250)
        },
        loading: function(isLoading, view) {
            !isLoading ? $('.dt-loader').addClass('hide') : $('.dt-loader').removeClass('hide');
        },
        direction: (isRTL == 'true' ? 'rtl' : 'ltr'),
        eventStartEditable: false,
        firstDay: parseInt(app.options.calendar_first_day),
        events: function(info, successCallback, failureCallback) {
            return $.getJSON(admin_url + 'room_management/getRoomSchedule', {
                start: info.startStr,
                end: info.endStr,
                selectedUser: selected_user,
                selectedRoom: selected_room
            }).then(function(data) {
                successCallback(data.map(function(e) {
                    return $.extend({}, e, {
                        start: e.start || e.date,
                        end: e.end || e.date
                    });
                }));
            });
        },
        eventDidMount: function(data) {
            var $el = $(data.el);
            $el.attr('title', data.event.extendedProps._tooltip);
            $el.attr('onclick', data.event.extendedProps.onclick);
            $el.attr('data-toggle', 'tooltip');
        },
    }
    var calendar = new FullCalendar.Calendar(document.getElementById('calendar_room'), settings);
    calendar.render();
}