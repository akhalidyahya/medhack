$(document).ready(function () {
    $('#table-custom').DataTable({
        "order": [[0, "dsc"]],
    });
    $('#table-appointment').DataTable({
        "order": [[0, "dsc"]],
        'columnDefs': [{
            'targets': 0,
            'searchable': true,
            // 'orderable': false,
            'bSort': true,
            "type": 'date'
        }],
    });
});

$(document).ready(function () {
    $('#table-list').DataTable();
});

$(document).ready(function () {
    $('#table-custom-bed-allotment').DataTable({
        "order": [[2, "dsc"]],
    });
});


function success() {
    alertify.set('notifier', 'delay', 2);
    alertify.set('notifier', 'position', 'top-center');
    alertify.success("Berhasil");
}

function info() {
    alertify.set('notifier', 'delay', 2);
    alertify.set('notifier', 'position', 'top-center');
    alertify.error("Data tidak boleh kosong!");
}

function error() {
    alertify.set('notifier', 'delay', 2);
    alertify.set('notifier', 'position', 'top-center');
    alertify.error("Gagal, data sudah ada atau terjadi error!");
}

if ($('#calendar_a').length) {
    var settings = {
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        editable: false,
        dayMaxEventRows: parseInt(app.options.calendar_events_limit) + 1,
        views: {
            day: {
                dayMaxEventRows: false
            }
        },
        initialView: app.options.default_view_calendar,
        moreLinkClick: function (info) {
            calendar.gotoDate(info.date)
            calendar.changeView('dayGridDay');

            setTimeout(function () {
                $('.fc-popover-close').click();
            }, 250)
        },
        loading: function (isLoading, view) {
            !isLoading ? $('.dt-loader').addClass('hide') : $('.dt-loader').removeClass('hide');
        },
        direction: (isRTL == 'true' ? 'rtl' : 'ltr'),
        eventStartEditable: false,
        firstDay: parseInt(app.options.calendar_first_day),
        events: function (info, successCallback, failureCallback) {
            return $.getJSON("appointment/list_appointment", {
                start: info.startStr,
                end: info.endStr,
            }).then(function (data) {
                console.log(data);
                successCallback(data.map(function (e) {
                    return $.extend({}, e, {
                        start: e.start || e.date,
                        end: e.start || e.date
                    });
                }));
            });
        },
        eventDidMount: function (data) {
            var $el = $(data.el);
            $el.attr('title', data.event.extendedProps._tooltip);
            $el.attr('onclick', data.event.extendedProps.onclick);
            $el.attr('data-toggle', 'tooltip');
        },
    }
    var calendar = new FullCalendar.Calendar(document.getElementById('calendar_a'), settings);
    calendar.render();
}

//init timepicekr
