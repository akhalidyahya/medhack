<?php

namespace app\constants;
defined('BASEPATH') or exit('No direct script access allowed');

class CommonConstant {
    const ADMIN_ROLE_FINANCE            = 2;
    const ADMIN_ROLE_CUSTOMER_SERVICE   = 3;   
    const ADMIN_ROLE_DOCTOR             = 4;
    const ADMIN_ROLE_NURSE              = 5;
    const ADMIN_ROLE_ADMIN_KLINIK       = 6;

    const APPOINTMENT_STATUS_APPROVED   = 'approved';
    const APPOINTMENT_STATUS_PENDING    = 'pending';
    const APPOINTMENT_STATUS_REJECT     = 'reject';
    const APPOINTMENT_STATUS_CANCEL     = 'cancel';
    const APPOINTMENT_STATUS_DONE       = 'done';
    const APPOINTMENT_STATUS_LIST       = [
        self::APPOINTMENT_STATUS_APPROVED   => 'Diterima',
        self::APPOINTMENT_STATUS_PENDING    => 'Menunggu Konfirmasi',
        self::APPOINTMENT_STATUS_REJECT     => 'Ditolak',
        self::APPOINTMENT_STATUS_CANCEL     => 'Dibatalkan',
        self::APPOINTMENT_STATUS_DONE       => 'Selesai'
    ];

    const MASTER_ROOM_STATUS_AVAILABLE     = 'AVAILABLE';
    const MASTER_ROOM_STATUS_OCCUPIED      = 'OCCUPIED';
    const MASTER_ROOM_STATUS_LIST          = [
        self::MASTER_ROOM_STATUS_AVAILABLE   => 'Tersedia',
        self::MASTER_ROOM_STATUS_OCCUPIED    => 'Ditempati',
    ];

    const ALLOTMENT_STATUS_ON_GOING     = 'ON_GOING';
    const ALLOTMENT_STATUS_WILL_OCCUR   = 'WILL_OCCUR';
    const ALLOTMENT_STATUS_PAST         = 'PAST';
    const ALLOTMENT_STATUS_DONE         = 'DONE';
    const ALLOTMENT_STATUS_LIST         = [
        self::ALLOTMENT_STATUS_ON_GOING     => 'Sedang Berlangsung',
        self::ALLOTMENT_STATUS_PAST         => 'Waktu Selesai',
        self::ALLOTMENT_STATUS_DONE         => 'Selesai',
        self::ALLOTMENT_STATUS_WILL_OCCUR   => 'Akan Berlangsung',
    ];

    const INVOICE_STATUS_UNPAID     = 1;
    const INVOICE_STATUS_PAID       = 2;
    const INVOICE_STATUS_PARTIALLY  = 3;
    const INVOICE_STATUS_OVERDUE    = 4;
    const INVOICE_STATUS_CANCELLED  = 5;
    const INVOICE_STATUS_DRAFT      = 6;
    const INVOICE_STATUS_REJECT     = 7;
    const INVOICE_STATUS_WAIT       = 8;
    const INVOICE_STATUS_LISTS      = [
        self::INVOICE_STATUS_UNPAID     => 'invoice_status_unpaid',
        self::INVOICE_STATUS_PAID       => 'invoice_status_paid',
        self::INVOICE_STATUS_PARTIALLY  => 'invoice_status_not_paid_completely',
        self::INVOICE_STATUS_OVERDUE    => 'invoice_status_overdue',
        self::INVOICE_STATUS_CANCELLED  => 'invoice_status_cancelled',
        self::INVOICE_STATUS_DRAFT      => 'invoice_status_draft',
        self::INVOICE_STATUS_REJECT     => 'invoice_status_reject',
        self::INVOICE_STATUS_WAIT       => 'invoice_status_wait',
    ];

    const WA_DIALOG_STATUS_LEADS     = 'LEAD';
    const WA_DIALOG_STATUS_FU        = 'FU';
    const WA_DIALOG_STATUS_PATIENT   = 'PATIENT';
    const WA_DIALOG_STATUS_CHAT      = [
        self::WA_DIALOG_STATUS_FU      => 'Follow Up',
        self::WA_DIALOG_STATUS_PATIENT => 'Pasien',
    ];
    const WA_DIALOG_STATUS_LISTS     = [
        self::WA_DIALOG_STATUS_LEADS   => 'Lead',
        self::WA_DIALOG_STATUS_FU      => 'Follow Up',
        self::WA_DIALOG_STATUS_PATIENT => 'Pasien',
    ];

    const JENIS_PEMBELIAN_OBAT_OFFLINE   = 'OBAT_OFFLINE';
    const JENIS_PEMBELIAN_OBAT_ONLINE    = 'OBAT_ONLINE';
    const JENIS_PEMBELIAN_RUANGAN        = 'RUANGAN';
    const JENIS_PEMBELIAN_JANJI          = 'JANJI';
    const JENIS_PEMBELIAN_OBAT_LISTS     = [
        self::JENIS_PEMBELIAN_OBAT_OFFLINE   => 'Langsung',
        self::JENIS_PEMBELIAN_OBAT_ONLINE    => 'Online',
    ];
    const JENIS_PEMBELIAN_LISTS          = [
        self::JENIS_PEMBELIAN_OBAT_OFFLINE   => 'Obat Online',
        self::JENIS_PEMBELIAN_OBAT_ONLINE    => 'Obat Offline',
        self::JENIS_PEMBELIAN_RUANGAN        => 'Ruangan',
        self::JENIS_PEMBELIAN_JANJI          => 'Janji',
    ];

    const APPOINTMENT_SUBTOTAL   = '0';
    const APPOINTMENT_TOTAL      = '0';
    const ALLOTMENT_SUBTOTAL     = '300000.00';
    const ALLOTMENT_TOTAL        = '400000.00';

    const GENDER_MALE   = 'MALE';
    const GENDER_FEMALE = 'FEMALE';
    const GENDER_LISTS  = [
        self::GENDER_MALE   => 'Pria',
        self::GENDER_FEMALE => 'Wanita',
    ];

    const DAILY_FU_COUNT = 10;
    const SHIPPING_STATUS_SENT      = 1;
    const SHIPPING_STATUS_UNSENT    = null;
    const SHIPPING_STATUS_LISTS      = [
        self::SHIPPING_STATUS_SENT     => 'Sudah Dikirim',
        self::SHIPPING_STATUS_UNSENT   => 'Belum Dikirim',
    ];
}