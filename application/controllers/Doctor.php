<?php

defined('BASEPATH') or exit('No direct script access allowed');
use app\constants\CommonConstant;


class Doctor extends ClientsController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('doctor_model');
    }

    public function index()
    {
        $data['doctor'] = $this->doctor_model->get_doctor(CommonConstant::ADMIN_ROLE_DOCTOR);
        $data['staff_branch'] = $this->doctor_model->get_staffbranch();
        $data['branch'] = $this->doctor_model->get_branch();
        $this->data($data);
        $this->view('doctor');
        $this->layout();
    }
}