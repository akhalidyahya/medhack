<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Prescription extends ClientsController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('prescription_model');
    }

    public function index()
    {
        $data['riwayat'] = $this->prescription_model->get();
        $this->data($data);
        $this->view('prescription');
        $this->layout();
    }

}