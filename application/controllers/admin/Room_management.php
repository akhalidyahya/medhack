<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

class Room_management extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_management_model');
    }

    public function index()
    {
        echo 'index';
    }

    public function allocation()
    {
        if (!has_permission('room_management', '', 'view')) {
            access_denied('room_management');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('room_management_allocation');
        }
        $data['title']                      = _l('room_allocation');
        $data['beds']                       = $this->room_management_model->getAllBeds();
        
        $this->load->view('admin/room_management/manage', $data);
    }

    public function master()
    {
        if (!has_permission('room_management', '', 'edit')) {
            access_denied('room_management');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('room_management_master');
        }
        $data['title']                      = _l('room_management_master');
        $data['room_total']                 = $this->room_management_model->getMasterCount();
        $data['bed_types']                  = $this->room_management_model->getBedTypes();
        
        $this->load->view('admin/room_management/master', $data);
    }

    public function getAvailableRoom()
    {
        $date = $this->input->get('date');
        if(empty($date)) {
            $date = Date('Y-m-d H:i');
        }
        echo json_encode($this->room_management_model->getAvailableRoom($date));
    }

    public function getAllotment($id)
    {
        echo json_encode($this->room_management_model->getDataAllotmentById($id));
    }

    public function deleteAllotment($id)
    {
        if (!has_permission('room_management', '', 'edit')) {
            access_denied('room_management');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        if($this->room_management_model->deleteAllotmentById($id)) {
        // if($true) {
            $response['status'] = true;
            $response['message'] = 'Data Deleted!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong';
        }

        echo json_encode($response);
    }

    public function saveAllotment() {
        if (!has_permission('room_management', '', 'edit')) {
            access_denied('room_management');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'allotment_timestamp'       => $this->input->post('allotment_timestamp'),
            'bed_id'                    => $this->input->post('bed_id'),
            'patient_id'                => $this->input->post('patient_id'),
            'discharge_timestamp'       => $this->input->post('discharge_timestamp'),
        ];
        if($this->input->post('method') == 'add') {
            if($this->room_management_model->insertDataAllotment($data)) {
                $response['message'] = 'Data Added!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        } else {
            $id = $this->input->post('id');
            if($this->room_management_model->updateDataAllotment($data,$id)) {
                $response['message'] = 'Data Updated!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function masterDataSave() {
        if (!has_permission('room_management', '', 'edit')) {
            access_denied('room_management');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'bed_number'            => $this->input->post('bed_number'),
            'type'                  => $this->input->post('type'),
            'status'                => CommonConstant::MASTER_ROOM_STATUS_AVAILABLE,
            'description'           => $this->input->post('description'),
        ];
        if($this->input->post('method') == 'add') {
            if($this->room_management_model->insertDataMaster($data)) {
                $response['message'] = 'Data Added!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        } else {
            $id = $this->input->post('id');
            if($this->room_management_model->updateDataMaster($data,$id)) {
                $response['message'] = 'Data Updated!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getMasterDataById($id) {
        echo json_encode($this->room_management_model->getDataMasterById($id));
    }

    public function deleteMasterData($id) {
        if (!has_permission('room_management', '', 'edit')) {
            access_denied('room_management');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        if($this->room_management_model->deleteDataMasterById($id)) {
        // if($true) {
            $response['status'] = true;
            $response['message'] = 'Data Deleted!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong';
        }

        echo json_encode($response);
    }

    public function bed_types_table()
    {
        $this->app->get_table_data('room_management_master_bed_type');
    }

    public function bed_type_save()
    {
        if (!has_permission('room_management', '', 'edit')) {
            access_denied('room_management');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'name'          => $this->input->post('name'),
            'key'           => str_replace('-','_',slug_it(htmlspecialchars($this->input->post('name')))),
        ];
        if($this->input->post('method') == 'add') {
            if($this->room_management_model->insertDataBedType($data)) {
                $response['message'] = 'Data Added!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        } else {
            $id = $this->input->post('id_bed_type');
            if($this->room_management_model->updateDataBedType($data,$id)) {
                $response['message'] = 'Data Updated!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getBedTypeById($id) {
        echo json_encode($this->room_management_model->getBedTypeById($id));
    }

    public function deleteBedType($id)
    {
        if (!has_permission('room_management', '', 'edit')) {
            access_denied('room_management');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        if($this->room_management_model->deleteBedTypeById($id)) {
        // if($true) {
            $response['status'] = true;
            $response['message'] = 'Data Deleted!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong';
        }

        echo json_encode($response);
    }

    public function getRoomSchedule()
    {
        $data = $this->room_management_model->getCalendarData(
            date('Y-m-d', strtotime($this->input->get('start'))),
            date('Y-m-d', strtotime($this->input->get('end'))),
            $this->input->get('selectedUser'),
            $this->input->get('selectedRoom')
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}