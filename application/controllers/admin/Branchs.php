<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Branchs extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('branchs_model');
    }

    /* List all Branchs */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('branchs');
        }
        $data['title']          = _l('branchs');
        $data['branch_count']   = $this->branchs_model->getBranchCount();
        $this->load->view('admin/branchs/manage', $data);
    }

    public function table($clientid = '')
    {

        $this->app->get_table_data('branchs', [
            'clientid' => $clientid,
        ]);
    }

    public function getBranchById($id)
    {
        echo json_encode($this->branchs_model->getBranchById($id));
    }

    public function deleteBranch($id)
    {
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $true = true;
        if($this->branchs_model->deleteBranchById($id)) {
        // if($true) {
            $response['status'] = true;
            $response['message'] = 'Branch Data Deleted!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong';
        }

        echo json_encode($response);
    }

    public function saveBranch()
    {
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'branch' => $this->input->post('branch'),
            'code' => $this->input->post('code'),
            'address' => $this->input->post('address'),
            'city' => strtoupper($this->input->post('city')),
        ];
        if($this->input->post('method') == 'add') {
            if($this->branchs_model->insertData($data)) {
                $response['success'] = true;
                $response['message'] = 'Branch Data Added!';
            }
        } else {
            $id = $this->input->post('id');
            if($this->branchs_model->updateData($data,$id)) {
                $response['success'] = true;
                $response['message'] = 'Branch Data Updated!';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
}
