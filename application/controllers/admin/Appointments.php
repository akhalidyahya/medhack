<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

class Appointments extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['appointment_model','branchs_model']);

    }

    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('appointments');
        }
        $data['title']                      = _l('appointment');
        $data['appointment_count']          = $this->appointment_model->getDataCount();
        $data['appointment_approved_count'] = $this->appointment_model->getDataCountByStatus(CommonConstant::APPOINTMENT_STATUS_APPROVED);
        $data['appointment_pending_count']  = $this->appointment_model->getDataCountByStatus(CommonConstant::APPOINTMENT_STATUS_PENDING);
        $data['branch']                     = $this->branchs_model->getAllData();
        
        $this->load->view('admin/appointments/manage', $data);
    }

    public function setStatus($id)
    {
        if (!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }

        $status = $this->input->post('status');
        $branch = $this->input->post('branch');
        $patient_id = $this->input->post('patient_id');

        
        $response = setCommonResponse();
        
        if($status == CommonConstant::APPOINTMENT_STATUS_DONE) {
            $response['invoice_id'] = $this->createInvoice($id, $branch, $patient_id);
            if(!is_admin()) {
                $response['invoice_id'] = NULL;
            }
        }

        if($this->appointment_model->changeStatus($id,$status)) {
            $response['status'] = true;
            $response['message'] = 'Status Janji Updated!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong!';
        }

        header('Content-Type: application/json');
        echo json_encode($response);        
    }

    public function getAppointmentByDoctor()
    {
        $data = $this->appointment_model->getCalendarDataByUser(
            date('Y-m-d', strtotime($this->input->get('start'))),
            date('Y-m-d', strtotime($this->input->get('end'))),
            $this->input->get('selectedUser')
        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function search()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            echo json_encode($this->appointment_model->search($this->input->post('q')));
        }
    }

    public function save_appointment() 
    {
        header('Content-Type: application/json');
        $response   = setCommonResponse();

        $data = [
            'timestamp'     => $this->input->post('form_waktu'),
            'doctor_id'     => $this->input->post('doctor_id'),
            'patient_id'    => $this->input->post('f_patient'),
            'status'        => CommonConstant::APPOINTMENT_STATUS_PENDING,
            'message'       => $this->input->post('message'),
            'branch_id'     => $this->input->post('form_branch_id'),
        ];
        if($this->appointment_model->add($data)) {
            $response['status'] = true;
            $response['message'] = 'Data janji disimpan!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong!';
        }
        echo json_encode($response);
    }

    public function save_doctor()
    {
        header('Content-Type: application/json');
        $response   = setCommonResponse();

        $id          = $this->input->post('appointment_id');
        $doctor_id   = $this->input->post('doctor_id');
        $patient_id  = $this->input->post('patient_id');
        if($this->appointment_model->saveDoctor($id,$doctor_id)) {
            $response['status'] = true;
            $response['message'] = 'Data Updated!';

            //add notification
            add_notification([
                'description' => 'Pasien '.get_contact_full_name($patient_id).' baru saja membuat janji!',
                'touserid'    => $doctor_id,
                'link'        => 'appointments/',
            ]);

        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong!';
        }
        echo json_encode($response);
    }

    public function createInvoice($id, $branch, $patient_id)
    {
        if (!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $post = [
            'clientid'          => 1,
            'patient_id'        => $patient_id,
            'branch_id'         => $branch,
            'jenis_pembelian'   => CommonConstant::JENIS_PEMBELIAN_JANJI,
            'datecreated'       => $this->appointment_model->get_appointment_by_id($id)->timestamp,
            'status'            => CommonConstant::INVOICE_STATUS_DRAFT
        ];

        return $this->appointment_model->add_invoice_order($post, $id);
    }
}