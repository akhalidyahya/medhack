<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');        
    }

    /* List all clients */
    public function index()
    {
        if (!(has_permission('users', '', 'view') || has_permission('users', '', 'view_branch'))) {
            access_denied('users');
        }

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('users');
        }

        $data['title']          = _l('user_lists');
        $data['user_count']     = $this->users_model->getTotalCount();

        $this->load->view('admin/users/manage', $data);
    }

    /**
     * Birthday pages
     */
    public function users_birthday()
    {
        if (!(has_permission('users', '', 'view') || has_permission('users', '', 'view_branch'))) {
            access_denied('users');
        }

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('users_birthday');
        }

        $data['title']          = _l('users_birthday');
        $data['user_count']     = $this->users_model->getTotalCount();

        $this->load->view('admin/users/manage-birthday', $data);
    }

    public function getUserById($id)
    {
        echo json_encode($this->users_model->getDataById($id));
    }

    public function deleteUser($id)
    {
        if (!has_permission('users', '', 'edit')) {
            access_denied('users');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $true = true;
        if($this->users_model->deleteDataById($id)) {
        // if($true) {
            $response['status'] = true;
            $response['message'] = 'User Data Deleted!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong';
        }

        echo json_encode($response);
    }

    public function table()
    {
        if (!has_permission('users', '', 'view')) {
            ajax_access_denied();
        }

        $this->app->get_table_data('users');
    }

    public function saveUser()
    {
        if (!has_permission('users', '', 'edit')) {
            access_denied('users');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }        

        $response = setCommonResponse();
        $form_validation_rules = [
            [
                'field' => 'firstname',
                'label' => 'Nama Depan',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s wajib diisi!',
                ]
            ],
            [
                'field' => 'lastname',
                'label' => 'Nama Belakang',
                'rules' => 'required',
                'errors' => [
                    'required' => '%s wajib diisi!',
                ]
            ],
            // [
            //     'field' => 'password',
            //     'label' => 'Password',
            //     'rules' => 'required',
            //     'errors' => [
            //         'required' => '%s wajib diisi!',
            //     ]
            // ],
            [
                'field' => 'phonenumber',
                'label' => 'Nomor Telepon',
                'rules' => 'required|is_unique[contacts.phonenumber]',
                'errors' => [
                    'required'  => '%s wajib diisi!',
                    // 'is_unique' => '%s sudah pernah didaftarkan!',
                ]
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'is_unique[contacts.email]',
                'errors' => [
                    'is_unique' => '%s sudah pernah didaftarkan!',
                ]
            ],
        ];
        $this->form_validation->set_rules($form_validation_rules);
        
        //validation
        if ($this->form_validation->run() == FALSE) {
            $response['success'] = false;
            $response['message'] = 'Data pada form tidak valid!';
            $response['error']   = $this->form_validation->error_array();
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }

        $data = [
            'firstname'         => $this->input->post('firstname'),
            'lastname'          => $this->input->post('lastname'),
            'address'           => $this->input->post('address'),
            'email'             => $this->input->post('email'),
            'phonenumber'       => $this->input->post('phonenumber'),
            'userid'            => 1,
            'is_primary'        => 1,
            'email_verified_at' => date('Y-m-d H:i:s'), 
            'datecreated'       => date('Y-m-d H:i:s'),
            'tempat'            => $this->input->post('tempat'),
            'tanggal_lahir'     => $this->input->post('tanggal_lahir'),
            'gender'            => $this->input->post('gender'),
            'nik'               => $this->input->post('nik'),
            'pernah_operasi'    => $this->input->post('pernah_operasi'),
            'riwayat_operasi'   => $this->input->post('riwayat_operasi'),
            'nama_cs'           => $this->input->post('nama_cs'),
            'is_agree'          => 0,
            'branch_id'         => $this->input->post('branch_id'),
        ];
        if($this->input->post('method') == 'add') {
            $data['password'] = app_hash_password($this->input->post('password'));
            if($this->users_model->insertData($data)) {
                $response['message'] = 'User Data Added!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something went wrong';
            }
        } else {
            $id = $this->input->post('id');
            if(!empty($this->input->post('password'))) {
                $data['password'] = app_hash_password($this->input->post('password'));
            }
            if($this->users_model->updateData($data,$id)) {
                $response['message'] = 'User Data Updated!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function search()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            echo json_encode($this->users_model->search($this->input->post('q')));
        }
    }
}