<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Shippings extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('shipping_model');
    }

    /* List all orders */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('shipping');
        }
        $data['title']         = 'Pengiriman';
        $data['order_count']   = $this->shipping_model->getDataCount();
        $data['order_master']   = $this->shipping_model->getAllData();
        // echo json_encode($data); die();
        $this->load->view('admin/shippings/manage', $data);
    }

    public function addResi($id)
    {
        $resi = $this->input->post('resi');
        $response = setCommonResponse();
        if($this->shipping_model->addResi($id, $resi)) {
                $response['status'] = true;
                $response['message'] = 'Status Pemesanan Updated!';
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function setStatus($id)
    {
        if (!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }

        $status = $this->input->post('status');

        $response = setCommonResponse();

        if($this->shipping_model->changeStatus($id,$status)) {
            $response['status'] = true;
            $response['message'] = 'Status Pemesanan Updated!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong!';
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}
