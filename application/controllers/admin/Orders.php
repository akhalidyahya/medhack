<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Orders extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_model');
    }

    /* List all orders */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('orders');
        }
        $data['title']         = _l('orders');
        $data['order_count']   = $this->order_model->getDataCount();
        $this->load->view('admin/orders/manage', $data);
    }

    public function getOrderById($id)
    {
        header('Content-Type: application/json');
        $data               = $this->order_model->getDataById($id);
        $data->invoice_number = format_invoice_number($data->id);
        echo json_encode($data);
        exit;
    }

    public function setStatus($id)
    {
        if (!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }

        $status = $this->input->post('status');

        $response = setCommonResponse();

        if($this->order_model->changeStatus($id,$status)) {
            $response['status'] = true;
            $response['message'] = 'Status Pemesanan Updated!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong!';
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}
