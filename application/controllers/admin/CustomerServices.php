<?php

defined('BASEPATH') or exit('No direct script access allowed');

use app\constants\CommonConstant;
use app\services\RestApi;
use Carbon\Carbon;

class CustomerServices extends AdminController
{
    public function __construct() {
        parent::__construct();
        $this->load->model('customer_services_model');
        $this->load->model('whatsapp_model');
        $this->load->library('pagination');
    }
    /* List all clients */
    public function index()
    {
        if (!has_permission('cust_service', '', 'view')) {
            access_denied('cust_service');
        }
        
        $data['title']          = _l('cust_service'); 
        $status                 = app\services\WhatsappApi::getStatus();  
        $data['whatsappApi']    = (object)[
            'status'    => '<i class="fa fa-circle" style="color:green"></i> online',
            'message'   => 'Whatsapp API berjalan dengan baik'
        ];

        if(!empty($status->error) || $status->accountStatus != 'authenticated') {
            $data['dialogs'] = [];
            $data['whatsappApi']->status    = '<i class="fa fa-circle"></i> offline';
            $data['whatsappApi']->message   = 'Whasapp butuh login';
            if(!empty($status->error)) {
                $data['whatsappApi']->status    = '<i class="fa fa-circle" style="color:red"></i>  error';
                $data['whatsappApi']->message   = 'Terjadi masalah dengan chat-api.com';
            }
        } else {
            // $data['dialogs'] = app\services\WhatsappApi::getDialogs(9,0);
            // $this->whatsapp_model->saveDialogs($data['dialogs']);
            if(is_admin(get_staff_user_id())) {
                $data['dialogs'] = app\services\WhatsappApi::getDialogs(9,0);
                $this->whatsapp_model->saveDialogs($data['dialogs']);
            } else {
                $data['dialogs'] = $this->whatsapp_model->getDialogByStaffId(get_staff_user_id(),9,0);
            }
        }

        // $data['dialogs'] = app\services\WhatsappApi::getDialogs(9,0);
        // $this->whatsapp_model->saveDialogs($data['dialogs']);

        //testing
        // $data['dialogs'] = $this->whatsapp_model->getDialogByStaffId(2,9,0);

        // $data['status']         = app\services\WhatsappApi::getStatus();

        if( $GLOBALS['current_user']->role == 3 && !is_admin() ) {
            if ($this->input->is_ajax_request()) {
                $this->app->get_table_data('whatsapp_dialogs');
            }
            $data['all_number'] = $this->whatsapp_model->getSourceNumber();
            $data['cs_page'] = 1;

            $config['base_url'] = base_url().'admin/customerServices/index';
            $config['total_rows'] = $this->whatsapp_model->getRowCount();
            $config['uri_segment'] = 4;
            $config['per_page'] = 20;
            $config['use_page_numbers'] = TRUE;
            $config['enable_query_strings'] = TRUE;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';
            $config['reuse_query_string'] = TRUE;

            // start add boostrap class and styles
            $config['full_tag_open'] = '<ul class="pagination">';        
            $config['full_tag_close'] = '</ul>';        
            $config['first_link'] = 'First';        
            $config['last_link'] = 'Last';        
            $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';        
            $config['first_tag_close'] = '</span></li>';        
            $config['prev_link'] = '&laquo';        
            $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';        
            $config['prev_tag_close'] = '</span></li>';        
            $config['next_link'] = '&raquo';        
            $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';        
            $config['next_tag_close'] = '</span></li>';        
            $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';        
            $config['last_tag_close'] = '</span></li>';        
            $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';        
            $config['cur_tag_close'] = '</a></li>';        
            $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';        
            $config['num_tag_close'] = '</span></li>';
            // end add boostrap class and styles

            // Initialize pagination
            $this->pagination->initialize($config);		

            $limit = $this->input->get('per_page') ?? $config['per_page'];
            $from = $this->input->get('page',0) * $limit;
            $userId = !is_admin() ? get_staff_user_id() : null;
            $data['dataWa'] = $this->whatsapp_model->getData($limit,$from,$userId);
            $this->load->view('admin/customer_services/cs-chat', $data);
            // $this->load->view('admin/whatsapp/whatsapp_dialog', $data);
        } else {
            $this->load->view('admin/customer_services/manage', $data);
        }
    }

    public function followup_result_index()
    {
        if (!has_permission('cust_service', '', 'view')) {
            access_denied('cust_service');
        }
        
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('fu_result');
        }

        $data['title']          = _l('fu_result');
        $data['summary_count']  = $this->customer_services_model->getCountDialogByStatus(CommonConstant::WA_DIALOG_STATUS_FU);
        $this->load->view('admin/customer_services/fu_result', $data);
    }

    public function table_fu_summary()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('fu_summary');
        } else {
            access_denied('Not Ajax Request');
        }
    }

    public function saveNote()
    {
        if (!has_permission('cust_service', '', 'view')) {
            access_denied('cust_service');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'followup_note' => $this->input->post('note'),
            'fu_status_id'  => $this->input->post('status'),
        ];
        $id = $this->input->post('dialog_id');
        if($this->whatsapp_model->saveNote($data,$id)) {
            $response['message'] = 'Dialog Data Updated!';
        } else {
            $response['success'] = false;
            $response['message'] = 'Something went wrong';
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function fuStatusIndex()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('fu_summary_status');
        }

        $data['title']  = _l('fu_summary').' Status';
        $this->load->view('admin/customer_services/fu_summary_status', $data);
    }

    public function getStatusFuById($id)
    {
        header('Content-Type: application/json');
        echo json_encode($this->customer_services_model->getStatusFuById($id));
        exit;
    }

    public function saveFuStatus()
    {
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'status'    => $this->input->post('status'),
        ];
        if($this->input->post('method') == 'add') {
            if($this->customer_services_model->insertStatusFu($data)) {
                $response['message'] = 'Data Added!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something went wrong';
            }
        } else {
            $id = $this->input->post('id');
            if($this->customer_services_model->updateStatusFu($data,$id)) {
                $response['message'] = 'Data Updated!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function deleteFuStatus($id)
    {
        if (!is_admin()) {
            access_denied('cust_services');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $true = true;
        if($this->customer_services_model->deleteStatusFuById($id)) {
        // if($true) {
            $response['status'] = true;
            $response['message'] = 'Data Deleted!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong';
        }

        echo json_encode($response);
        exit;
    }

    public function getFuPersonByDialogId($dialog_id)
    {
        header('Content-Type:application/json');
        $person = $this->whatsapp_model->getFollowUpBy($dialog_id);
        if(!empty($person)) {
            echo json_encode($person->fullname);
        } else {
            echo json_encode([]);
        }
        exit;
    }

    public function followUpDialog($dialog_id)
    {
        header('Content-Type:application/json');
        $dialog         = $this->whatsapp_model->getDialogByDialogId($dialog_id);
        $currentFollowUpCount  = getDailyFollowUp(get_staff_user_id());
        $isPriority     = $GLOBALS['current_user']->priority == 1 ? true : false;
        $maxFollowUp    = !empty($GLOBALS['current_user']->staff_quota) ? $GLOBALS['current_user']->staff_quota : get_option('fu_daily_limit');
        $canFollowUp    = false;
        if($isPriority || $currentFollowUpCount <= $maxFollowUp) {
            $canFollowUp = true;
        }
        if((@$dialog->label == CommonConstant::WA_DIALOG_STATUS_LEADS || empty(@$dialog->label)) && $canFollowUp) {
            $data   = [
                'followup_id'   => get_staff_user_id(),
                'label'         => CommonConstant::WA_DIALOG_STATUS_FU,
                'followup_time' => Carbon::now()
            ];
            $this->whatsapp_model->saveNote($data,$dialog_id);
        }
        $phone = str_replace('@c.us','',$dialog_id);
        echo json_encode($phone);
        exit;
    }

    public function statistik()
    {
        $data['title']                      = 'Statistik Customer Service';
        $data['summary_count_follow_up']    = $this->customer_services_model->getCountDialogByStatus(CommonConstant::WA_DIALOG_STATUS_FU);
        $data['summary_count_patient']      = $this->customer_services_model->getCountDialogByStatus(CommonConstant::WA_DIALOG_STATUS_PATIENT);
        $data['summary_count_leads']        = $this->customer_services_model->getCountDialogByStatus(CommonConstant::WA_DIALOG_STATUS_LEADS);

        $data['fu_status']['label']         = [];
        $data['fu_status']['dataset']       = [];
        $data['fu_status']['bgcolor']       = [];

        foreach($this->whatsapp_model->getAllFuStatus() as $fuStatus) {
            $data['fu_status']['label'][]   = $fuStatus->status;
        }

        foreach($this->whatsapp_model->getDialogCountGroupByFuStatus() as $dataChart) {
            if(empty($dataChart->fu_status_id)) {
                continue;
            }
            $data['fu_status']['bgcolor'][] = 'rgb('.rand(0,255).','.rand(0,255).','.rand(0,255).')';
            $data['fu_status']['dataset'][] = $dataChart->jumlah;
        }

        $data['all_staff']      = $this->customer_services_model->getAllStaff();
        $data['fu_summaries']   = $this->customer_services_model->getAllFuStatusStatistik();        

        $this->load->view('admin/customer_services/statistik', $data);
    }

    public function getChartDataByStaff()
    {
        header('Content-Type:application/json');
        $staff_id = $this->input->get('staffId');
        $data['fu_status']['label']         = [];
        $data['fu_status']['dataset']       = [];
        $data['fu_status']['bgcolor']       = [];

        // foreach($this->whatsapp_model->getAllFuStatus() as $fuStatus) {
        //     $data['fu_status']['label'][]   = $fuStatus->status;
        // }

        foreach($this->whatsapp_model->getDialogCountGroupByFuStatus($staff_id) as $dataChart) {
            if(empty($dataChart->fu_status_id)) {
                continue;
            }
            $data['fu_status']['bgcolor'][] = 'rgb('.rand(0,255).','.rand(0,255).','.rand(0,255).')';
            $data['fu_status']['dataset'][] = $dataChart->jumlah;
            $data['fu_status']['label'][]   = $dataChart->status;
        }
        echo json_encode($data);
        exit;
    }

    public function getTableDataByStaff() {
        header('Content-Type:application/json');
        $staff_id = $this->input->get('staffId');
        $data = $this->customer_services_model->getAllFuStatusStatistik($staff_id);
        echo json_encode($data);
        exit;
    }

    public function setFuLimit()
    {
        header('Content-Type:application/json');
        
        $name   = 'fu_daily_limit';
        $value  = $this->input->post('limit');

        $response = setCommonResponse();
        if(update_option($name,$value)) {
            $response['status'] = true;
            $response['message'] = 'Data Limit Berhasil Diupdate!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Data Limit Gagal Diupdate!';
        }

        echo json_encode($response);
        exit;
        
    }

    public function save() 
    {
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'branch_id'    => $this->input->post('branch_id'),
        ];
        if($this->customer_services_model->saveBranchChat($data,$this->input->post('dialog_id'))) {
            $response['message'] = 'Data berhasil diupdate!';
        } else {
            $response['success'] = false;
            $response['message'] = 'Something went wrong';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}
