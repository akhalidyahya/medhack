<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Terms_and_conditions extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('utilities_model');
    }

    /* List all clients */
    public function index()
    {
        $data['title']          = _l('terms_and_conditions');
        $data['terms']          = get_option('terms_and_conditions');

        $this->load->view('admin/terms_and_conditions/manage', $data);
    }

    public function save()
    {
        $text = $this->input->post('text');
        $this->utilities_model->setTermsAndConditions($text);
        header('Content-Type: application/json');
        echo json_encode(['success'=>true,'message'=>'Data saved!']);
    }

}