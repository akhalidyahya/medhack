<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Banks extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('banks_model');        
    }

    /* List all clients */
    public function index()
    {
        if (!is_admin()) {
            access_denied('banks');
        }

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('bank');
        }

        $data['title']          = _l('bank');
        $data['bank_total']     = $this->banks_model->getTotalCount();

        $this->load->view('admin/banks/manage', $data);
    }

    public function getBankById($id)
    {
        echo json_encode($this->banks_model->getDataById($id));
    }

    public function deleteBank($id)
    {
        if (!is_admin()) {
            access_denied('banks');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $true = true;
        if($this->banks_model->deleteDataById($id)) {
        // if($true) {
            $response['status'] = true;
            $response['message'] = 'User Data Deleted!';
        } else {
            $response['status'] = false;
            $response ['message']= 'Something Went Wrong';
        }

        echo json_encode($response);
    }

    public function saveBank()
    {
        if (!is_admin()) {
            access_denied('banks');
        }
        if(!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'bank_name'         => $this->input->post('bank_name'),
            'acc_number'        => $this->input->post('acc_number'),
            'person_name'       => $this->input->post('person_name'),
        ];
        if($this->input->post('method') == 'add') {
            if($this->banks_model->insertData($data)) {
                $response['message'] = 'Bank Data Added!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something went wrong';
            }
        } else {
            $id = $this->input->post('id');
            if($this->banks_model->updateData($data,$id)) {
                $response['message'] = 'User Data Updated!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

}