<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

class Whatsappapi extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('whatsapp_model');
    }

    public function index()
    {
        $data['title'] = _l('wa_api');
        $data['status'] = app\services\WhatsappApi::getStatus();
        $this->load->view('admin/whatsapp/manage', $data);
    }

    public function logout()
    {
        // echo 'logout';
        $logout = app\services\WhatsappApi::logout();
        if($logout) {
            redirect('admin/whatsappapi');
        }
    }

    public function getDialogLabel($dialog_id)
    {
        header('Content-Type:application/json');
        $label = $this->whatsapp_model->getDialogLabel($dialog_id);
        if(!empty($label)) {
            echo json_encode(@CommonConstant::WA_DIALOG_STATUS_LISTS[$label->label]);
        } else {
            echo json_encode([]);
        }
        exit;
    }
    public function getDialogLabelRaw($dialog_id)
    {
        header('Content-Type:application/json');
        $label = $this->whatsapp_model->getDialogLabel($dialog_id);
        if(!empty($label)) {
            echo json_encode($label->label);
        } else {
            echo json_encode([]);
        }
        exit;
    }

    public function saveStatus() 
    {
        header('Content-Type:application/json');
        if($this->whatsapp_model->changeStatus()) {
            echo json_encode([
                'success' => true,
                'message' => 'Berhasil Ubah Status',
                'label' => CommonConstant::WA_DIALOG_STATUS_LISTS[$this->input->post('status')],
                'key' => $this->input->post('status'),
                'dialog_id' => $this->input->post('dialog_id'),
            ]);
        } else {
            echo json_encode([
                'success' => false,
                'message' => 'Gagal Ubah Status, Silahkan coba lagi nanti',
            ]);
        }
    }

    public function whatsappChats()
    {
        if (!has_permission('whatsapp_management', '', 'view')) {
            access_denied('whatsapp_management');
        }

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('whatsapp_dialogs');
        }

        $data['title']              = _l('whatsapp_chat_list');
        $data['all_number']         = $this->whatsapp_model->getSourceNumber();

        $this->load->view('admin/whatsapp/whatsapp_dialog', $data);
    }

    public function whatsappCalls()
    {
        if (!has_permission('whatsapp_management', '', 'view')) {
            access_denied('whatsapp_management');
        }

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('whatsapp_calls');
        }

        $data['title']              = _l('whatsapp_call_list');

        $this->load->view('admin/whatsapp/whatsapp_call', $data);
    }

    public function whatsappAssignments()
    {
        if (!has_permission('whatsapp_management', '', 'view')) {
            access_denied('whatsapp_management');
        }

        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('whatsapp_cs_assignment');
        }

        $data['title']              = _l('whatsapp_cs_assignment');

        $this->load->view('admin/whatsapp/whatsapp_cs_assignment', $data);
    }

    public function getRawChatData($id)
    {
        header('Content-Type:application/json');
        $chat = $this->whatsapp_model->getRawData($id);
        echo json_encode($chat);
        exit;
    }

    public function getChatByDialog($id)
    {
        header('Content-Type:application/json');
        $chat = $this->whatsapp_model->getChatByDialogId($id);
        echo json_encode($chat);
        exit;
    }

    public function getCsAssignment($id)
    {
        header('Content-Type:application/json');
        $assignment = $this->whatsapp_model->getAssignmentData($id);
        echo json_encode($assignment);
        exit;
    }

    public function setAssignmentCsBulk()
    {
        if(!empty($this->input->post('dialog_ids'))) {
            foreach($this->input->post('dialog_ids') as $id) {
                $data = [
                    'staff_id'  => $this->input->post('staff_id'),
                ];
                // $this->whatsapp_model->updateData($data,$id)
                if($this->whatsapp_model->setAssignmentStaff($data,$id)) {
                    $response['success'] = true;
                    $response['message'] = 'Data Updated!';
                } else {
                    $response['success'] = false;
                    $response['message'] = 'Something went wrong';
                    break;
                }
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function setAssignmentCsBulkFromDialog() {
        if(!empty($this->input->post('dialog_ids'))) {
            foreach($this->input->post('dialog_ids') as $id) {
                $data = [
                    'staff_id'  => $this->input->post('staff_id'),
                ];
                // $this->whatsapp_model->updateDataBulkWaLog($data,$id)
                if($this->whatsapp_model->setAssignmentStaff($data,$id)) {
                    $response['success'] = true;
                    $response['message'] = 'Data Updated!';
                } else {
                    $response['success'] = false;
                    $response['message'] = 'Something went wrong';
                    break;
                }
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function setAssignmentCs()
    {
        $id = $this->input->post('id');
        $data = [
            'staff_id'  => $this->input->post('staff_id'),
        ];
        if($this->whatsapp_model->updateData($data,$id)) {
            $response['success'] = true;
            $response['message'] = 'Data Updated!';
        } else {
            $response['success'] = false;
            $response['message'] = 'Something went wrong';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function sendMessage($chatId) {
        $body = $this->input->get('body');
        $chats = app\services\WhatsappApi::sendMessage($chatId,$body);
        echo json_encode($chats);
    }

    public function getChat($chatId) {
        $chats = app\services\WhatsappApi::getDialogChatById($chatId,10,0);
        echo json_encode($chats);
    }

    public function getMoreChat() {
        $chatId = $this->input->get('chatId');
        $page = $this->input->get('page');
        $chats = app\services\WhatsappApi::getDialogChatById($chatId,10,$page);
        echo json_encode($chats);
    }

    public function getMoreDialog() {
        $page = $this->input->get('page');
        
        //old method
        // if(is_admin(get_staff_user_id())) {
        //     $dialogs = app\services\WhatsappApi::getDialogs(9,$page);
        //     $this->whatsapp_model->saveDialogs($dialogs);
        // } else {
        //     $page*=9;
        //     $dialogs = $this->whatsapp_model->getDialogByStaffId(get_staff_user_id(),9,$page);
        // }

        //new
        $dialogs = app\services\WhatsappApi::getDialogs(9,$page);
        $this->whatsapp_model->saveDialogs($dialogs);

        //tes
        // $page*=9;
        // $dialogs = $this->whatsapp_model->getDialogByStaffId(2,9,$page);

        echo json_encode($dialogs);
    }

    public function searchDialogFromDb() {
        $page = $this->input->get('page');
        $contact = $this->input->get('contact');
        $dialogs = $this->whatsapp_model->searchDialogs($contact,20,$page);
        echo json_encode($dialogs);
    }

    public function getDialogByDialogIdFromDb($dialog_id)
    {
        $data = $this->whatsapp_model->getDialogByDialogId($dialog_id);
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    public function info()
    {
        $data = app\services\WhatsappApi::getInfo();
        print_r($data);
    }

    public function migrate()
    {
        header('Content-Type: application/json');
        $page   = $this->input->get('page');
        $limit  = $this->input->get('limit');
        $order  = $this->input->get('order');
        $message= $this->input->get('message');

        if(empty($limit) || empty($order) || empty($message)) {
            echo json_encode(['error'=>'you must fill limit, message, and order argument']);
            exit;
        }

        $data           = app\services\WhatsappApi::getDialogs($limit,$page,$order);
        $phoneNumber    = str_replace('@c.us','',app\services\WhatsappApi::getInfo()->id);
        $chats          = [];
        $loop           = 0;
        foreach($data as $dialog) {
            $data[$loop]->chats = app\services\WhatsappApi::getDialogChatById($dialog->id,$message,0);
            $loop++;
        }
        // echo json_encode($data);exit;

        $dialogs = [];
        foreach($data as $dialog) {
            $dialogs[] = [
                'dialog_id' => @$dialog->id,
                'name'      => @$dialog->name,
                'image'     => !empty(@$dialog->image) ? @$dialog->image : site_url().'assets/images/user-placeholder.jpg',
                'metadata'  => @json_encode($dialog->metadata),
                'last_time' => @$dialog->last_time,
                'label'     => CommonConstant::WA_DIALOG_STATUS_LEADS,
                'followup_note' => NULL,
                'raw'           => @json_encode($dialog),
                'fu_status_id'  => NULL,
                'followup_id'   => NULL,
                'followup_time' => NULL,
                'closing_time'  => NULL,
                'closing_id'    => NULL,
                'source'        => @$phoneNumber
            ];
            if(!empty($dialog->chats)) {
                $temp = [];
                foreach($dialog->chats as $chat) {
                    $temp[] = [
                        'chat_id'       => @$chat->id,
                        'dialog_id'     => @$dialog->id,
                        'sender'        => @$chat->senderName,
                        'senderName'    => @$chat->senderName,
                        'body'          => @$chat->body,
                        'fromMe'        => @$chat->fromMe,
                        'self'          => @$chat->self,
                        'isForwarded'   => @$chat->isForwarded,
                        'time'          => @$chat->time,
                        'messageNumber' => @$chat->messageNumber,
                        'type'          => @$chat->type,
                        'caption'       => @$chat->caption,
                        'quotedMsgBody' => @$chat->quotedMsgBody,
                        'quotedMsgId'   => @$chat->quotedMsgId,
                        'quotedMsgType' => @$chat->quotedMsgType,
                        'chatName'      => @$chat->chatName,
                        'raw'           => @json_encode($chat)
                    ];
                }
                $this->whatsapp_model->saveChatBackup($temp);
            }
        }
        try {
            $this->whatsapp_model->saveDialogBackup($dialogs);
            echo json_encode($dialogs);
        } catch (\Throwable $th) {
            echo '!';
        }

        exit;
    }
}
