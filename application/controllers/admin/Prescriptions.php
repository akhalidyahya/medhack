<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

class Prescriptions extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['prescriptions_model', 'appointment_model']);
    }

    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('prescriptions');
        }
        $data['title']                      = _l('prescriptions');
        $data['history_count']              = $this->prescriptions_model->getDataCount();
        $data['history_patient']            = $this->prescriptions_model->getDataPatientCount();

        $this->load->view('admin/prescriptions/manage', $data);
    }

    public function getPrescriptionById($id)
    {
        echo json_encode($this->prescriptions_model->getDataById($id));
    }

    public function getPrescriptionByAppointmentId($id)
    {
        echo json_encode($this->prescriptions_model->getDataByAppointmentId($id));
    }

    public function deletePrescription($id)
    {
        if (!has_permission('prescriptions', '', 'edit')) {
            access_denied('prescriptions');
        }
        if (!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $true = true;
        if ($this->prescriptions_model->deleteDataById($id)) {
            // if($true) {
            $response['status'] = true;
            $response['message'] = 'Prescription Data Deleted!';
        } else {
            $response['status'] = false;
            $response['message'] = 'Something Went Wrong';
        }

        echo json_encode($response);
    }

    public function savePrescription()
    {
        if (!has_permission('prescriptions', '', 'edit')) {
            access_denied('prescriptions');
        }
        if (!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();

        $data = [
            'timestamp'     => $this->input->post('timestamp'),
            'patient_id'    => $this->input->post('patient_id'),
            'doctor_id'     => get_staff_user_id(),
            'complaint'     => $this->input->post('complaint'),
            'case_history'  => $this->input->post('case_history'),
            'response'      => $this->input->post('response'),
            'lintah'        => $this->input->post('lintah'),
            'action'        => $this->input->post('action'),
            'note'          => $this->input->post('note'),
            'ramuan'        => $this->input->post('ramuan'),
            'rekomendasi'   => $this->input->post('rekomendasi'),
            'keterangan_gambar'   => $this->input->post('keterangan_gambar'),
            'branch_id'     => $this->input->post('branch_id'),
        ];


        $config['upload_path']          = get_upload_path_by_type('prescriptions');
        $config['allowed_types']        = 'gif|jpg|png';
        $config['overwrite']            = true;
        $config['max_size']             = 1024;
        $this->load->library('upload', $config);

        if (!empty($_FILES['userfile']['name'])) {
            if (!$this->upload->do_upload('keterangan_gambar')) {
                $error = array('error' => $this->upload->display_errors());
                echo json_encode($error);
                die();
            } else {
                $upload = $this->upload->data();
                $data['keterangan_gambar'] = $upload['file_name'];
            }
        }

        if ($this->input->post('method') == 'add') {
            $lastId = $this->prescriptions_model->insertData($data);
            if ($lastId) {
                if(!empty($this->input->post('appointment_id'))) {
                    $appointment_id = explode('#',$this->input->post('appointment_id'))[3];
                    $this->appointment_model->updateAppointment(['history_id' => $lastId], $appointment_id);
                }
                $response['message'] = 'Prescription Data Added!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        } else {
            $id = $this->input->post('id');
            if ($this->prescriptions_model->updateData($data, $id)) {
                $response['message'] = 'Prescription Data Updated!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function savePrescriptionByDoctor()
    {
        if (!$this->input->is_ajax_request()) {
            echo 'not ajax request!';
            exit;
        }
        $response = setCommonResponse();
        $data = [
            'timestamp'     => '', //$this->input->post('timestamp'),
            'patient_id'    => '', //$this->input->post('h_patient_id'),
            'doctor_id'     => $this->input->post('doctor_id'),
            'complaint'     => $this->input->post('complaint'),
            'case_history'  => $this->input->post('case_history'),
            'response'      => $this->input->post('response'),
            'lintah'        => $this->input->post('lintah'),
            'action'        => $this->input->post('action'),
            'note'          => $this->input->post('note'),
            'ramuan'        => $this->input->post('ramuan'),
            'rekomendasi'   => $this->input->post('rekomendasi'),
            'keterangan_gambar'   => $this->input->post('keterangan_gambar'),
        ];

        if(!empty($this->input->post('keterangan_gambar'))) {
            $config['upload_path']          = get_upload_path_by_type('prescriptions');
            $config['allowed_types']        = 'gif|jpg|png';
            $config['overwrite']            = true;
            $config['max_size']             = 1024;
            $this->load->library('upload', $config);
    
            if (!$this->upload->do_upload('keterangan_gambar')) {
                $error = array('error' => $this->upload->display_errors());
                echo json_encode($error);
                die();
            } else {
                $upload = $this->upload->data();
                $data['keterangan_gambar'] = $upload['file_name'];
            }
        }

        $appointment_id = $this->input->post('appointment_id');
        $appointment    = $this->appointment_model->get_appointment_by_id($appointment_id);
        $data['timestamp']  = $appointment->timestamp;
        $data['doctor_id']  = $appointment->doctor_id;
        $data['patient_id'] = $appointment->patient_id;
        $data['branch_id']  = @$appointment->branch_id;

        if (empty($appointment->history_id)) {
            $history_id = $this->prescriptions_model->insertData($data);
            if (!empty($history_id)) {
                $this->appointment_model->updateAppointment(['history_id' => $history_id], $appointment_id);
                $response['message'] = 'Data treatment Disimpan!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        } else {
            if ($this->prescriptions_model->updateData($data, $appointment->history_id)) {
                $response['message'] = 'Data treatment Disimpan!';
            } else {
                $response['success'] = false;
                $response['message'] = 'Something Went wrong';
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
}
