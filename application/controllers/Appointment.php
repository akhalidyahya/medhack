<?php

defined('BASEPATH') or exit('No direct script access allowed');
use app\constants\CommonConstant;


class Appointment extends ClientsController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['appointment_model','staff_model']);
    }

    public function index()
    {
        $data = $this->common_data();
        $data['title'] = 'Buat Janji';
        $this->data($data);
        $this->view('appointment');
        $this->layout();
    }

    public function common_data()
    {
        $data['appointment'] = $this->appointment_model->get(get_contact_user_id());
        $data['doctor'] = get_staff_role(CommonConstant::ADMIN_ROLE_DOCTOR);
        $data['branch'] = $this->appointment_model->get_branch();
        return $data;
    }

    public function list_appointment()
    {
        $event_data = $this->appointment_model->get(get_contact_user_id());
        foreach ($event_data as $key => $value) {
            $data[] = array(
                'id' => $value->id,
                'title' => $value->doctor_firstname.' '.$value->doctor_lastname.' '.date("H:i", strtotime($value->date)),
                'start' => date("Y-m-d", strtotime($value->date)),
            );

        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function list_doctor()
    {
        $branch_id = $this->input->post('branch_id',TRUE);
        $data = $this->appointment_model->get_doctor($branch_id);
        echo json_encode($data);
    }

    public function add()
    {
        $datetime = $this->input->post('datetime');
        // $doctor = $this->input->post('doctor');
        $message = $this->input->post('message');
        $branch_id = $this->input->post('branch_id');
        $data = array(
            'timestamp' => $datetime,
            // 'doctor_id' => $doctor,
            'patient_id' => get_contact_user_id(),
            'message' => $message,
            'branch_id' => $branch_id,
            'status' => CommonConstant::APPOINTMENT_STATUS_PENDING
        );
        $this->appointment_model->add($data);

        //add notification
        $staffs = $this->staff_model->getAllStaff();
        foreach($staffs as $staff) {
            if (has_permission('appointment', $staff->staffid, 'view') && $staff->role != 4) {
                add_notification([
                    'description' => 'Pasien '.get_contact_full_name(get_contact_user_id()).' baru saja membuat janji!',
                    'touserid'    => $staff->staffid,
                    'link'        => 'appointments/',
                ]);
            }
        }

        $response = $this->common_data();
        header('Content-Type: application/json');
        echo json_encode($response);
    }


}