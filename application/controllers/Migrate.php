<?php

defined('BASEPATH') or exit('No direct script access allowed');
use app\constants\CommonConstant;


class Migrate extends ClientsController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('whatsapp_model');
    }

    public function index() 
    {
        echo 'tes if this is worked!';
    }

    public function migrate($page,$limit,$order,$message)
    {
        header('Content-Type: application/json');
        // $page   = $this->input->get('page');
        // $limit  = $this->input->get('limit');
        // $order  = $this->input->get('order');
        // $message= $this->input->get('message');

        if(empty($limit) || empty($order) || empty($message)) {
            echo json_encode(['error'=>'you must fill limit, message, and order argument']);
            exit;
        }

        // echo $page.' # '.$limit.' # '.$order.' # '.$message;exit;

        $data           = app\services\WhatsappApi::getDialogs($limit,$page,$order);
        $phoneNumber    = str_replace('@c.us','',app\services\WhatsappApi::getInfo()->id);
        $chats          = [];
        $loop           = 0;
        foreach($data as $dialog) {
            $data[$loop]->chats = app\services\WhatsappApi::getDialogChatById($dialog->id,$message,0);
            $loop++;
        }

        $dialogs = [];
        foreach($data as $dialog) {
            $dialogs[] = [
                'dialog_id' => @$dialog->id,
                'name'      => @$dialog->name,
                'image'     => !empty(@$dialog->image) ? @$dialog->image : site_url().'assets/images/user-placeholder.jpg',
                'metadata'  => @json_encode($dialog->metadata),
                'last_time' => @$dialog->last_time,
                'label'     => CommonConstant::WA_DIALOG_STATUS_LEADS,
                'followup_note' => NULL,
                'raw'           => @json_encode($dialog),
                'fu_status_id'  => NULL,
                'followup_id'   => NULL,
                'followup_time' => NULL,
                'closing_time'  => NULL,
                'closing_id'    => NULL,
                'source'        => @$phoneNumber
            ];
            if(!empty($dialog->chats)) {
                $temp = [];
                foreach($dialog->chats as $chat) {
                    $temp[] = [
                        'chat_id'       => @$chat->id,
                        'dialog_id'     => @$dialog->id,
                        'sender'        => @$chat->senderName,
                        'senderName'    => @$chat->senderName,
                        'body'          => @$chat->body,
                        'fromMe'        => @$chat->fromMe,
                        'self'          => @$chat->self,
                        'isForwarded'   => @$chat->isForwarded,
                        'time'          => @$chat->time,
                        'messageNumber' => @$chat->messageNumber,
                        'type'          => @$chat->type,
                        'caption'       => @$chat->caption,
                        'quotedMsgBody' => @$chat->quotedMsgBody,
                        'quotedMsgId'   => @$chat->quotedMsgId,
                        'quotedMsgType' => @$chat->quotedMsgType,
                        'chatName'      => @$chat->chatName,
                        'raw'           => @json_encode($chat)
                    ];
                }
                $this->whatsapp_model->saveChatBackup($temp);
            }
        }
        try {
            $this->whatsapp_model->saveDialogBackup($dialogs);
            echo json_encode($dialogs);
        } catch (\Throwable $th) {
            echo '!';
        }

        exit;
    }


}