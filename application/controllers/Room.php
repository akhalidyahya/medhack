<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Room extends ClientsController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_model');
    }

    public function index()
    {
        $data['riwayat'] = $this->room_model->get();
        $this->data($data);
        $this->view('room');
        $this->layout();
    }
}