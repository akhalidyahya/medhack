<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Room_model extends App_Model
{
    public function get()
    {
        $this->db->select('b.bed_allotment_id as id, b.allotment_timestamp as waktu_masuk, b.discharge_timestamp as waktu_keluar, bed.bed_number as bed_no, bt.name as type');
        $this->db->from('bed_allotment as b');
        $this->db->where('b.patient_id', get_contact_user_id());
        $this->db->join('bed', 'bed.bed_id = b.bed_id', 'LEFT');
        $this->db->join('bed_types as bt', 'bt.id = bed.type', 'LEFT');
        $query = $this->db->get();

        return $query->result();
    }
}