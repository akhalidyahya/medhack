<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Prescriptions_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = db_prefix() . 'prescriptions';
    }

    private $table = '';

    public function getDataCount()
    {        
        $sql = 'SELECT prescription_id FROM '.$this->table.' WHERE doctor_id = '.get_staff_user_id().' ';
        
        if(is_admin()) {
            $sql = 'SELECT prescription_id FROM ' . $this->table;
        }

        return $this->db->query($sql)->num_rows();
    }

    public function getDataPatientCount()
    {
        $sql = 'SELECT DISTINCT(patient_id) FROM '.$this->table.' WHERE doctor_id = '.get_staff_user_id().' ';
        
        if(is_admin()) {
            $sql = 'SELECT DISTINCT(patient_id) FROM ' . $this->table;
        }

        return $this->db->query($sql)->num_rows();
    }

    public function getDataById($id)
    {
        $this->db->select(''.db_prefix().'prescriptions.*,CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name');
        $this->db->join(db_prefix().'contacts',''.db_prefix().'contacts.id = '.db_prefix().'prescriptions.patient_id','left');
        $this->db->where(db_prefix().'prescriptions.prescription_id',$id);
        
        return $this->db->get($this->table)->row();
    }

    public function getDataByAppointmentId($id)
    {
        $this->db->select(''.db_prefix().'prescriptions.*,CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name');
        $this->db->join(db_prefix().'contacts',''.db_prefix().'contacts.id = '.db_prefix().'prescriptions.patient_id','left');
        $this->db->join(db_prefix().'appointment',''.db_prefix().'appointment.history_id = '.db_prefix().'prescriptions.prescription_id','left');
        $this->db->where(db_prefix().'appointment.appointment_id',$id);
        
        return $this->db->get($this->table)->row();
    }

    public function deleteDataById($id)
    {
        $this->db->where('prescription_id', $id);
        if($this->db->delete($this->table)) {
            return true;
        } else {
            return false;
        }
    }

    public function insertData($data)
    {
        if($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function updateData($data,$id)
    {
        $this->db->where('prescription_id', $id);
        if($this->db->update($this->table, $data)) {
            return true;
        } else {
            return false;
        }
    }
}