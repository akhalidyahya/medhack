<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Doctor_model extends App_Model
{
    public function get_doctor($staff_id)
    {
        $this->db->where('role', $staff_id);
        return $this->db->get("staff")->result();
    }

    public function get_staffbranch()
    {
        return $this->db->get("staff_branch")->result();
    }

    public function get_branch()
    {
        return $this->db->get("branchs")->result();
    }
}