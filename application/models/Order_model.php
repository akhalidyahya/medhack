<?php

use app\constants\CommonConstant;
use Carbon\Carbon;

defined('BASEPATH') or exit('No direct script access allowed');

class Order_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataCount()
    {
        return $this->db->query('SELECT id FROM '.db_prefix() .'invoices')->num_rows();
    }

    public function changeStatus($id,$status)
    {
        $data = [
            'status' => $status
        ];
        $this->db->where('id', $id);
        $this->db->update(db_prefix().'invoices', $data);

        if(!empty($this->getOrdermasterByInvoiceId($id))) {
            $this->db->where('invoice_id',$id);
            $this->db->update(db_prefix().'order_master', $data);
        }

        if($status == CommonConstant::INVOICE_STATUS_PAID) {
            $invoicepaymentrecordsdata = [
                'invoiceid'     => $id,
                'amount'        => $this->getDataById($id)->total,
                'paymentmode'   => 1,
                'paymentmethod' => '-',
                'date'          => date('Y-m-d'),
                'daterecorded'  => Carbon::now(),
                'note'          => '-',
                // 'transactionid' => '',
            ];
            $this->db->insert(db_prefix() . 'invoicepaymentrecords', $invoicepaymentrecordsdata);
        }
        
        return true;
    }

    public function getAllData()
    {
        return $this->db->query('SELECT * FROM '.db_prefix() .' invoices')->result();
    }

    public function getDataById($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get(db_prefix() . 'invoices')->row();
    }

    public function getOrdermasterByInvoiceId($invoice_id)
    {
        $this->db->select('*');
        $this->db->where('invoice_id',$invoice_id);
        return $this->db->get(db_prefix() . 'order_master')->row();
    }

}