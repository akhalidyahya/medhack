<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

class Room_management_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = db_prefix() . 'bed_allotment';
        $this->table_master = db_prefix() . 'bed';
        $this->table_type = db_prefix() . 'bed_types';
    }

    private $table = '';
    private $table_master = '';
    private $table_type = '';

    public function getDataCount()
    {        
        $sql = 'SELECT appointment_id FROM '.db_prefix() .'appointment WHERE doctor_id = '.get_staff_user_id().' ';
        
        if(is_admin()) {
            $sql = 'SELECT appointment_id FROM '.db_prefix() .'appointment';
        }

        return $this->db->query($sql)->num_rows();
    }

    public function getAllBeds()
    {
        $this->db->select('*');
        $this->db->from($this->table_master);
        return $this->db->get()->result();
    }

    public function getAvailableRoom($date)
    {
        $this->db->select('bed_id');
        $this->db->from($this->table);
        $this->db->where('discharge_timestamp >=',$date);
        $occupied_beds = $this->db->get()->result();
        $occupied_beds_array = [];
        foreach($occupied_beds as $bed) {
            $occupied_beds_array[] = $bed->bed_id;
        }
        $this->db->select('*');
        $this->db->from($this->table_master);
        if(!empty($occupied_beds_array)) {
            $this->db->where_not_in('bed_id',$occupied_beds_array);
        }
        return $this->db->get()->result();
    }

    public function getDataAllotmentById($id)
    {
        $this->db->select($this->table.'.bed_allotment_id as id,'.$this->table.'.allotment_timestamp,'.$this->table.'.discharge_timestamp,'.$this->table_master.'.bed_number,'.$this->table_master.'.bed_id,'.'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name,'.$this->table.'.patient_id');
        $this->db->where('bed_allotment_id',$id);
        $this->db->join(db_prefix().'contacts',db_prefix().'contacts.id = '.$this->table.'.patient_id','left');
        $this->db->join($this->table_master,$this->table_master.'.bed_id = '.$this->table.'.bed_id','left');
        
        return $this->db->get($this->table)->row();
    }

    public function insertDataAllotment($data)
    {
        if($this->db->insert($this->table, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateDataAllotment($data,$id)
    {
        $this->db->where('bed_allotment_id', $id);
        if($this->db->update($this->table, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteAllotmentById($id)
    {
        $this->db->where('bed_allotment_id', $id);
        if($this->db->delete($this->table)) {
            return true;
        } else {
            return false;
        }
    }

    public function getMasterCount()
    {
        $this->db->select('bed_id');
        $this->db->from($this->table_master);
        return $this->db->get()->num_rows();
    }

    public function getDataMasterById($id)
    {
        $this->db->select('*');
        $this->db->where('bed_id',$id);
        
        return $this->db->get($this->table_master)->row();
    }

    public function deleteDataMasterById($id)
    {
        $this->db->where('bed_id', $id);
        if($this->db->delete($this->table_master)) {
            return true;
        } else {
            return false;
        }
    }

    public function insertDataMaster($data)
    {
        if($this->db->insert($this->table_master, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateDataMaster($data,$id)
    {
        $this->db->where('bed_id', $id);
        if($this->db->update($this->table_master, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getCalendarData($start,$end,$selectedUser,$selectedRoom)
    {
        $data = [];
        $schedules = $this->get_all_events($start,$end,$selectedUser,$selectedRoom);
        foreach ($schedules as $schedule) {
            $_data = [];
            $_data['is_not_creator'] = true;
            $_data['onclick']        = true;
            $_data['_tooltip']       = date('d F Y', strtotime($schedule['allotment_timestamp'])) .' - '.  date('d F Y', strtotime($schedule['discharge_timestamp']));

            $_data['color']    = '#5d78ff';
            $status            = CommonConstant::ALLOTMENT_STATUS_ON_GOING;
            if($schedule['discharge_timestamp'] <= Date('Y-m-d H:i')) {
                $_data['color']    = '#999';
                $status            = CommonConstant::ALLOTMENT_STATUS_DONE;
            } else if($schedule['allotment_timestamp'] >= Date('Y-m-d H:i')) {
                $_data['color']    = '#f0a70c';
                $status            = CommonConstant::ALLOTMENT_STATUS_WILL_OCCUR;
            }

            $_data['title']    = $schedule['bed_number'].' - '.$schedule['full_name'].' ('.CommonConstant::ALLOTMENT_STATUS_LIST[$status].')';
            $_data['start']    = $schedule['allotment_timestamp'];
            $_data['end']      = $schedule['discharge_timestamp'];
            $_data['eventid']  = $schedule['bed_allotment_id'];
            $_data['public']   = 0;
            array_push($data, $_data);
        }

        return hooks()->apply_filters('calendar_data', $data, [
            'start'      => $start,
            'end'        => $end,
        ]);
    }

    public function get_all_events($start,$end,$selectedUser,$selectedRoom)
    {
        $this->db->select('ba.*,CONCAT('.db_prefix().'c.firstname, \' \', '.db_prefix().'c.lastname) as full_name,b.bed_number');
        $this->db->join(db_prefix().'contacts c','c.id = ba.patient_id');
        $this->db->join(db_prefix().'bed b','b.bed_id = ba.bed_id');
        // $this->db->where('ba.timestamp >= ',$start);
        if(!empty($selectedUser)) {
            $this->db->where('ba.patient_id',$selectedUser);
        }
        if(!empty($selectedRoom)) {
            $this->db->where('ba.bed_id',$selectedRoom);
        }

        return $this->db->get($this->table.' ba')->result_array();
    }

    public function getBedTypes()
    {
        $this->db->select('*');
        $this->db->from($this->table_type);
        return $this->db->get()->result();
    }

    public function insertDataBedType($data)
    {
        if($this->db->insert($this->table_type, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getBedTypeById($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        
        return $this->db->get($this->table_type)->row();
    }

    public function updateDataBedType($data,$id)
    {
        $this->db->where('id', $id);
        if($this->db->update($this->table_type, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteBedTypeById($id)
    {
        $this->db->where('id', $id);
        if($this->db->delete($this->table_type)) {
            return true;
        } else {
            return false;
        }
    }
}