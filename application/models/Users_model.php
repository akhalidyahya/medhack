<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTotalCount()
    {
        $sql = 'SELECT id FROM '.db_prefix() .'contacts';
        if(has_permission('users', '', 'view_branch')) {
            $ids    = getStaffBrachIdString(get_staff_user_id());
            if(is_admin()) {
                $sql = 'SELECT id FROM '.db_prefix() .'contacts';
            } else {
                $sql = 'SELECT id FROM '.db_prefix() .'contacts WHERE branch_id IN ('.$ids.')';
            }
        }
        return $this->db->query($sql)->num_rows();
    }

    public function getAllData()
    {
        return $this->db->query('SELECT * FROM '.db_prefix() .' contacts')->result();
    }

    public function getDataById($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        
        return $this->db->get(db_prefix() . 'contacts')->row();
    }

    public function deleteDataById($id)
    {
        $this->db->where('id', $id);
        if($this->db->delete(db_prefix() . 'contacts')) {
            return true;
        } else {
            return false;
        }
    }

    public function insertData($data)
    {
        if($this->db->insert(db_prefix() . 'contacts', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateData($data,$id)
    {
        $this->db->where('id', $id);
        if($this->db->update(db_prefix() . 'contacts', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function search($name)
    {
        // $name_array = explode(' ',$name);
        $this->db->select('CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name, contacts.id');
        $this->db->like('CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname)', $name);
        // if(count($name_array)>0) {
        //     foreach($name_array as $s_name){
        //         $this->db->or_like('firstname', $s_name);
        //         $this->db->or_like('lastname', $s_name);
        //     }
        // }
        
        $results = $this->db->get(db_prefix() . 'contacts')->result_array();

        $items = [];

        foreach ($results as $key => $result) {
            $items[$key]['name']        = $result['full_name'];
            $items[$key]['id']          = $result['id'];
        }

        return $items;
    }
}