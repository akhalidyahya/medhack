<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Shipping_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataCount()
    {
        return $this->db->query('SELECT id FROM '.db_prefix() .'order_master')->num_rows();
    }

    public function changeStatus($id,$status)
    {
        $data = [
            'status' => $status
        ];
        $this->db->where('id', $id);
        $this->db->update(db_prefix().'order_master', $data);

        if(!empty($this->getOrdermasterByInvoiceId($id))) {
            $this->db->where('invoice_id',$id);
            $this->db->update(db_prefix().'order_master', $data);
        }
        
        return true;
    }

    public function getAllData()
    {
        return $this->db->query('SELECT * FROM '.db_prefix() .' order_master')->result();
    }

    public function getDataById($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get(db_prefix() . 'order_master')->row();
    }

    public function getOrdermasterByInvoiceId($invoice_id)
    {
        $this->db->select('*');
        $this->db->where('invoice_id',$invoice_id);
        return $this->db->get(db_prefix() . 'order_master')->row();
    }

    public function addResi($id, $resi)
    {
        $this->db->set([
            'shipping_receipt_number'=>$resi,
            'shipping_status'=>1
        ]);
        $this->db->where('id', $id);
		$this->db->update('order_master');
    }

}