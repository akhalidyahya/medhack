<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

class Customer_services_model extends App_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getCountDialogByStatus($status)
    {
        return $this->db->query('SELECT id FROM '.db_prefix() .'whatsapp_dialogs WHERE label = "'.$status.'"')->num_rows();
    }

    public function getAllFuStatus()
    {
        $this->db->select('*');
        return $this->db->get(db_prefix() . 'fu_summary_status')->result();
    }

    public function getAllFuStatusStatistik($staff_id = NULL)
    {
        $where = " WHERE whatsapp_dialogs.label='".CommonConstant::WA_DIALOG_STATUS_FU."' ";
        if(false) {
            // $where += " AND whatsapp_dialogs.followup_id = 2 ";
            $time_start = now();
            $where .= " AND whatsapp_dialogs.followup_time >= $time_start ";
            $time_end = now();
            $where .= " AND whatsapp_dialogs.followup_time <= $time_end ";
        }
        if(!empty($staff_id)) {
            $where .= " AND whatsapp_dialogs.followup_id = $staff_id ";
        }
        return $this->db->query("
                SELECT COUNT(whatsapp_dialogs.id) as jumlah,fu_summary_status.status as status FROM whatsapp_dialogs
                    LEFT JOIN fu_summary_status ON fu_summary_status.id = whatsapp_dialogs.fu_status_id
                    $where
                    GROUP BY whatsapp_dialogs.fu_status_id
            ")
            ->result();
    }

    public function getStatusFuById($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        
        return $this->db->get(db_prefix() . 'fu_summary_status')->row();
    }

    public function deleteStatusFuById($id)
    {
        $this->db->where('id', $id);
        if($this->db->delete(db_prefix() . 'fu_summary_status')) {
            return true;
        } else {
            return false;
        }
    }

    public function insertStatusFu($data)
    {
        if($this->db->insert(db_prefix() . 'fu_summary_status', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateStatusFu($data,$id)
    {
        $this->db->where('id', $id);
        if($this->db->update(db_prefix() . 'fu_summary_status', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllStaff()
    {
        $this->db->select('staffid, CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as fullname');
        $this->db->where('role',CommonConstant::ADMIN_ROLE_CUSTOMER_SERVICE);
        if(!has_permission('whatsapp_management', '', 'statistic')) {
            $this->db->where('staffid',get_staff_user_id());
        }
        return $this->db->get(db_prefix() . 'staff')->result();
    }

    public function saveBranchChat($data,$id)
    {
        $this->db->where('dialog_id', $id);
        if($this->db->update(db_prefix() . 'whatsapp_dialogs', $data)) {
            return true;
        } else {
            return false;
        }
    }
}
