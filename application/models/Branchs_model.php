<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Branchs_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBranchCount()
    {
        return $this->db->query('SELECT id FROM '.db_prefix() .'branchs')->num_rows();
    }

    public function getAllData($array = false)
    {
        if($array) {
            return $this->db->query('SELECT * FROM '.db_prefix() .' branchs')->result_array();    
        }
        return $this->db->query('SELECT * FROM '.db_prefix() .' branchs')->result();
    }

    public function getBranchById($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get(db_prefix() . 'branchs')->row();
    }

    public function deleteBranchById($id)
    {
        $this->db->where('id', $id);
        if($this->db->delete(db_prefix() . 'branchs')) {
            return true;
        } else {
            return false;
        }
    }

    public function insertData($data)
    {
        if($this->db->insert(db_prefix() . 'branchs', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateData($data,$id)
    {
        $this->db->where('id', $id);
        if($this->db->update(db_prefix() . 'branchs', $data)) {
            return true;
        } else {
            return false;
        }
    }
}