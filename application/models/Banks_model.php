<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Banks_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTotalCount()
    {
        return $this->db->query('SELECT id FROM '.db_prefix() .'bank')->num_rows();
    }

    public function getAllData()
    {
        return $this->db->query('SELECT * FROM '.db_prefix() .' bank')->result();
    }

    public function getDataById($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        
        return $this->db->get(db_prefix() . 'bank')->row();
    }

    public function deleteDataById($id)
    {
        $this->db->where('id', $id);
        if($this->db->delete(db_prefix() . 'bank')) {
            return true;
        } else {
            return false;
        }
    }

    public function insertData($data)
    {
        if($this->db->insert(db_prefix() . 'bank', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateData($data,$id)
    {
        $this->db->where('id', $id);
        if($this->db->update(db_prefix() . 'bank', $data)) {
            return true;
        } else {
            return false;
        }
    }

    // public function search($name)
    // {
    //     // $name_array = explode(' ',$name);
    //     $this->db->select('CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name, contacts.id');
    //     $this->db->like('CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname)', $name);
    //     // if(count($name_array)>0) {
    //     //     foreach($name_array as $s_name){
    //     //         $this->db->or_like('firstname', $s_name);
    //     //         $this->db->or_like('lastname', $s_name);
    //     //     }
    //     // }
        
    //     $results = $this->db->get(db_prefix() . 'contacts')->result_array();

    //     $items = [];

    //     foreach ($results as $key => $result) {
    //         $items[$key]['name']        = $result['full_name'];
    //         $items[$key]['id']          = $result['id'];
    //     }

    //     return $items;
    // }
}