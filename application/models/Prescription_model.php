<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Prescription_model extends App_Model
{
    public function get()
    {
        $this->db->select('p.prescription_id as id, p.timestamp as date, p.case_history as case_history, p.medication as medication, p.note as note, c.firstname as patient_firstname, c.lastname as patient_lastname, s.firstname as doctor_firstname, s.lastname as doctor_lastname, p.complaint, p.action, p.response, p.lintah');
        $this->db->from('prescriptions as p');
        $this->db->where('p.patient_id', get_contact_user_id());
        $this->db->join('contacts as c', 'c.userid = p.patient_id', 'LEFT');
        $this->db->join('staff as s', 's.staffid = p.doctor_id', 'LEFT');
        $this->db->group_by('p.timestamp');
        $query = $this->db->get();

        return $query->result();
    }
}