<?php

use app\constants\CommonConstant;
use Carbon\Carbon;

defined('BASEPATH') or exit('No direct script access allowed');

class Whatsapp_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getRowCount()
    {
        return $this->db->count_all_results('whatsapp_dialogs');
    }

    public function getData($number,$offset,$userId = NULL)
    {
        $this->db->select('whatsapp_dialogs.*,
                        branchs.branch,
                        CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as staff_followup,
                        CONCAT(' . db_prefix() . 'assignment.firstname, \' \', ' . db_prefix() . 'assignment.lastname) as staff_assignment');
        $this->db->from('whatsapp_dialogs');
        $this->db->join('branchs', 'branchs.id = whatsapp_dialogs.branch_id', 'left');
        $this->db->join('staff', 'staff.staffid = whatsapp_dialogs.followup_id', 'left');
        $this->db->join('staff as assignment', 'assignment.staffid = whatsapp_dialogs.assignment_to', 'left');
        if(!empty($userId)){
            $this->db->join('whatsapp_dialog_staff', 'whatsapp_dialog_staff.dialog_id = whatsapp_dialogs.dialog_id', 'left');
            $this->db->where("(whatsapp_dialog_staff.staff_id = $userId OR whatsapp_dialogs.assignment_to = $userId)");
        }
        return $query = $this->db->order_by('last_time','desc')->limit($number)->offset($offset)->get()->result();
    }

    public function getDialogLabel($dialog_id) {
        $this->db->select('label');
        $this->db->where('dialog_id', $dialog_id);
        return $this->db->get('whatsapp_dialogs')->row();
    }
    
    public function saveDialogs($dialogs) {
        if(!empty($dialogs)) {
            foreach($dialogs as $dialog) {
                if(self::checkDialogExist($dialog->id)) {
                    self::updateDialog($dialog);
                } else {
                    self::insertDialog($dialog);
                }
            }
        }
    }

    public function checkDialogExist($dialog_id) {
        $rows = $this->db->query("SELECT id FROM ".db_prefix() . " whatsapp_dialogs WHERE dialog_id = '$dialog_id'")->num_rows();
        if($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function insertDialog($dialog) {
        $data = [
            'last_time' => $dialog->last_time,
            'name' => $dialog->name,
            'metadata' => json_encode($dialog->metadata),
            'image' => $dialog->image,
            'raw' => json_encode($dialog),
            'dialog_id' => $dialog->id,
            'label' => CommonConstant::WA_DIALOG_STATUS_LEADS
        ];

        $this->db->insert(db_prefix() . 'whatsapp_dialogs', $data);
    }

    public function updateDialog($dialog) {
        $data = [
            'last_time' => $dialog->last_time,
            'name' => $dialog->name,
            'metadata' => json_encode($dialog->metadata),
            'image' => $dialog->image,
            'raw' => json_encode($dialog)
        ];

        $this->db->where('dialog_id', $dialog->id);
        $this->db->update(db_prefix() . 'whatsapp_dialogs', $data);
    }

    public function getDialogByStaffId($staff_id,$limit,$offset)
    {
        $this->db->select(db_prefix().'whatsapp_dialogs.dialog_id as id,'. db_prefix().'whatsapp_dialogs.name,'. db_prefix().'whatsapp_dialogs.image,'. db_prefix().'whatsapp_dialogs.label,'. db_prefix().'whatsapp_dialogs.last_time,'. db_prefix().'whatsapp_dialogs.label,CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as fullname
        ');
        $this->db->join(db_prefix() . 'whatsapp_dialogs', db_prefix() . 'whatsapp_dialogs.dialog_id = '.db_prefix() . 'whatsapp_dialog_staff.dialog_id','left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = '.db_prefix() . 'whatsapp_dialogs.followup_id','left');
        $this->db->where('staff_id', $staff_id);
        $this->db->order_by(db_prefix() . 'whatsapp_dialogs.last_time', 'desc');
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get(db_prefix() . 'whatsapp_dialog_staff')->result();
    }

    public function searchDialogs($contact,$limit,$offset)
    {
        $this->db->select(
            db_prefix().'whatsapp_dialogs.dialog_id as id,
            '. db_prefix().'whatsapp_dialogs.name,
            '. db_prefix().'whatsapp_dialogs.image,
            '. db_prefix().'whatsapp_dialogs.label,
            '. db_prefix().'whatsapp_dialogs.last_time,
            '. db_prefix().'whatsapp_dialogs.label
        ');
        $this->db->like('name', $contact);
        $this->db->or_like('dialog_id', $contact);
        $this->db->order_by(db_prefix() . 'last_time', 'desc');
        $this->db->limit($limit);
        $this->db->offset($offset);
        $this->db->distinct();
        return $this->db->get(db_prefix() . 'whatsapp_dialogs')->result();
    }

    public function getFollowUpBy($dialog_id)
    {
        $this->db->select('CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as fullname');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = '.db_prefix() . 'whatsapp_dialogs.followup_id','left');
        $this->db->where(db_prefix() . 'whatsapp_dialogs.dialog_id', $dialog_id);
        return $this->db->get(db_prefix() . 'whatsapp_dialogs')->row();
    }

    public function getRawData($id)
    {
        $this->db->select('id,chat_id,dialog_id,sender,senderName,body,fromMe,self,isForwarded,time,messageNumber,type,caption,quotedMsgBody,quotedMsgId,quotedMsgtype,chatName');
        $this->db->where('id',$id);
        return $this->db->get(db_prefix().'whatsapp_chats')->row();
    }

    public function getAssignmentData($id)
    {
        $this->db->select(db_prefix().'whatsapp_dialog_staff.*,'.db_prefix().'whatsapp_dialogs.name as chat_name,'.'CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as full_name');
        $this->db->join(db_prefix().'whatsapp_dialogs',db_prefix().'whatsapp_dialogs.dialog_id = '.db_prefix().'whatsapp_dialog_staff.dialog_id');
        $this->db->join(db_prefix().'staff',db_prefix().'staff.staffid = '.db_prefix().'whatsapp_dialog_staff.staff_id');
        $this->db->where(db_prefix().'whatsapp_dialog_staff.id',$id);
        return $this->db->get(db_prefix().'whatsapp_dialog_staff')->row();
    }

    public function updateData($data,$id)
    {
        $this->db->where('id', $id);
        if($this->db->update(db_prefix() . 'whatsapp_dialog_staff', $data)) {
            $dialog_id = $this->db->where('id',$id)->get('whatsapp_dialog_staff')->row()->dialog_id;
            $this->updateWaDialogs(['followup_time'=>Date('Y-m-d H:i:s'),'followup_id'=>$data['staff_id']],$dialog_id);
            return true;
        } else {
            return false;
        }
    }

    public function setAssignmentStaffByDialogStaffId($data,$id) {
        $this->db->where('id', $id);
        if($this->db->update(db_prefix() . 'whatsapp_dialogs', ['assignment_to' => $data['staff_id']])) {
            return true;
        } else {
            return false;
        }
    }

    public function setAssignmentStaff($data,$id) {
        $this->db->where('dialog_id', $id);
        if($this->db->update(db_prefix() . 'whatsapp_dialogs', ['assignment_to' => $data['staff_id']])) {
            return true;
        } else {
            return false;
        }
    }

    public function updateDataBulkWaLog($data,$id)
    {
        
        if($this->isWaDialogStaffExist($id)) {
            $this->db->where('dialog_id', $id);
            $action = $this->db->update(db_prefix() . 'whatsapp_dialog_staff', $data);
        } else {
            $newData = [
                'staff_id' => $data['staff_id'],
                'dialog_id' => $id
            ];
            $action = $this->db->insert('whatsapp_dialog_staff', $newData);
        }

        if($action) {
            $this->updateWaDialogs(['followup_time'=>Date('Y-m-d H:i:s'),'followup_id'=>$data['staff_id']],$id);
            return true;
        } else {
            return false;
        }
    }

    public function updateWaDialogs($data,$dialog_id) {
        $this->db->where('dialog_id', $dialog_id);
        if($this->db->update(db_prefix() . 'whatsapp_dialogs', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function isWaDialogStaffExist($dialog_id)
    {
        $this->db->where('dialog_id',$dialog_id);
        $exist = $this->db->get('whatsapp_dialog_staff')->num_rows();
        if($exist > 0) {
            return true;
        }

        return false;
    }

    public function changeStatus()
    {
        $exisisting = $this->getDialogByDialogId($this->input->post('dialog_id'));

        $data = [
            'label' =>  $this->input->post('status')
        ];

        if($this->input->post('status') == CommonConstant::WA_DIALOG_STATUS_FU && @$exisisting->label == CommonConstant::WA_DIALOG_STATUS_LEADS) {
            $data['followup_time']  = Carbon::now();
            $data['followup_id']  = get_staff_user_id();
        } elseif($this->input->post('status') == CommonConstant::WA_DIALOG_STATUS_PATIENT) {
            $data['closing_time']  = Carbon::now();
        }

        $this->db->where('dialog_id', $this->input->post('dialog_id'));
        if($this->db->update(db_prefix() . 'whatsapp_dialogs', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getDialogByDialogId($dialog_id)
    {
        $this->db->where('dialog_id',$dialog_id);
        return $this->db->get(db_prefix().'whatsapp_dialogs')->row();
    }

    public function saveNote($data,$dialog_id)
    {
        $this->db->where('dialog_id', $dialog_id);
        if($this->db->update(db_prefix() . 'whatsapp_dialogs', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getDialogCountGroupByFuStatus($staff_id = NULL)
    {
        $sqlWhere = '';
        if(!empty($staff_id)) {
            $sqlWhere = " WHERE followup_id = $staff_id ";
        }
        return $this->db->query("SELECT COUNT(*) as jumlah, fu_status_id, fu_summary_status.status as status
            FROM whatsapp_dialogs
            LEFT JOIN fu_summary_status ON fu_summary_status.id = whatsapp_dialogs.fu_status_id
            $sqlWhere
            GROUP BY fu_status_id
            ORDER BY fu_status_id")->result();
    }

    public function getAllFuStatus()
    {
        $this->db->select('*');
        $this->db->order_by('id','ASC');
        return $this->db->get('fu_summary_status')->result();
    }

    public function saveChatBackup($data_bulk)
    {
        $this->db->insert_batch('whatsapp_chats', $data_bulk);
    }

    public function saveDialogBackup($data_bulk)
    {
        $this->db->insert_batch('whatsapp_dialogs', $data_bulk);
    }

    public function getChatByDialogId($dialog_id,$limit = 5,$offset = 0)
    {
        $this->db->select('*');
        $this->db->where('dialog_id',$dialog_id);
        $this->db->order_by('time','DESC');
        $this->db->limit($limit);
        if(!empty($offset)) {
            $this->db->offset($offset);
        }
        return $this->db->get('whatsapp_chats')->result();
    }

    public function getSourceNumber()
    {
        return $this->db->query("SELECT DISTINCT source as number FROM whatsapp_dialogs WHERE source IS NOT NULL")->result();
    }
}
