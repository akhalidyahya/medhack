<?php

defined('BASEPATH') or exit('No direct script access allowed');
use app\constants\CommonConstant;
use Carbon\Carbon;

class Appointment_model extends App_Model
{

    public function get($user_id)
    {
        $this->db->select('ap.appointment_id as id, ap.patient_id as patient_id, ap.timestamp as date, ap.status as status, ap.message as message, s.firstname as doctor_firstname, s.lastname as doctor_lastname');
        $this->db->from('appointment as ap');
        $this->db->where('ap.patient_id', $user_id);
        $this->db->join('staff as s', 's.staffid = ap.doctor_id', 'LEFT');
        $this->db->group_by('ap.appointment_id');
        $this->db->order_by('ap.timestamp', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_calendar()
    {
        $this->db->where('patient_id', get_client_user_id());
        $this->db->get('appointment')->result();
    }

    public function get_branch()
    {
        return $this->db->get('branchs')->result_array();
    }

    public function get_doctor($branch_id)
    {
        $this->db->select('s.staffid as staffid, s.firstname as doctor_firstname, s.lastname as doctor_lastname, sb.id as staff_branch_id, sb.staff_id as staffid_onbranch, sb.branch_id as branch_id');
        $this->db->from('staff_branch sb');
        $this->db->where('sb.branch_id', $branch_id);
        $this->db->where('s.role', CommonConstant::ADMIN_ROLE_DOCTOR);
        $this->db->join('staff s', 'sb.staff_id = s.staffid', 'LEFT');
        $query = $this->db->get();
        return $query->result();
    }

    public function add($data)
    {
        return $this->db->insert('appointment', $data);
    }

    public function getDataCount()
    {        
        $sql = 'SELECT appointment_id FROM '.db_prefix() .'appointment WHERE doctor_id = '.get_staff_user_id().' ';
        
        if(is_admin()) {
            $sql = 'SELECT appointment_id FROM '.db_prefix() .'appointment';
        }

        return $this->db->query($sql)->num_rows();
    }

    public function getDataCountByStatus($status)
    {        
        $sql = 'SELECT appointment_id FROM '.db_prefix() .'appointment WHERE doctor_id = '.get_staff_user_id().' AND status = "'.$status.'" ';
        
        if(is_admin()) {
            $sql = 'SELECT appointment_id FROM '.db_prefix() .'appointment WHERE status = "'.$status.'" ';
        }

        return $this->db->query($sql)->num_rows();
    }

    public function changeStatus($id,$status)
    {
        $data = [
            'status' => $status
        ];
        $this->db->where('appointment_id', $id);
        return $this->db->update('appointment', $data);
    }

    public function saveDoctor($id,$doctor_id)
    {
        $data = [
            'doctor_id' => $doctor_id
        ];
        $this->db->where('appointment_id', $id);
        return $this->db->update('appointment', $data);
    }

    public function updateAppointment($data,$id)
    {
        $this->db->where('appointment_id', $id);
        return $this->db->update('appointment', $data);
    }

    public function getCalendarDataByUser($start,$end,$user)
    {
        $data = [];
        $appointments = $this->get_all_events($start,$end,$user);
        foreach ($appointments as $appointment) {
            $_data = [];
            if ($appointment['doctor_id'] != get_staff_user_id()) {
                $_data['is_not_creator'] = true;
                $_data['onclick']        = true;
            }
            $_data['_tooltip'] = date('d F Y H:i:s', strtotime($appointment['timestamp']));
            $_data['color']    = '#2eb82e';
            $_data['title']    = $appointment['full_name'];
            $_data['start']    = $appointment['timestamp'];
            $_data['end']      = $appointment['timestamp'];
            $_data['eventid']  = $appointment['appointment_id'];
            $_data['userid']   = $appointment['doctor_id'];
            $_data['public']   = 0;
            array_push($data, $_data);
        }

        return hooks()->apply_filters('calendar_data', $data, [
            'start'      => $start,
            'end'        => $start,
            'client_id'  => 1,
            'contact_id' => get_staff_user_id(),
        ]);
    }

    public function get_all_events($start,$end,$user)
    {
        $this->db->select('a.*,CONCAT('.db_prefix().'c.firstname, \' \', '.db_prefix().'c.lastname) as full_name');
        $this->db->join('contacts c','c.id = a.patient_id');
        $this->db->where('a.timestamp >= ',$start);
        $this->db->where('a.timestamp <= ',$end);
        if(!is_admin()) {
            $this->db->where('a.doctor_id', get_staff_user_id());
        }
        if(!empty($user)) {
            $this->db->where('a.patient_id', $user);
        }
        $this->db->where('a.status', app\constants\CommonConstant::APPOINTMENT_STATUS_APPROVED);

        return $this->db->get(db_prefix() . 'appointment a')->result_array();
        // return $this->db->last_query();
    }

    public function search($name)
    {
        $this->db->select('appointment_id as id, timestamp as date, patient_id, appointment.branch_id as branch_id, CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as fullname');
        
        if(!is_admin()) {
            $this->db->where('doctor_id',get_staff_user_id());
        }

        $this->db->like('timestamp', $name);
        $this->db->or_like('CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname)', $name);

        $this->db->join(db_prefix() . 'contacts','contacts.id = appointment.patient_id');
        
        $this->db->order_by(db_prefix() . 'appointment.timestamp','desc');
        $results = $this->db->get(db_prefix() . 'appointment')->result_array();

        $items = [];

        foreach ($results as $key => $result) {
            $items[$key]['name']        = $result['fullname'].' ('.Carbon::parse($result['date'])->format('d-F-Y H:i:s').')';
            $items[$key]['id']          = $result['patient_id'].'#'.$result['fullname'].'#'.$result['date'].'#'.$result['branch_id'];
        }

        return $items;
    }

    public function get_appointment_by_id($id)
    {
        $this->db->where('appointment_id', $id);
        return $this->db->get('appointment')->row();
    }

    public function add_invoice_order($post, $id)
    {
        if (empty($post)) {
            return ['status'=>false,'message'=>"Post data cannot be empty`"];
        }
        $post['sent'] = 0;
        $post['clientid'] = 1;
        $post['number'] = get_option('next_invoice_number');
        $post['prefix'] = 'INV-';
        $order_data['order_date']         = $post['date']           = _d(date('Y-m-d'));
        $post['duedate']                  = _d(date('Y-m-d', strtotime('+'.get_option('invoice_due_after').' DAY', strtotime(date('Y-m-d')))));
        $post['currency'] = 3;
        $post['subtotal'] = CommonConstant::APPOINTMENT_SUBTOTAL;
        $post['total'] = CommonConstant::APPOINTMENT_TOTAL;
        $post['hash'] = app_generate_hash('next_invoice_number');    
        $post['number_format'] = 1;
        $post['allowed_payment_modes'] = 'a:1:{i:0;s:1:"1";}';
        $this->db->insert('invoices', $post);

        $insert_id = $this->db->insert_id();

        $this->db->set(['invoice_id' => $insert_id]);
	    $this->db->where('appointment_id', $id);
        $this->db->update('appointment');
        return $insert_id;
    }
    
}