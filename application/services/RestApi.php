<?php

namespace app\services;

defined('BASEPATH') or exit('No direct script access allowed');

class RestApi
{
    public function __construct()
    {
        $this->base_url = CHAT_API_BASE_URL;
        $this->instance = CHAT_API_INSTANCE . '/';
        $this->token    = '?token=' . CHAT_API_TOKEN;
    }

    private $base_url   = '';
    private $instance   = '';
    private $token      = '';

    public function callAPI($method, $action, $data, $params = [])
    {
        $url = $this->base_url . $this->instance . $action . $this->token;

        if(!empty($params)) {
            foreach($params as $key => $param) {
                $url.="&$key=$param";
            }
        }

        $curl = curl_init();
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            // 'APIKEY: 111111111111111111111',
            // 'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if (!$result) {
            die("Connection Failure");
        }
        curl_close($curl);
        return $result;
    }
}
