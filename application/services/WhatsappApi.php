<?php

namespace app\services;
use app\services\RestApi;

defined('BASEPATH') or exit('No direct script access allowed');

class WhatsappApi
{
    
    public static function getStatus()
    {
        $RestApi = new RestApi();
        $status = json_decode($RestApi->callAPI('GET','status',[]));
        return $status;
    }

    public static function getDialogs($limit,$offset,$order = 'desc')
    {
        $RestApi = new RestApi();
        $params = [
            'limit' => $limit,
            'page'  => $offset,
            'order' => $order
        ];
        $dialogs = json_decode($RestApi->callAPI('GET','dialogs',[],$params));
        if(!empty($dialogs->error)) {
            $dialogs->dialogs = [];
        }
        return $dialogs->dialogs;
    }

    public static function getDialogChatById($id,$limit,$page)
    {
        $RestApi = new RestApi();
        $params = [
            'chatId'    => $id,
            'count'     => $limit,
            'page'      => $page,
        ];
        
        $datas = $RestApi->callAPI('GET','messagesHistory',[],$params);
        $messages = json_decode($datas);
        if(!empty($messages->error)) {
            $messages->messages = [];
        }
        return $messages->messages;
    }

    public static function sendMessage($id,$body) {
        $RestApi = new RestApi();
        $params = [
            'chatId'    => $id,
            'body'     => $body,
        ];
        
        $datas = $RestApi->callAPI('POST','sendMessage',[],$params);
        $messages = json_decode($datas);
        return $messages;
        // $status = true;
        // if(!empty($messages->error)) {
            // $status = false;
        // }
        // return $status;
    }

    public static function logout()
    {
        $RestApi = new RestApi();
        return $RestApi->callAPI('POST','logout',[]);
    }

    public static function getInfo()
    {
        $RestApi    = new RestApi();
        $response   = $RestApi->callAPI('GET','me',[]);
        $info       = json_decode($response);
        return $info;

    }
}
