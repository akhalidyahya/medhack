<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'bank_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="totalInvoice" class="control-label">Total Pemesanan</label>
                            <input id="totalInvoice" class="form-control" type="text" />
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="bukti" class="control-label">Bukti Transfer</label>
                            <img id="buktiImg" src="" alt="" width="100%" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button group="button" class="btn btn-default" data-dismiss="modal"><?php //echo _l('close'); ?></button> -->
                <!-- <button group="button" type="button" class="btn btn-info" onclick="saveData()"><?php //echo _l('submit'); ?></button> -->
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
