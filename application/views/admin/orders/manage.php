<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="clearfix"></div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('orders'); ?></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="room_total"><?php echo @$order_count; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Pemesanan</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo admin_url('invoices/invoice'); ?>" class="btn btn-info pull-left new new-invoice-list mright5"><?php echo _l('create_new_invoice'); ?></a>
                            </div>
                            
                            <div class="clearfix"></div>
                            <hr class="hr-panel-heading" />
                            
                            <div class="col-md-3">
                                <?php echo render_date_input('from_date', 'from_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo render_date_input('to_date', 'to_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group select-placeholder">
                                    <label for="s_patient" class="control-label">Pasien</label>
                                    <select name="s_patient" class="selectpicker no-margin ajax-search" data-width="100%" id="s_patient" data-none-selected-text="Klik untuk mencari Pasien" data-live-search="true">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group select-placeholder">
                                    <label for="status" class="control-label">Status</label>
                                    <select name="status" class="selectpicker" data-width="100%" id="status" data-none-selected-text="Klik untuk memilih Status" data-live-search="true">
                                        <option value=""></option>
                                        <?php foreach(\app\constants\CommonConstant::INVOICE_STATUS_LISTS as $key => $label): ?>
                                            <option value="<?= $key ?>"><?php echo _l($label); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php
                        $table_data = array(
                            'Tanggal',
                            'No Invoice',
                            'Cabang',
                            'Jumlah',
                            'Pembeli',
                            'Bukti',
                            'Status',
                            'Aksi'
                        );
                        render_datatable($table_data, 'order');
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/orders/_form_modal'); ?>
<?php init_tail(); ?>
<script>
    $(function() {
        init_current_table();

        init_ajax_search('users', '#s_patient.ajax-search', undefined, admin_url + 'users/search');

        $('input[name="from_date"]').on('change', function() {
            init_current_table();
        });
        $('input[name="to_date"]').on('change', function() {
            init_current_table();
        });
        $("body").on('change', 'select[name="status"]', function() {
            init_current_table();
        });
        $("body").on('change', 'select[name="s_patient"]', function() {
            init_current_table();
        });
    });

    function init_current_table() {
        if ($.fn.DataTable.isDataTable('.table-order')) {
            $('.table-order').DataTable().destroy();
        }
        fnServerParams = {
            "from_date": '[name="from_date"]',
            "to_date": '[name="to_date"]',
            "status": '[name="status"]',
            "s_patient": '[name="s_patient"]',
        };
        initDataTable('.table-order', window.location.href, [5,6,7], [5,6,7], fnServerParams, [0, 'desc']);
    }
</script>
<script>
    function addData() {
        console.log('add');
    }

    function detailInvoice(id) {
        $.ajax({
            url: "<?php echo admin_url().'orders/getOrderById/' ?>" + id,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                console.log(data);
                $('.modal-title').text('Invoice '+ data.invoice_number);
                $('#buktiImg').prop('src',"<?php echo site_url().'uploads/bukti/'; ?>"+data.image_bukti);
                $('#totalInvoice').val(data.total);
                $('#form_modal').modal('show');
            }
        })
    }

    function changeStatus(id,status) {
        let statusList = [];
        <?php foreach (app\constants\CommonConstant::INVOICE_STATUS_LISTS as $key => $label) : ?>
            statusList["<?php echo $key; ?>"] = "<?php echo _l($label); ?>"
        <?php endforeach; ?>
        
        swal({
            title: `Status akan diubah!`,
            text: `Status pemesanan akan dirubah menjadi ${statusList[status]}`,
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Confirm!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "orders/setStatus/" + id
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        status: status
                    },
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Ubah Status",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-order').DataTable().ajax.reload();
                        } else {
                            swal({
                                title: "Gagal Ubah Status",
                                text: `${data.message}`,
                                icon: "error",
                                timer: '3000'
                            });
                        }
                    }
                })
            }
        })
    }

</script>

</body>

</html>