<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'whatsapp_assignment_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="e_branch_id" class="control-label">Cabang</label>
                        <div class="form-group select-placeholder">
                            <select name="e_branch_id" class="selectpicker form-control">
                                <option value=""></option>
                                <?php foreach(getAllBranchs() as $branch): ?>
                                <option value="<?php echo $branch->id ?>"> <?php echo $branch->branch ?> </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button id="submitBtn" group="button" type="button" class="btn btn-info" onclick="saveData()" data-id=""><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
