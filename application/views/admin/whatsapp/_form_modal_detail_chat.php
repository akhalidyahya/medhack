<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'master_room_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="chat-wrapper">
                        <div class="form-group">
                            <label for="" class="control-label">Chat Id</label>
                            <input type="text" name="chat_id" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Dialog Id</label>
                            <input type="text" name="dialog_id" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Sender</label>
                            <input type="text" name="sender" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Sender Name</label>
                            <input type="text" name="senderName" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Body</label>
                            <textarea name="body" class="form-control" cols="30" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">From Me</label>
                            <input type="text" name="fromMe" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Self</label>
                            <input type="text" name="self" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Is Forwarded</label>
                            <input type="text" name="isForwarded" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Time</label>
                            <input type="text" name="time" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Message Number</label>
                            <input type="text" name="messageNumber" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Caption</label>
                            <input type="text" name="caption" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">quotedMsgBody</label>
                            <input type="text" name="quotedMsgBody" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">quotedMsgId</label>
                            <input type="text" name="quotedMsgId" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">quotedMsgType</label>
                            <input type="text" name="quotedMsgType" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">chatName</label>
                            <input type="text" name="chatName" value="" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
