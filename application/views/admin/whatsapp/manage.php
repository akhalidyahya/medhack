<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<h4 class="no-margin bold"><?php echo _l('wa_api'); ?> status</h4>
						</div>
						<div class="clearfix"></div>
						<?php $whatsapp = $status; 

						if(!empty($whatsapp->error)): ?>
							<?php	echo "Terdapat masalah dengan chat-api.com.<br>Error: ".$whatsapp->error; ?>
						<?php else: ?>
							<?php if($whatsapp->accountStatus != 'authenticated'): ?>
							<?php echo _l('whatsapp_number_need_login'); ?>
							<div class="text-center">
								<img src="<?php echo $status->qrCode; ?>" alt="">
							</div>
							<?php else: ?>
							<?php echo _l('whatsapp_number_authenticated'); ?>
							<br>
							<a href="<?php echo admin_url('whatsappapi/logout'); ?>">Logout</a>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
</body>
</html>
