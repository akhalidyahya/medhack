<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'whatsapp_assignment_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="chat-wrapper">
                        <div class="form-group select-placeholder">
                            <label for="staff_id" class="control-label">Customer Service</label>
                            <select name="staff_id" class="selectpicker no-margin ajax-search" data-width="100%" id="staff_id" data-none-selected-text="Klik untuk mencari Customer Service" data-live-search="true">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Dialog Id</label>
                            <input type="text" name="dialog_id" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Chat Name</label>
                            <input type="text" name="chat_name" value="" class="form-control" readonly>
                        </div>
                        <?php echo form_hidden('id'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button id="submitBtn" group="button" type="button" class="btn btn-info" onclick="saveData()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
