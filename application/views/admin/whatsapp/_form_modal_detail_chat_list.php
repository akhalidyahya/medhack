<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="chats-wrapper">
                        <div class="form-group text-left">
                            <label for="chat_id" class="control-label">Panjul</label>
                            <input type="text" id="chat_id" name="chat_id" value="" class="form-control incoming">
                        </div>
                        <div class="form-group text-right">
                            <label for="chat_id" class="control-label">Panjul</label>
                            <input type="text" id="chat_id" name="chat_id" value="" class="form-control sending">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <?php if(isset($cs_page)): ?>
                <button id="waLinkChat" class="btn btn-primary" onclick="openFromWa()" data-id=""> <i class="fa fa-whatsapp"></i> Chat Lewat Whatsapp </button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
