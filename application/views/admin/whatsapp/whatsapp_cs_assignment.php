<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body _buttons">
            <div class="row">
              <div class="col-md-12">
                <h4 class="no-margin text-success"><?php echo _l('whatsapp_cs_assignment'); ?></h4>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="panel_s">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group select-placeholder">
                  <label for="s_staff" class="control-label">Staff</label>
                  <select name="s_staff" class="selectpicker no-margin ajax-search" data-width="100%" id="s_staff" data-none-selected-text="Klik untuk mencari Staff" data-live-search="true">
                    <option value=""></option>
                  </select>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <?php
            $table_data = [
              '#',
              'Dialog id',
              'Chat Name',
              'FU by',
              'FU time',
              'Aksi'
            ];
            render_datatable($table_data, 'whatsapp_cs_assignment');
            ?>
            <?php if(is_admin()): ?>
              <div class="col-md-12">
              <label class="checkbox-inline"><input type="checkbox" id="checkAll" value="">Semua Pada Halaman ini</label>
              <br>
              <br>
              <button class="btn btn-primary" onclick="bulkAction()">Bulk Action</button>
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_assignment'); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_assignment_bulk'); ?>
<script>
  $("#checkAll").click(function () {
    $('input:checkbox[name=dialogs]').not(this).prop('checked', this.checked);
  });
  
  function bulkAction() {
    $('#form_modal_bulk').modal('show');
  }

  function saveDataBulk() {
    let myArray = [];
    $("input:checkbox[name=dialogs]:checked").each(function(){
      myArray.push($(this).val());
    });
    
    var url = "<?= admin_url() ?>" + "whatsappapi/setAssignmentCsBulk";

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        staff_id: $('[name="bulk_staff_id"]').val(),
        dialog_ids: myArray
      },
      success: function(data) {
          if (data.success) {
              swal({
                  title: 'Berhasil Simpan Data',
                  text: data.message,
                  icon: 'success',
                  timer: '3000'
              }).then(() => {
                  $('#form_modal_bulk').modal('hide');
                  $('.table-whatsapp_cs_assignment').DataTable().ajax.reload();
                  calendar.refetchEvents();
              });
          } else {
              swal({
                  title: 'Gagal Simpan Data',
                  text: data.message,
                  icon: 'error',
                  timer: '3000'
              }).then(() => {
                  $('#form_modal').modal('hide');
              });
          }
      },
      error: function(jqXHR, textStatus, errorThrown) {
          swal({
              title: 'System Error',
              text: errorThrown,
              icon: 'error',
              timer: '4000'
          }).then(() => {
              $('#form_modal').modal('hide');
          });
      }
    });
  }

  $(function() {
    init_current_table();
    init_ajax_search('staff', '#staff_id.ajax-search', undefined, admin_url + 'staff/search');
    init_ajax_search('staff', '#s_staff.ajax-search', undefined, admin_url + 'staff/search');

    $("body").on('change', 'select[name="s_staff"]', function() {
      init_current_table();
    });
  });

  function init_current_table() {
    if ($.fn.DataTable.isDataTable('.table-whatsapp_cs_assignment')) {
      $('.table-whatsapp_cs_assignment').DataTable().destroy();
    }
    fnServerParams = {
      "s_staff": '[name="s_staff"]',
    };
    initDataTable('.table-whatsapp_cs_assignment', window.location.href, [3], [3], fnServerParams, [0, 'desc']);
  }
</script>
<script>
  function editData(id) {
    $.ajax({
      url: admin_url + 'whatsappapi/getCsAssignment/' + id,
      method: 'GET',
      dataType: 'JSON',
      success: function(data) {
        // console.log(data);
        $('#form_modal').modal('show');
        $('.modal-title').text('Ubah Chat-CS');
        $('[name="id"]').val(data.id);
        $('[name="dialog_id"]').val(data.dialog_id);
        $('[name="chat_name"]').val(data.chat_name);
        $('select[name=staff_id]').append('<option value="' + data.staff_id + '">' + data.full_name + '</option>');
        $('select[name=staff_id]').val(data.staff_id);
        $('#staff_id').selectpicker('val', data.staff_id);
        $('#staff_id').trigger('change');
      }
    })
  }

  function saveData() {
    var url = "<?= admin_url() ?>" + "whatsappapi/setAssignmentCs";

    $.ajax({
        url: url,
        type: 'POST',
        data: $('#whatsapp_assignment_form').serialize(),
        success: function(data) {
            // data = JSON.parse(data);
            if (data.success) {
                swal({
                    title: 'Berhasil Simpan Data',
                    text: data.message,
                    icon: 'success',
                    timer: '3000'
                }).then(() => {
                    $('#form_modal').modal('hide');
                    $('.table-whatsapp_cs_assignment').DataTable().ajax.reload();
                    calendar.refetchEvents();
                });
            } else {
                swal({
                    title: 'Gagal Simpan Data',
                    text: data.message,
                    icon: 'error',
                    timer: '3000'
                }).then(() => {
                    $('#form_modal').modal('hide');
                });
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal({
                title: 'System Error',
                text: errorThrown,
                icon: 'error',
                timer: '4000'
            }).then(() => {
                $('#form_modal').modal('hide');
            });
        }
    });
  }
</script>
</body>

</html>