<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body _buttons">
            <div class="row">
              <div class="col-md-12">
                <h4 class="no-margin text-success"><?php echo _l('whatsapp_chat_list'); ?></h4>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="panel_s">
          <div class="panel-body">
            <div class="clearfix"></div>
            <?php
            $table_data = [
              'Time',
              'Chat Name',
              'Body',
              'Sender Name',
              'Type',
              'From Me',
              'Detail'
            ];
            render_datatable($table_data, 'whatsapp_chats');
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_detail_chat'); ?>
<script>
  $(function() {
    initDataTable('.table-whatsapp_chats', window.location.href, [2,4,5], [2,4,5], undefined, [0, 'desc']);
  });
</script>
<script>
  function viewData(id)
  {
    $.ajax({
      url: admin_url+'whatsappapi/getRawChatData/'+id,
      method: 'GET',
      dataType: 'JSON',
      success: function(data) {
        $('#form_modal').modal('show');
        $('.modal-title').text('Detail Chat');
        $('[name="chat_id"]').val(data.chat_id);
        $('[name="dialog_id"]').val(data.dialog_id);
        $('[name="sender"]').val(data.sender);
        $('[name="senderName"]').val(data.senderName);
        $('[name="body"]').val(data.body);
        $('[name="fromMe"]').val(data.fromMe);
        $('[name="self"]').val(data.self);
        $('[name="isForwarded"]').val(data.isForwarded);
        $('[name="time"]').val(data.time);
        $('[name="messageNumber"]').val(data.messageNumber);
        $('[name="caption"]').val(data.caption);
        $('[name="quotedMsgBody"]').val(data.quotedMsgBody);
        $('[name="quotedMsgId"]').val(data.quotedMsgId);
        $('[name="quotedMsgType"]').val(data.quotedMsgType);
        if(data.chatName) {
          $('[name="chatName"]').val(data.chatName); 
        } else {
          $('[name="chatName"]').val(data.senderName);
        }
      }
    })
  }
</script>
</body>

</html>