<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<style>
  .incoming {
    width: 90%;
    margin-right: auto;
  }

  .sending {
    width: 90%;
    margin-left: auto;
  }
</style>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body _buttons">
            <div class="row">
              <div class="col-md-12">
                <h4 class="no-margin text-success"><?php echo _l('whatsapp_chat_list'); ?></h4>
              </div>
              <input type="hidden" name="cs_page" value="<?php echo @$cs_page ?>">
              <?php if (isset($cs_page) == false) : ?>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="source" class="control-label">Nomor Telepon</label>
                    <select name="source" class="form-control">
                      <option value="">Semua</option>
                      <?php foreach ($all_number as $number) : ?>
                        <option value="<?= $number->number ?>"><?php echo $number->number; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              <?php endif; ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="panel_s">
          <div class="panel-body">
            <div class="clearfix"></div>
            <?php
            $table_data = [
              '#',
              'Id',
              // 'Last Chat',
              'Nomor',
              'Chat Name',
              'Type',
              'Follow Up',
              'Cabang',
              'Assignment',
              'Detail'
            ];
            render_datatable($table_data, 'whatsapp_dialogs');
            ?>
            <?php if (is_admin()) : ?>
              <div class="col-md-12">
                <label class="checkbox-inline"><input type="checkbox" id="checkAll" value="">Semua Pada Halaman ini</label>
                <br>
                <br>
                <button class="btn btn-primary" onclick="bulkAction()">Bulk Action</button>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_detail_chat_list'); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_edit_chat'); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_assignment_bulk'); ?>
<script>
  $(function() {
    init_current_datatable();
    init_ajax_search('staff', '#staff_id.ajax-search', undefined, admin_url + 'staff/search');
    $("body").on('change', 'select[name="source"]', function() {
      init_current_datatable();
    });

  });

  function init_current_datatable() {
    if ($.fn.DataTable.isDataTable('.table-whatsapp_dialogs')) {
      $('.table-whatsapp_dialogs').DataTable().destroy();
    }
    fnServerParams = {
      "source": '[name="source"]',
      "cs_page": '[name="cs_page"]'
    };
    initDataTable('.table-whatsapp_dialogs', window.location.href, [], [], fnServerParams, [0, 'desc']);
  }
</script>
<script>
  $("#checkAll").click(function() {
    $('input:checkbox[name=dialogs]').not(this).prop('checked', this.checked);
  });

  function bulkAction() {
    $('#form_modal_bulk').modal('show');
  }

  function saveDataBulk() {
    let myArray = [];
    $("input:checkbox[name=dialogs]:checked").each(function() {
      myArray.push($(this).val());
    });

    var url = "<?= admin_url() ?>" + "whatsappapi/setAssignmentCsBulkFromDialog";

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        staff_id: $('[name="bulk_staff_id"]').val(),
        dialog_ids: myArray
      },
      success: function(data) {
        if (data.success) {
          swal({
            title: 'Berhasil Simpan Data',
            text: data.message,
            icon: 'success',
            timer: '3000'
          }).then(() => {
            $('#form_modal_bulk').modal('hide');
            $('.table-whatsapp_dialogs').DataTable().ajax.reload();
          });
        } else {
          swal({
            title: 'Gagal Simpan Data',
            text: data.message,
            icon: 'error',
            timer: '3000'
          }).then(() => {
            $('#form_modal').modal('hide');
          });
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        swal({
          title: 'System Error',
          text: errorThrown,
          icon: 'error',
          timer: '4000'
        }).then(() => {
          $('#form_modal').modal('hide');
        });
      }
    });
  }

  function viewData(id) {
    $.ajax({
      url: admin_url + 'whatsappapi/getChatByDialog/' + id,
      method: 'GET',
      // dataType: 'JSON',
      success: function(data) {
        console.log(data);
        $('#chats-wrapper').html('');
        $('#form_modal').modal('show');
        $('.modal-title').text('Detail Chats');
        // $('#waLinkChat').prop('href','https://wa.me/'+waNumber);
        $('#waLinkChat').attr('data-id', id);
        data.forEach((value, index) => {
          let html = '';
          html += renderChat(value);
          $('#chats-wrapper').prepend(html);
        })
        // $('[name="chat_id"]').val(data.chat_id);
        // $('[name="dialog_id"]').val(data.dialog_id);
        // $('[name="sender"]').val(data.sender);
        // $('[name="senderName"]').val(data.senderName);
        // $('[name="body"]').val(data.body);
        // $('[name="fromMe"]').val(data.fromMe);
        // $('[name="self"]').val(data.self);
        // $('[name="isForwarded"]').val(data.isForwarded);
        // $('[name="time"]').val(data.time);
        // $('[name="messageNumber"]').val(data.messageNumber);
        // $('[name="caption"]').val(data.caption);
        // $('[name="quotedMsgBody"]').val(data.quotedMsgBody);
        // $('[name="quotedMsgId"]').val(data.quotedMsgId);
        // $('[name="quotedMsgType"]').val(data.quotedMsgType);
        // if(data.chatName) {
        //   $('[name="chatName"]').val(data.chatName); 
        // } else {
        //   $('[name="chatName"]').val(data.senderName);
        // }
      }
    })
  }

  function editData(id) {
    $.ajax({
      url: admin_url + 'whatsappapi/getDialogByDialogIdFromDb/' + id,
      method: 'GET',
      // dataType: 'JSON',
      success: function(data) {
        $('#modalEdit').modal('show');
        $('.modal-title').text('Edit Chats Detail');
        $('#submitBtn').attr('data-id', id);
        $('#modalEdit select').val(data.branch_id).change();
        $('#modalEdit select').val(data.branch_id);
      }
    })
  }

  function saveData() {
    let id = $('#submitBtn').attr('data-id');
    let formData = {
      dialog_id: id,
      branch_id: $('[name="e_branch_id"]').val(),
    };

    $.ajax({
      url: '<?php echo base_url() ?>admin/customerservices/save',
      type: 'POST',
      dataType: 'JSON',
      data: formData,
      success: function(data) {
        // data = JSON.parse(data);
        if (data.success) {
          swal({
            title: 'Berhasil Ubah Status',
            text: data.message,
            icon: 'success',
            timer: '3000'
          }).then(() => {
            $('#modalEdit').modal('hide');
          });
        } else {
          swal({
            title: 'Gagal Ubah Status',
            text: data.message,
            icon: 'error',
            timer: '5000'
          })
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        swal({
          title: 'System Error',
          text: errorThrown,
          icon: 'error',
          timer: '5000'
        }).then(() => {
          $('#form_modal').modal('hide');
        });
      }
    });
  }

  function openFromWa() {
    let id = $('#waLinkChat').attr('data-id');
    if (!id) {
      return '';
    }

    $.ajax({
      url: '<?php echo admin_url() ?>CustomerServices/followUpDialog/' + id,
      type: 'GET',
      async: true,
      beforeSend: function(data) {
        swal({
          text: 'redirecting...',
          button: false,
          time: 4000
        });
      },
      success: function(data) {
        location.href = "https://wa.me/" + data;
      },
      error: function(jqXHR, textStatus, errorThrown) {
        swal({
          icon: 'error',
          title: 'Something went wrong!',
          text: errorThrown,
          time: 4000
        });
      }
    });
  }

  function renderChat(chat) {
    let html = '';
    let chatFrom = 'sending';
    let labelFrom = 'text-right';
    let caption = '';
    if (chat.caption) {
      caption = chat.caption;
    }
    if (chat.fromMe == 0) {
      chatFrom = 'incoming';
      labelFrom = 'text-left';
    }
    html += '<div class="form-group ' + labelFrom + '">';
    html += '<label for="chat_id" class="control-label"><b>' + chat.senderName + '</b> (' + moment.unix(chat.time).format("llll") + ')</label>';
    if (chat.type == 'image') {
      html += '<p type="text" id="chat_id" class="' + chatFrom + '" ><img src="' + chat.body + '" height="100px"/></p><p>' + caption + '</p>';
    } else {
      html += '<p type="text" id="chat_id" class="' + chatFrom + '" >' + chat.body + '</p>';
    }
    html += '</div>';
    html += '<hr>';
    return html;
  }
</script>
</body>

</html>