<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body _buttons">
            <div class="row">
              <div class="col-md-12">
                <h4 class="no-margin text-success"><?php echo _l('whatsapp_call_list'); ?></h4>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="panel_s">
          <div class="panel-body">
            <div class="clearfix"></div>
            <?php
            $table_data = [
              'Waktu Call',
              'Chat Name',
              'Body',
              'Sender Name',
              'Type',
            ];
            render_datatable($table_data, 'whatsapp_calls');
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<script>
  $(function() {
    initDataTable('.table-whatsapp_calls', window.location.href, [2,4], [2,4], undefined, [0, 'desc']);
  });
</script>
</body>
</html>