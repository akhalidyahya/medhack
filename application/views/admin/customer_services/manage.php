<?php

defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/chat.css" />
<div id="wrapper">
   <div class="content">
      <div class="row">

         <div class="col-md-12">
            <div class="panel_s mbot15">
               <div class="panel-body">
                  <div class="_buttons">
                     <h4 class="no-margin bold">Customer Services (<?php echo get_staff_full_name(); ?>)</h4>
                  </div>
                  <div class="pull-right text-right">
                     <?php if(isset($whatsappApi)) {
                        echo 'Whatsapp State: '.$whatsappApi->status;
                        echo '<br>';
                        echo 'Message: '.$whatsappApi->message;
                     } ?>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>

         <div class="col-md-12">
            <div id="frame">
               <div id="sidepanel">
                  <div id="profile">
                     <div class="wrap">
                        <?php echo staff_profile_image($GLOBALS['current_user']->staffid,array('online')); ?>
                        <p><?php echo get_staff_full_name(); ?></p>
                        <!-- <i class="fa fa-chevron-down expand-button" aria-hidden="true"></i> -->
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group" style="padding-right:10px; padding-left:10px;">
                           <input type="text" class="form-control" id="filterContact" placeholder="contoh: 62882920151011" onchange="filter()">
                        </div>
                     </div>
                  </div>
                  <div id="refresh" class="text-right mright5 mbot5">
                     <button id="refreshDialog" class="btn btn-default btn-sm" onclick="getMoreDialog(true)"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                  </div>
                  <div id="contacts">
                     <ul id="chatDialogs">
                        <?php foreach($dialogs as $dialog): ?>
                        <?php
                           $label = !empty($dialog->label) ? $dialog->label : getDialogLabel($dialog->id);
                           $fuBy = !empty($dialog->fullname) ? $dialog->fullname : getFuPerson($dialog->id);
                        ?>
                        <li id="<?= $dialog->id ?>" class="contact" 
                           data-chat-profile="<?php echo ($dialog->image != NULL) ? $dialog->image : base_url()."assets/images/user-placeholder.jpg"; ?>" 
                           data-chat-name="<?php echo $dialog->name; ?>" 
                           data-chat-id="<?php echo $dialog->id; ?>"
                           data-chat-label="<?php echo @$label ?>"
                           onclick="select(this)" >
                           <div class="wrap">
                              <!-- <span class="contact-status online"></span> -->
                              <img src="<?php echo ($dialog->image != NULL) ? $dialog->image : base_url()."assets/images/user-placeholder.jpg"; ?>" alt="" />
                              <div class="meta">
                                 <p class="name"><?php echo $dialog->name ?> <b id="label" class="wa-label wa-label-<?php echo @$label ?>"> <?php echo @\app\constants\CommonConstant::WA_DIALOG_STATUS_LISTS[$label]; ?></b> </p>
                                 <p class="preview"><?php echo @time2str($dialog->last_time) ?></p>
                                 <p class="preview">FU by: <strong><i id="fuBy" class="text-danger"><?php echo @$fuBy ?></i></strong></p>
                              </div>
                           </div>
                        </li>
                        <?php endforeach; ?>
                        <div class="text-center mtop5 mbot5" id="getMoreDialogs"><button class="btn btn-default btn-sm" onclick="getMoreDialog()">Load more</button></div>
                     </ul>
                  </div>
                  <!-- <div id="bottom-bar">
                     <button id="addcontact"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> <span>Add contact</span></button>
                     <button id="settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>
                  </div> -->
               </div>
               <div class="content">
                  <div class="contact-profile">
                     <img id="chatProfile" src="<?php echo base_url()."assets/images/user-placeholder.jpg"; ?>" alt="" />
                     <p id="chatName" style="margin-right: 10px;"></p>
                      <span id="labelChat" class="wa-label"></span>
                     <div id="refresh" class="pull-right mright5 mbot5">
                        <button id="openFromWa" class="btn btn-default btn-sm" onclick="openFromWa()" style="margin-right: 10px;" ><i class="fa fa-whatsapp" aria-hidden="true"></i></button>
                        <button id="changeLabel" class="btn btn-default btn-sm" onclick="changeLabel()"><i class="fa fa-edit" aria-hidden="true"></i></button>
                        <button id="refreshChat" class="btn btn-default btn-sm" onclick="refreshChat()"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                     </div>
                  </div>
                  <div class="messages">
                     <ul id="chatMessages">
                        
                     </ul>
                  </div>
                  <div class="message-input">
                     <div class="wrap">
                        <input id="inputMessage" type="text" placeholder="Write your message..." />
                        <!-- <i class="fa fa-paperclip attachment" aria-hidden="true"></i> -->
                        <button class="submit" onclick="sendMessage()"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                     </div>
                  </div>
               </div>
            </div>

         </div>

      </div>
   </div>
</div>
<?php $this->load->view('admin/customer_services/_form_modal'); ?>
<?php init_tail(); ?>
<script src="<?php echo base_url() ?>assets/js/hoy3lrg.js"></script>
<!-- <script src="<?php //echo base_url() ?>assets/js/moment.min.js"></script> -->
<script>
   try {
      Typekit.load({
         async: true
      });
   } catch (e) {}
</script>
<script>
let statusList = [];
<?php foreach (app\constants\CommonConstant::WA_DIALOG_STATUS_LISTS as $key => $label) : ?>
   statusList["<?php echo $key; ?>"] = "<?php echo strtoupper($label); ?>"
<?php endforeach; ?>
//state
let currentDialog = {
   id: 0,
   name: '',
   profile: '',
   page: 0,
   label: '',
};

let stateDialog = {
   page: 0
}

</script>
<script>
   $("#profile-img").click(function() {
	$("#status-options").toggleClass("active");
});

$(".expand-button").click(function() {
  $("#profile").toggleClass("expanded");
	$("#contacts").toggleClass("expanded");
});

$("#status-options ul li").click(function() {
	$("#profile-img").removeClass();
	$("#status-online").removeClass("active");
	$("#status-away").removeClass("active");
	$("#status-busy").removeClass("active");
	$("#status-offline").removeClass("active");
	$(this).addClass("active");
	
	if($("#status-online").hasClass("active")) {
		$("#profile-img").addClass("online");
	} else if ($("#status-away").hasClass("active")) {
		$("#profile-img").addClass("away");
	} else if ($("#status-busy").hasClass("active")) {
		$("#profile-img").addClass("busy");
	} else if ($("#status-offline").hasClass("active")) {
		$("#profile-img").addClass("offline");
	} else {
		$("#profile-img").removeClass();
	};
	
	$("#status-options").removeClass("active");
});

function newMessage(element) {
	let chatElement = '';
   if(element.fromMe) {
      chatElement+="<li class='sent'>";
      chatElement+="<h6>Me";
   } else {
      chatElement+="<li class='replies'>";
      chatElement+="<h6>"+element.senderName;
   }
   chatElement+=" <small>("+element.time+")</small>";
   chatElement+="</h6>";
   chatElement+="<p>";
   if(element.type == 'image') {
      chatElement+="<img width='100%' src='"+element.body+"' />";
   } else {
      chatElement+=element.body;
   }
   chatElement+="</p>";
   chatElement+="</li>";
   $('#chatMessages').append(chatElement);
};

function select(eL) {
   currentDialog.id = eL.getAttribute('data-chat-id');
   currentDialog.name = eL.getAttribute('data-chat-name');
   currentDialog.profile = eL.getAttribute('data-chat-profile');
   currentDialog.label = '';
   if(eL.getAttribute('data-chat-label')) {
      currentDialog.label = eL.getAttribute('data-chat-label');
   }
   currentDialog.page = 0;
   // console.log(currentDialog);
   generateChat(currentDialog);
}

function refreshChat() {
   generateChat(currentDialog);
}

function generateChat(chat) {
   $('#chatName').text(chat.name);
   $('#chatProfile').attr('src',chat.profile);

   if(!chat.label) {
      $.ajax({
         url: '<?php echo base_url() ?>admin/whatsappapi/getDialogLabelRaw/'+chat.id,
         type: 'GET',
         dataType:'JSON',
         // async: false,
         success: function(data) {
            if(data) {
               generateLabelChat(statusList[data],data);
            } else {
               generateLabelChat('','LEAD');
            }
         },
         error: function(e) {
            $('#refreshChat').prop('disabled', false);
            console.log(e);
         }
      });
   } else {
      generateLabelChat(chat.label);
   }

   $.ajax({
      url: '<?php echo base_url() ?>admin/whatsappapi/getchat/'+chat.id,
      type: 'GET',
      dataType:'JSON',
      beforeSend: function(data) {
         $('#refreshChat').prop('disabled', true);
         $('#chatMessages').html('<li class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
      },
      success: function(data) {
         $('#refreshChat').prop('disabled', false);
         $('#chatMessages').html('');
         if(data.length > 0) {
            data.forEach((element,index)=>{
               let chatElement = '';
               if(element.fromMe) {
                  chatElement+="<li class='sent'>";
                  chatElement+="<h6>Me";
               } else {
                  chatElement+="<li class='replies'>";
                  chatElement+="<h6>"+element.senderName;
               }
               chatElement+=" <small>("+convertDateTime(element.time)+")</small>";
               chatElement+="</h6>";
               chatElement+="<p>";
               if(element.type == 'image') {
                  chatElement+="<img width='100%' src='"+element.body+"' />";
                  chatElement+="<span>"+element.caption+"</span>";
               } else {
                  chatElement+=element.body;
               }
               chatElement+="</p>";
               chatElement+="</li>";
               $('#chatMessages').prepend(chatElement);
            })
            $('#chatMessages').prepend('<div class="text-center mtop5" id="getMoreChats"><button class="btn btn-default btn-sm" onclick="getMoreChat()">Load more</button></div>');
            $(".messages").animate({ scrollTop: $('.content').height() }, "fast");
         }
      },
      error: function(e) {
         $('#refreshChat').prop('disabled', false);
         console.log(e);
      }
   });
}

function getMoreChat() {
   currentDialog.page++;
   $.ajax({
      url: '<?php echo base_url() ?>admin/whatsappapi/getMoreChat',
      type: 'GET',
      dataType:'JSON',
      data: {chatId:currentDialog.id,page:currentDialog.page},
      beforeSend: function(data) {
         // $('#chatMessages').html('<li class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
         $('#getMoreChats').remove();
         $('#chatMessages').prepend('<li id="loadingChat" class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
      },
      success: function(data) {
         if(data.length > 0) {
            data.forEach((element,index)=>{
               $('#loadingChat').remove();
               let chatElement = '';
               if(element.fromMe) {
                  chatElement+="<li class='sent'>";
                  chatElement+="<h6>Me";
               } else {
                  chatElement+="<li class='replies'>";
                  chatElement+="<h6>"+element.senderName;
               }
               chatElement+=" <small>("+convertDateTime(element.time)+")</small>";
               chatElement+="</h6>";
               chatElement+="<p>";
               if(element.type == 'image') {
                  chatElement+="<img width='100%' src='"+element.body+"' />";
               } else {
                  chatElement+=element.body;
               }
               chatElement+="</p>";
               chatElement+="</li>";
               $('#chatMessages').prepend(chatElement);
            })
            $('#chatMessages').prepend('<div class="text-center mtop5" id="getMoreChats"><button class="btn btn-default btn-sm" onclick="getMoreChat()">Load more</button></div>');
         }
      },
      error: function(e) {
         console.log(e);
      }
   });
   
}

function getMoreDialog(init = false)
{
   if(!init) {
      stateDialog.page++;
   } else {
      stateDialog.page = 0;
   }
   $.ajax({
      url: '<?php echo base_url() ?>admin/whatsappapi/getMoreDialog',
      type: 'GET',
      dataType:'JSON',
      data: {page:stateDialog.page},
      beforeSend: function(data) {
         $('#refreshDialog').prop('disabled', true);
         // $('#chatMessages').html('<li class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
         if(init) {
            $('#chatDialogs').html('');
         }
         $('#getMoreDialogs').remove();
         $('#chatDialogs').append('<li id="loadingDialog" class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
      },
      success: function(data) {
         $('#refreshDialog').prop('disabled', false);
         $('#loadingDialog').remove();
         
         if(data.length > 0) {
            data.forEach((element,index)=>{
               let dialogElement = '';
               dialogElement+='<li id="'+element.id+'" class="contact"';
               let image = '';
               if(element.image) {
                  image = element.image;
               } else {
                  image = "<?php echo base_url().'assets/images/user-placeholder.jpg'; ?>";
               }

               let label = '';
               let dialogUrl = '<?php echo admin_url() ?>whatsappapi/getDialogLabelRaw/'+element.id;
               let id_dialog = element.id;

               $.ajax({
                  url: dialogUrl,
                  type: 'GET',
                  async: true,
                  success: function(dataa) {
                     changeLabelDialog({
                        key:dataa,
                        label:statusList[dataa],
                        dialog_id:id_dialog,
                     });
                  }
               });

               let fuPersonUrl = '<?php echo admin_url() ?>CustomerServices/getFuPersonByDialogId/'+element.id;
               $.ajax({
                  url: fuPersonUrl,
                  type: 'GET',
                  async: true,
                  success: function(dataa) {
                     setFuByPerson({
                        fullname:dataa,
                        dialog_id:id_dialog,
                     });
                  }
               });

               dialogElement+='data-chat-profile="'+image+'"';
               dialogElement+='data-chat-name="'+element.name+'"';
               dialogElement+='data-chat-label="'+label+'"';
               dialogElement+='data-chat-id="'+element.id+'"onclick="select(this)" ><div class="wrap">';
               dialogElement+='<img src="'+image+'" alt="" /><div class="meta">';
               dialogElement+='<p class="name">'+element.name+'<b id="label" class="wa-label wa-label-'+label+'"> loading...</b></p>';
               dialogElement+='<p class="preview">'+moment.unix(element.last_time).fromNow()+'</p>';
               dialogElement+='<p class="preview">FU by: <strong><i id="fuBy" class="text-danger"></i></strong></p>';
               dialogElement+='</div></div></li>';
               $('#chatDialogs').append(dialogElement);
            })
            $('#chatDialogs').append('<div class="text-center mtop5 mbot5" id="getMoreDialogs"><button class="btn btn-default btn-sm" onclick="getMoreDialog()">Load more</button></div>');
         }
      },
      error: function(e) {
         $('#refreshDialog').prop('disabled', false);
         console.log(e);
      }
   });
}

function sendMessage() {
   // let chat = {
   //             fromMe: true,
   //             time: moment().format("llll"),
   //             body: $('#inputMessage').val()
   //          }
   //          newMessage(chat);
   //          $('#inputMessage').val('');
   $.ajax({
      url: '<?php echo base_url() ?>admin/whatsappapi/sendMessage/'+currentDialog.id,
      type: 'GET',
      dataType:'JSON',
      data: {body:$('#inputMessage').val()},
      beforeSend: function(data) {
         // $('#getMoreDialogs').remove();
         // $('#chatDialogs').append('<li id="loadingDialog" class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
      },
      success: function(data) {
         if(data.sent) {
            let chat = {
               fromMe: true,
               time: moment().format("llll"),
               body: $('#inputMessage').val()
            }
            newMessage(chat);
            $('#inputMessage').val('');
         } else {
            console.log(data);
            swal({
               title: 'Gagal kirim pesan',
               // text: data.message,
               icon: 'error',
               timer: '5000'
            })
         }
      },
      error: function(jqXHR, textStatus, errorThrown) {
         swal({
            title: 'System Error',
            text: errorThrown,
            icon: 'error',
            timer: '5000'
         });
      }
   });
}

function changeLabel() {
   if(!currentDialog.id) {
      return '';
   }
   $('.modal-title').text('Ubah Status');
   $('#id').val(0);
   $('#form_modal form')[0].reset();
   $('[name=method]').val('add');
   $('label.error').remove();
   $('#form_modal').modal('show');
}

function generateLabelChat(label,key = null) {
   $('#labelChat').text(label);
   var cssLabel = 'wa-label-'+label;
   if(key) {
      cssLabel = 'wa-label-'+key;
   }
   $('#labelChat').removeClass();
   $('#labelChat').addClass('wa-label');
   $('#labelChat').addClass(cssLabel);
}

function changeLabelDialog(data) {
   // console.log(data);
   var element = document.getElementById(data.dialog_id);
   // console.log(element);
   element.setAttribute("data-chat-label", data.label);
   if(data.key != data.label)  {
      element.setAttribute("data-chat-label", data.key); 
   }
   element.firstElementChild.children[1].children[0].children[0].setAttribute('class','wa-label wa-label-'+data.key);
   element.firstElementChild.children[1].children[0].children[0].innerHTML = data.label;
}

function setFuByPerson(data) {
   var element = document.getElementById(data.dialog_id);
   let person = '';
   if(data.fullname) {
      person = data.fullname;
   }
   // console.log(data);
   // console.log(element);
   element.firstElementChild.children[1].children[2].innerHTML = 'FU by: <strong><i id="fuBy" class="text-danger">'+person+'</i></strong>';
}

function saveStatus(e) {
   e.preventDefault();
   let formData = {
      dialog_id : currentDialog.id,
      status : $('[name="label-status"]').val(),
   };

   $.ajax({
      url: '<?php echo base_url() ?>admin/whatsappapi/saveStatus',
      type: 'POST',
      dataType: 'JSON',
      data: formData,
      success: function(data) {
            // data = JSON.parse(data);
            if (data.success) {
               generateLabelChat(data.label, data.key)
               changeLabelDialog(data);
               swal({
                  title: 'Berhasil Ubah Status',
                  text: data.message,
                  icon: 'success',
                  timer: '3000'
               }).then(() => {
                  $('#form_modal').modal('hide');
               });
            } else {
               swal({
                  title: 'Gagal Ubah Status',
                  text: data.message,
                  icon: 'error',
                  timer: '5000'
               })
            }
      },
      error: function(jqXHR, textStatus, errorThrown) {
            swal({
               title: 'System Error',
               text: errorThrown,
               icon: 'error',
               timer: '5000'
            }).then(() => {
               $('#form_modal').modal('hide');
            });
      }
   });
}

function openFromWa() {
   if(!currentDialog.id) {
      return '';
   }

   $.ajax({
      url: '<?php echo admin_url() ?>CustomerServices/followUpDialog/'+currentDialog.id,
      type: 'GET',
      async: true,
      beforeSend: function(data) {
         swal({text:'redirecting...',button:false,time:4000});
      },
      success: function(data) {
         location.href="https://wa.me/"+data;
      },
      error: function(jqXHR, textStatus, errorThrown) {
         swal({icon:'error',title:'Something went wrong!',text:errorThrown,time:4000});
      }
   });
}

function convertDateTime(epochTime){
   return moment.unix(epochTime).format("llll");
}

function filter() {
   stateDialog.page = 0;
   if($('#filterContact').val() == '') {
      getMoreDialog(true);
      return '';
   }
   let keyword = $('#filterContact').val();
   $.ajax({
      url: '<?php echo base_url() ?>admin/whatsappapi/searchDialogFromDb',
      type: 'GET',
      dataType:'JSON',
      data: {page:stateDialog.page,contact:keyword},
      beforeSend: function(data) {
         $('#refreshDialog').prop('disabled', true);
         // $('#chatMessages').html('<li class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
         $('#chatDialogs').html('');
         $('#getMoreDialogs').remove();
         $('#chatDialogs').append('<li id="loadingDialog" class="loading text-center"><i class="fa fa-spin fa-spinner fa-lg"></i></li>');
      },
      success: function(data) {
         $('#refreshDialog').prop('disabled', false);
         $('#loadingDialog').remove();
         
         if(data.length > 0) {
            data.forEach((element,index)=>{
               let dialogElement = '';
               dialogElement+='<li id="'+element.id+'" class="contact"';
               let image = '';
               if(element.image) {
                  image = element.image;
               } else {
                  image = "<?php echo base_url().'assets/images/user-placeholder.jpg'; ?>";
               }

               let label = '';
               let dialogUrl = '<?php echo admin_url() ?>whatsappapi/getDialogLabelRaw/'+element.id;
               let id_dialog = element.id;

               $.ajax({
                  url: dialogUrl,
                  type: 'GET',
                  async: true,
                  success: function(dataa) {
                     changeLabelDialog({
                        key:dataa,
                        label:statusList[dataa],
                        dialog_id:id_dialog,
                     });
                  }
               });

               let fuPersonUrl = '<?php echo admin_url() ?>CustomerServices/getFuPersonByDialogId/'+element.id;
               $.ajax({
                  url: fuPersonUrl,
                  type: 'GET',
                  async: true,
                  success: function(dataa) {
                     setFuByPerson({
                        fullname:dataa,
                        dialog_id:id_dialog,
                     });
                  }
               });

               dialogElement+='data-chat-profile="'+image+'"';
               dialogElement+='data-chat-name="'+element.name+'"';
               dialogElement+='data-chat-label="'+label+'"';
               dialogElement+='data-chat-id="'+element.id+'"onclick="select(this)" ><div class="wrap">';
               dialogElement+='<img src="'+image+'" alt="" /><div class="meta">';
               dialogElement+='<p class="name">'+element.name+'<b id="label" class="wa-label wa-label-'+label+'"> loading...</b></p>';
               dialogElement+='<p class="preview">'+moment.unix(element.last_time).fromNow()+'</p>';
               dialogElement+='<p class="preview">FU by: <strong><i id="fuBy" class="text-danger"></i></strong></p>';
               dialogElement+='</div></div></li>';
               $('#chatDialogs').append(dialogElement);
            })
            $('#chatDialogs').append('<div class="text-center mtop5 mbot5" id="getMoreDialogs"><button class="btn btn-default btn-sm" onclick="getMoreDialog()">Load more</button></div>');
         }
      },
      error: function(e) {
         $('#refreshDialog').prop('disabled', false);
         console.log(e);
      }
   });
}

</script>
</body>

</html>