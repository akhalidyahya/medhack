<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="clearfix"></div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('fu_result'); ?></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="room_total"><?php echo @$summary_count; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Follow Up</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo render_date_input('from_date', 'from_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo render_date_input('to_date', 'to_date'); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php
                        $table_data = array(
                            'Id',
                            'Name',
                            'Tanggal FU',
                            'Label',
                            'Status',
                            'Note',
                            'Edit'
                        );
                        render_datatable($table_data, 'fu_summary');
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/customer_services/_fu_summary_form_modal'); ?>
<?php init_tail(); ?>
<script>
    $(function() {
        init_current_table();
        $('input[name="from_date"]').on('change', function() {
            init_current_table();
        });
        $('input[name="to_date"]').on('change', function() {
            init_current_table();
        });
    });

    function init_current_table() {
        if ($.fn.DataTable.isDataTable('.table-fu_summary')) {
            $('.table-fu_summary').DataTable().destroy();
        }
        fnServerParams = {
            "from_date": '[name="from_date"]',
            "to_date": '[name="to_date"]',
            "status": '[name="status"]',
        };
        initDataTable('.table-fu_summary', window.location.href, [6], [6], fnServerParams, [2, 'desc']);
    }
</script>
<script>
    function addData() {
        console.log('add');
    }

    function editData(id) {
        $.ajax({
            url: "<?php echo admin_url() . 'whatsappapi/getDialogByDialogIdFromDb/' ?>" + id,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                console.log(data);
                $('.modal-title').text(data.name);
                $('[name="dialog_id"]').val(data.dialog_id);
                $('[name="note"]').val(data.followup_note);
                $('select[name="status"]').val(data.fu_status_id);
                $('#form_modal').modal('show');
            }
        })
    }

    function saveData() {
        var url = "<?= admin_url() ?>" + "CustomerServices/saveNote";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#myForm').serialize(),
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                        $('.table-fu_summary').DataTable().ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    $('#form_modal').modal('hide');
                    $('.table-branchs').DataTable().ajax.reload();
                });
            }
        });
    }
</script>

</body>

</html>