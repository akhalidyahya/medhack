<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="clearfix"></div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success">Statistik Customer Service</h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="room_total"><?php echo @$summary_count_leads; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Leads</span>
                            </div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="room_total"><?php echo @$summary_count_follow_up; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Follow Up</span>
                            </div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="room_total"><?php echo @$summary_count_patient; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Pasien</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4 pull-right">
                                <div class="form-group">
                                    <label for="staff" class="control-label">Staff</label>
                                    <select name="staff" class="form-control" id="staff" onchange="getStatDataByStaff(this)">
                                        <option value="">Semua</option>
                                        <?php foreach ($all_staff as $staff) : ?>
                                            <option value="<?= $staff->staffid ?>"><?php echo $staff->fullname; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Persentase FU Result</h4>
                                <canvas id="myChart"></canvas>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo render_date_input('from_date', 'from_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo render_date_input('to_date', 'to_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="staff2" class="control-label">Staff</label>
                                    <select name="staff2" class="form-control" onchange="getTableDataByStaff(this)">
                                        <option value="">Semua</option>
                                        <?php foreach ($all_staff as $staff) : ?>
                                            <option value="<?= $staff->staffid ?>"><?php echo $staff->fullname; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <table class="table table-responsive table-bordered">
                            <thead>
                                <tr>
                                    <th>Result</th>
                                    <th>Lead</th>
                                    <th>Fu</th>
                                    <th>Closing</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_wrapper">
                                <?php foreach($fu_summaries as $summary): ?>
                                <?php if(empty($summary->status)) continue; ?>
                                <tr>
                                    <td><?php echo $summary->status ?></td>
                                    <td>0</td>
                                    <td><?php echo $summary->jumlah ?></td>
                                    <td>0</td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script src="<?php site_url() ?>assets/plugins/Chart.js/Chart.js"></script>

<script>
    var ctx = document.getElementById('myChart').getContext('2d');

    let data = {
        labels: <?php echo json_encode($fu_status['label']) ?>,
        datasets: [{
            label: 'My First Dataset',
            data: <?php echo json_encode($fu_status['dataset']) ?>,
            backgroundColor: <?php echo json_encode($fu_status['bgcolor']) ?>,
            hoverOffset: 4
        }, ],
        responsive: true,
    };

    const config = {
        type: 'pie',
        data: data,
    };

    var myChart = new Chart(ctx, config);

    function getStatDataByStaff(element) {
        let staffId = element.value;
        $.ajax({
            url: "<?php echo admin_url() . 'CustomerServices/getChartDataByStaff' ?>",
            method: 'GET',
            // dataType: 'JSON',
            data: {
                staffId: staffId
            },
            success: function(dataa) {
                let chartData = dataa.fu_status;
                data.labels = chartData.label;
                // data.datasets[0].labels = chartData.label;
                data.datasets[0].data = chartData.dataset;
                data.datasets[0].backgroundColor = chartData.bgcolor;
                // console.log(data.datasets[0]);
                myChart.update();
                // console.log(data.datasets[0]);
                // myChart.destroy();
                // myChart = new Chart(ctx, config);
            }
        })
    }

    function getTableDataByStaff(element) {
        let staffId = element.value;
        $.ajax({
            url: "<?php echo admin_url() . 'CustomerServices/getTableDataByStaff' ?>",
            method: 'GET',
            data: {
                staffId: staffId
            },
            beforeSend: function(dataa) {
                swal({text:'Loading..',button:false});
            },
            success: function(dataa) {
                console.log(dataa);
                $('#tbody_wrapper').html('');
                if(dataa.length > 0) {
                    dataa.forEach((value,index)=>{
                        let html = '';
                        html+='<tr>';
                        html+='<td>'+value.status+'</td>';
                        html+='<td>'+0+'</td>';
                        html+='<td>'+value.jumlah+'</td>';
                        html+='<td>'+0+'</td>';
                        html+='</tr>';
                        $('#tbody_wrapper').append(html);
                    })
                }
            }
        })
    }

</script>

</body>

</html>