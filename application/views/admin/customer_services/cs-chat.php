<?php

defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/chat.css" />
<div id="wrapper">
   <div class="content">
      <div class="row">

         <div class="col-md-12">
            <div class="panel_s mbot15">
               <div class="panel-body">
                  <div class="_buttons">
                     <h4 class="no-margin bold">Customer Services (<?php echo get_staff_full_name(); ?>)</h4>
                  </div>
                  <div class="pull-right text-right">
                     <?php if (isset($whatsappApi)) {
                        echo 'Whatsapp State: ' . $whatsappApi->status;
                        echo '<br>';
                        echo 'Message: ' . $whatsappApi->message;
                     } ?>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>

         <div class="col-md-12">
            <div class="text-right">
               <?php
                  echo $this->pagination->create_links();
               ?>
            </div>
            <div class="table-responsive">
               <table class="table table-striped">
                  <tr>
                     <th>#</th>
                     <th>nomor</th>
                     <th>chat name</th>
                     <th>tipe</th>
                     <th>FU</th>
                     <th>cabang</th>
                     <th>assignment</th>
                     <th>detail</th>
                  </tr>
                  <?php
                  // $no = $this->uri->segment('3') + 1;
                  foreach ($dataWa as $d) {
                  ?>
                     <tr>
                        <td><?php echo '<input type="checkbox" name="dialogs" class="form-control" value="' . $d->dialog_id . '">' ?></td>
                        <td><?php echo '<span class="text-primary" style="font-weight:600;">+'.str_replace('@c.us','',$d->dialog_id).'</span><br/><small>Last chat: '.date('d F Y H:i:s',$d->last_time).'</small>' ?></td>
                        <td><?php echo !empty($d->name) ? $d->name : $d->dialog_id; ?></td>
                        <td><?php echo (stripos($d->dialog_id,'g.us') === false) ? 'Personal' : 'Group'; ?></td>
                        <td><?php echo $d->staff_followup ?? '-' ?></td>
                        <td><?php echo $d->branch ?? '-' ?></td>
                        <td><?php echo $d->staff_assignment ?? '-'; ?></td>
                        <td><?php echo '<a class="btn btn-default btn-icon" href="javascript:;" onclick="viewData(\'' . $d->dialog_id . '\')"><i class="fa fa-eye"></i> </a>
                     <a class="btn btn-info btn-icon" href="javascript:;" onclick="editData(\'' . $d->dialog_id . '\')"><i class="fa fa-edit"></i> </a>
                     ' ?>
                        </td>
                     </tr>
                  <?php } ?>
               </table>
            </div>
            <br />
            <div class="text-right">
               <?php
                  echo $this->pagination->create_links();
               ?>
            </div>
         </div>
         <?php if (is_admin()) : ?>
            <div class="col-md-12">
               <label class="checkbox-inline"><input type="checkbox" id="checkAll" value="">Semua Pada Halaman ini</label>
               <br>
               <br>
               <button class="btn btn-primary" onclick="bulkAction()">Bulk Action</button>
            </div>
         <?php endif; ?>
      </div>
   </div>
</div>
<?php $this->load->view('admin/whatsapp/_form_modal_detail_chat_list'); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_edit_chat'); ?>
<?php $this->load->view('admin/whatsapp/_form_modal_assignment_bulk'); ?>
<?php init_tail(); ?>
<script src="<?php echo base_url() ?>assets/js/hoy3lrg.js"></script>
<!-- <script src="<?php //echo base_url() 
                  ?>assets/js/moment.min.js"></script> -->
<script>
   try {
      Typekit.load({
         async: true
      });
   } catch (e) {}
</script>
<script>
   window.onbeforeunload = null;

   $("#checkAll").click(function() {
      $('input:checkbox[name=dialogs]').not(this).prop('checked', this.checked);
   });

   function bulkAction() {
      $('#form_modal_bulk').modal('show');
   }

   function saveDataBulk() {
      window.onbeforeunload = null;
      let myArray = [];
      $("input:checkbox[name=dialogs]:checked").each(function() {
         myArray.push($(this).val());
      });

      var url = "<?= admin_url() ?>" + "whatsappapi/setAssignmentCsBulkFromDialog";

      $.ajax({
         url: url,
         type: 'POST',
         data: {
            staff_id: $('[name="bulk_staff_id"]').val(),
            dialog_ids: myArray
         },
         success: function(data) {
            if (data.success) {
               swal({
                  title: 'Berhasil Simpan Data',
                  text: data.message,
                  icon: 'success',
                  timer: '3000'
               }).then(() => {
                  $('#form_modal_bulk').modal('hide');
                  location.reload();
               });
            } else {
               swal({
                  title: 'Gagal Simpan Data',
                  text: data.message,
                  icon: 'error',
                  timer: '3000'
               }).then(() => {
                  $('#form_modal').modal('hide');
               });
            }
         },
         error: function(jqXHR, textStatus, errorThrown) {
            swal({
               title: 'System Error',
               text: errorThrown,
               icon: 'error',
               timer: '4000'
            }).then(() => {
               $('#form_modal').modal('hide');
            });
         }
      });
   }

   function viewData(id) {
      window.onbeforeunload = null;
      $.ajax({
         url: admin_url + 'whatsappapi/getChatByDialog/' + id,
         method: 'GET',
         // dataType: 'JSON',
         success: function(data) {
            console.log(data);
            $('#chats-wrapper').html('');
            $('#form_modal').modal('show');
            $('.modal-title').text('Detail Chats');
            // $('#waLinkChat').prop('href','https://wa.me/'+waNumber);
            $('#waLinkChat').attr('data-id', id);
            data.forEach((value, index) => {
               let html = '';
               html += renderChat(value);
               $('#chats-wrapper').prepend(html);
            })
            // $('[name="chat_id"]').val(data.chat_id);
            // $('[name="dialog_id"]').val(data.dialog_id);
            // $('[name="sender"]').val(data.sender);
            // $('[name="senderName"]').val(data.senderName);
            // $('[name="body"]').val(data.body);
            // $('[name="fromMe"]').val(data.fromMe);
            // $('[name="self"]').val(data.self);
            // $('[name="isForwarded"]').val(data.isForwarded);
            // $('[name="time"]').val(data.time);
            // $('[name="messageNumber"]').val(data.messageNumber);
            // $('[name="caption"]').val(data.caption);
            // $('[name="quotedMsgBody"]').val(data.quotedMsgBody);
            // $('[name="quotedMsgId"]').val(data.quotedMsgId);
            // $('[name="quotedMsgType"]').val(data.quotedMsgType);
            // if(data.chatName) {
            //   $('[name="chatName"]').val(data.chatName); 
            // } else {
            //   $('[name="chatName"]').val(data.senderName);
            // }
         }
      })
   }

   function editData(id) {
      window.onbeforeunload = null;
      $.ajax({
         url: admin_url + 'whatsappapi/getDialogByDialogIdFromDb/' + id,
         method: 'GET',
         // dataType: 'JSON',
         success: function(data) {
            $('#modalEdit').modal('show');
            $('.modal-title').text('Edit Chats Detail');
            $('#submitBtn').attr('data-id', id);
            $('#modalEdit select').val(data.branch_id).change();
            $('#modalEdit select').val(data.branch_id);
         }
      })
   }

   function saveData() {
      window.onbeforeunload = null;
      let id = $('#submitBtn').attr('data-id');
      let formData = {
         dialog_id: id,
         branch_id: $('[name="e_branch_id"]').val(),
      };

      $.ajax({
         url: '<?php echo base_url() ?>admin/customerservices/save',
         type: 'POST',
         dataType: 'JSON',
         data: formData,
         success: function(data) {
            // data = JSON.parse(data);
            if (data.success) {
               swal({
                  title: 'Berhasil Ubah Status',
                  text: data.message,
                  icon: 'success',
                  timer: '3000'
               }).then(() => {
                  $('#modalEdit').modal('hide');
                  window.onbeforeunload = null;
                  location.reload();
               });
            } else {
               swal({
                  title: 'Gagal Ubah Status',
                  text: data.message,
                  icon: 'error',
                  timer: '5000'
               })
            }
         },
         error: function(jqXHR, textStatus, errorThrown) {
            swal({
               title: 'System Error',
               text: errorThrown,
               icon: 'error',
               timer: '5000'
            }).then(() => {
               $('#form_modal').modal('hide');
            });
         }
      });
   }

   function openFromWa() {
      window.onbeforeunload = null;
      let id = $('#waLinkChat').attr('data-id');
      if (!id) {
         return '';
      }

      $.ajax({
         url: '<?php echo admin_url() ?>CustomerServices/followUpDialog/' + id,
         type: 'GET',
         async: true,
         beforeSend: function(data) {
            swal({
               text: 'redirecting...',
               button: false,
               time: 4000
            });
         },
         success: function(data) {
            location.href = "https://wa.me/" + data;
         },
         error: function(jqXHR, textStatus, errorThrown) {
            swal({
               icon: 'error',
               title: 'Something went wrong!',
               text: errorThrown,
               time: 4000
            });
         }
      });
   }

   function renderChat(chat) {
      window.onbeforeunload = null;
      let html = '';
      let chatFrom = 'sending';
      let labelFrom = 'text-right';
      let caption = '';
      if (chat.caption) {
         caption = chat.caption;
      }
      if (chat.fromMe == 0) {
         chatFrom = 'incoming';
         labelFrom = 'text-left';
      }
      html += '<div class="form-group ' + labelFrom + '">';
      html += '<label for="chat_id" class="control-label"><b>' + chat.senderName + '</b> (' + moment.unix(chat.time).format("llll") + ')</label>';
      if (chat.type == 'image') {
         html += '<p type="text" id="chat_id" class="' + chatFrom + '" ><img src="' + chat.body + '" height="100px"/></p><p>' + caption + '</p>';
      } else {
         html += '<p type="text" id="chat_id" class="' + chatFrom + '" >' + chat.body + '</p>';
      }
      html += '</div>';
      html += '<hr>';
      return html;
   }
</script>
</body>

</html>