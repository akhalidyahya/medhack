<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'bank_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group select-placeholder">
                            <label for="label-status" class="control-label">Status</label>
                            <select name="label-status" class="selectpicker" data-width="100%" id="label-status" data-none-selected-text="Klik untuk memilih Status" data-live-search="true">
                                <option value=""></option>
                                <?php foreach(\app\constants\CommonConstant::WA_DIALOG_STATUS_CHAT as $key => $label): ?>
                                    <option value="<?= $key ?>"><?php echo $label; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?php echo form_hidden('id'); ?>
                        <?php echo form_hidden('method'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="button" type="button" class="btn btn-info" onclick="saveStatus(event)"><?php echo _l('save'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
