<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'myForm']); ?>
            <input type="hidden" name="dialog_id">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="status" class="control-label">Status</label>
                            <select name="status" class="form-control" id="status" >
                                <option value=""></option>
                                <?php foreach(getFuSummaryStatus() as $data): ?>
                                    <option value="<?= $data->id ?>"><?php echo $data->status; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="note" class="control-label">Note</label>
                            <textarea id="note" class="form-control" type="text" name="note" rows="7"> </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="button" type="button" class="btn btn-info" onclick="saveData()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
