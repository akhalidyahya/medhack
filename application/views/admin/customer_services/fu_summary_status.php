<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="clearfix"></div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('fu_summary'); ?> Status</h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <a href="javascript:;" onclick="addData()" class="btn btn-info pull-right btn-icon display-block"> <i class="fa fa-plus"></i> Tambah Status Baru</a>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php
                        $table_data = array(
                            'Id',
                            'Status',
                            'Action'
                        );
                        render_datatable($table_data, 'fu_summary_status');
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel_s">
                    <div class="panel-body">
                        <form id="limitForm">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <div class="form-group">
                                <label for="limit" class="control-label">Limit Follow Up harian</label>
                                <input id="limit" class="form-control" type="text" name="limit" value="<?php echo get_option('fu_daily_limit') ?>" />
                            </div>
                            <button group="button" type="button" class="btn btn-info pull-right" onclick="saveDataLimit()">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/customer_services/_fu_summary_status_form_modal'); ?>
<?php init_tail(); ?>
<script>
    $(function() {
        init_current_table();
    });

    function init_current_table() {
        if ($.fn.DataTable.isDataTable('.table-fu_summary_status')) {
            $('.table-fu_summary_status').DataTable().destroy();
        }
        initDataTable('.table-fu_summary_status', window.location.href, [], [], null, [0, 'desc']);
    }
</script>
<script>
    function addData() {
        $('.modal-title').text('Tambah FU Summary Status Baru');
        $('#id').val(0);
        $('#form_modal form')[0].reset();
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#form_modal').modal('show');
    }
    function saveData() {
        var url = "<?= admin_url() ?>" + "CustomerServices/saveFuStatus";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#myForm').serialize(),
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                        $('.table-fu_summary_status').DataTable().ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    $('#form_modal').modal('hide');
                });
            }
        });
    }

    function saveDataLimit() {
        var url = "<?= admin_url() ?>" + "CustomerServices/setFuLimit";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#limitForm').serialize(),
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                });
            }
        });
    }

    function editData(id) {
        $('label.error').remove();
        var url = "<?= admin_url() ?>" + "CustomerServices/getStatusFuById/" + id;
        $('[name=method]').val('edit');
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                $('#form_modal').modal('show');
                $('.modal-title').text('Edit data');
                $('[name=id]').val(data.id);
                $('[name=status]').val(data.status);
            }
        })
    }

    function deleteData(id) {
        swal({
            title: "Yakin ingin hapus data?",
            text: "Data akan dihapus secara permanen",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Hapus!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "CustomerServices/deleteFuStatus/" + id
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Delete Data",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-fu_summary_status').DataTable().ajax.reload();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '4000'
                        });
                    }
                })
            }
        })
    }
</script>

</body>

</html>