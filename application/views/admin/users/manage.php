<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body _buttons">
            <a href="javascript:;" onclick="addData()" class="btn btn-info pull-right display-block"> <i class="fa fa-plus"></i> <?php echo _l('new_user'); ?></a>
            <div class="clearfix"></div>
            <hr class="hr-panel-heading" />
            <div class="row" id="contract_summary">
              <div class="col-md-12">
                <h4 class="no-margin text-success"><?php echo _l('user_lists'); ?></h4>
              </div>
              <div class="col-md-2 col-xs-6 border-right">
                <h3 class="bold" id="user_count"><?php echo @$user_count; ?></h3>
                <span class="text-info"><?php echo _l('user_summary'); ?></span>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="panel_s">
          <div class="panel-body">
            <div class="clearfix"></div>
            <?php
            $table_data = array(
              "Tanggal bergabung",
              _l('staff_dt_name'),
              _l('client_email'),
              _l('client_phonenumber'),
              'Cabang Asal',
              _l('contact_active'),
            );
            render_datatable($table_data, 'all-users');
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/users/_form_modal'); ?>
<?php init_tail(); ?>
<script>
  $(function() {
    initDataTable('.table-all-users', window.location.href, [4], [4], undefined, [0, 'desc']);
  });
</script>
<script>
  function addData() {
    $('.modal-title').text('Tambah User Baru');
    $('#id').val(0);
    $('#form_modal form')[0].reset();
    $('[name=method]').val('add');
    $('label.error').remove();
    $('#form_modal').modal('show');
  }

  function editData(id) {
    $('label.error').remove();
    var url = "<?= admin_url() ?>" + "users/getUserById/" + id;
    $('[name=method]').val('edit');
    $.ajax({
      url: url,
      method: 'GET',
      dataType: 'JSON',
      success: function(data) {
        $('#form_modal').modal('show');
        $('.modal-title').text('Edit Cabang');
        $('[name=id]').val(data.id);
        $('[name=firstname]').val(data.firstname);
        $('[name=lastname]').val(data.lastname);
        $('[name=email]').val(data.email);
        $('[name=phonenumber]').val(data.phonenumber);
        $('[name=address]').val(data.address);
        $('[name=nik]').val(data.nik);
        $('[name=tempat]').val(data.tempat);
        $('[name=tanggal_lahir]').val(data.tanggal_lahir);
        $('select[name=gender]').val(data.gender);
        $('select[name=pernah_operasi]').val(data.pernah_operasi);
        $('[name=riwayat_operasi]').val(data.riwayat_operasi);
        $('[name=nama_cs]').val(data.nama_cs);
        $('[name="branch_id"]').val(data.branch_id);
      }
    })
  }

  function saveData() {
    var url = "<?= admin_url() ?>" + "users/saveUser";

    $.ajax({
      url: url,
      type: 'POST',
      data: $('#user_form').serialize(),
      success: function(data) {
        // data = JSON.parse(data);
        if (data.success) {
          swal({
            title: 'Berhasil Simpan Data',
            text: data.message,
            icon: 'success',
            timer: '3000'
          }).then(() => {
            $('#form_modal').modal('hide');
            $('.table-all-users').DataTable().ajax.reload();
            if ($('[name=method]').val() == 'add') {
              let current_count = Number($('#user_count').text());
              current_count++;
              $('#user_count').text(current_count);
            }
          });
        } else {
          swal({
            title: 'Gagal Simpan Data',
            text: data.message,
            icon: 'error',
            timer: '3000'
          }).then(()=>{
            let text = '';
            let no = 0;
            Object.keys(data.error).forEach((key) => {
              no++;
              text+=no+'. ';
              text+=data.error[key];
              text+='\n';
            })
            swal({
              title: 'Periksa Data Kembali',
              text: text,
              icon: 'error',
              // timer: '3000'
            })
          });
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        swal({
          title: 'System Error',
          text: errorThrown,
          icon: 'error',
          timer: '4000'
        }).then(() => {
          $('#form_modal').modal('hide');
        });
      }
    });
  }

  function deleteData(id) {
    swal({
      title: "Yakin ingin hapus pelanggan?",
      text: "Data akan dihapus secara permanen",
      icon: "warning",
      buttons: {
        cancel: true,
        confirm: {
          text: 'Hapus!',
          closeModal: false,
        },
      },
    }).then(process => {
      if (process) {
        var url = "<?= admin_url() ?>" + "users/deleteUser/" + id
        $.ajax({
          url: url,
          method: 'GET',
          dataType: 'JSON',
          success: function(data) {
            if (data.success) {
              swal({
                title: "Berhasil Delete Pelaggan",
                text: `${data.message}`,
                icon: "success",
                timer: '3000'
              });
              $('.table-all-users').DataTable().ajax.reload();
              let current_count = Number($('#user_count').text());
              current_count--;
              $('#user_count').text(current_count);
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal({
              title: 'System Error',
              text: errorThrown,
              icon: 'error',
              timer: '4000'
            }).then(() => {
              $('#form_modal').modal('hide');
            });
          }
        })
      }
    })
  }
</script>
</body>

</html>