<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id' => 'user_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>* NIK</label><br>
                            <input class="form-control" type="text" name="nik" id="nik" placeholder="contoh: 01234567898765">
                        </div>
                        <?php echo render_input('firstname', 'clients_firstname', '', 'text', ['placeholder' => _l('clients_firstname')]); ?>
                        <?php echo render_input('lastname', 'clients_lastname', '', 'text', ['placeholder' => _l('clients_lastname')]); ?>
                        <div class="form-group">
                            <label>* Tempat</label><br>
                            <input class="form-control" type="text" name="tempat" id="tempat" placeholder="contoh: jakarta">
                        </div>
                        <?php echo render_date_input('tanggal_lahir', 'Tanggal'); ?>
                        <?php echo render_input('email', 'clients_email', '', 'text', ['placeholder' => _l('clients_email')]); ?>
                        <?php echo render_input('phonenumber', 'clients_phone', '', 'text', ['placeholder' => _l('input_phone_number')]); ?>
                        <?php echo render_input('password', 'client_password', '', 'password', ['placeholder' => 'minimal 6 karakter']); ?>
                        <?php echo render_textarea('address', 'clients_address'); ?>
                        <div class="form-group">
                            <label>* Jenis Kelamin</label><br>
                            <select name="gender" id="gender" class="form-control">
                                <?php foreach (app\constants\CommonConstant::GENDER_LISTS as $value => $label) : ?>
                                    <option value="<?= $value ?>"> <?= $label ?> </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Pernah Operasi</label><br>
                            <select name="pernah_operasi" id="pernah_operasi" class="form-control">
                                <option value="0">Tidak</option>
                                <option value="1">Ya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Riwayat Operasi </label><small> (Bila ada)</small> <br>
                            <textarea class="form-control" name="riwayat_operasi" id="riwayat_operasi"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Nama CS </label><small> (Bila ada)</small> <br>
                            <input class="form-control" type="text" name="nama_cs" id="nama_cs" placeholder="contoh: John Doe">
                        </div>
                        <div class="form-group">
                            <label for="branch_id" class="control-label">Cabang</label>
                            <select id="branch_id" name="branch_id" data-width="100%" class="form-control">
                                <?php foreach (getAllBranchs() as $branch) : ?>
                                    <option value="<?php echo $branch->id ?>"><?php echo $branch->branch ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?php echo form_hidden('id'); ?>
                        <?php echo form_hidden('method'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="button" type="button" class="btn btn-info" onclick="saveData()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>