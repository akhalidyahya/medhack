<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="clearfix"></div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('banks_master'); ?></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="room_total"><?php echo @$bank_total; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Bank</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <a href="javascript:;" onclick="addData()" class="btn btn-info pull-right btn-icon display-block"> <i class="fa fa-plus"></i> <?php echo _l('new_bank'); ?></a>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php
                        $table_data = array(
                            'Bank',
                            'No Rekening',
                            'Atas Nama',
                            'Cabang',
                            'Aksi',
                        );
                        render_datatable($table_data, 'bank');
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/banks/_form_modal'); ?>
<?php init_tail(); ?>
<script>
    $(function() {
        init_current_table();
    });

    function init_current_table() {
        initDataTable('.table-bank', window.location.href, [3], [3], undefined, [0, 'asc']);
    }
</script>
<script>
    function addData() {
        $('.modal-title').text('Tambah Bank Baru');
        $('#id').val(0);
        $('#form_modal form')[0].reset();
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#form_modal').modal('show');
    }
    function saveData() {
        var url = "<?= admin_url() ?>" + "banks/saveBank";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#bank_form').serialize(),
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                        $('.table-bank').DataTable().ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    $('#form_modal').modal('hide');
                });
            }
        });
    }

    function editData(id) {
        $('label.error').remove();
        var url = "<?= admin_url() ?>" + "banks/getBankById/" + id;
        $('[name=method]').val('edit');
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                $('#form_modal').modal('show');
                $('.modal-title').text('Edit bank');
                $('[name=id]').val(data.id);
                $('[name=bank_name]').val(data.bank_name);
                $('[name=acc_number]').val(data.acc_number);
                $('[name=person_name]').val(data.person_name);
            }
        })
    }

    function deleteData(id) {
        swal({
            title: "Yakin ingin hapus bank?",
            text: "Data akan dihapus secara permanen",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Hapus!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "banks/deleteBank/" + id
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Delete Data",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-bank').DataTable().ajax.reload();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '4000'
                        });
                    }
                })
            }
        })
    }

</script>

</body>

</html>