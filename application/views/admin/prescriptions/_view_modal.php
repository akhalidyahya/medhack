<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('prescriptions/saveData', ['id' => 'user_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_datetime_input('timestamp', 'Tanggal & Waktu'); ?>
                        <!-- <div class="form-group" app-field-wrapper="patient_id">
                            <label for="patient_id" class="control-label"> Pasien</label>
                        </div> -->
                        <div class="form-group select-placeholder">
                            <label for="patient_id" class="control-label">Pilih Pasien</label>
                            <select name="patient_id" class="selectpicker no-margin ajax-search" data-width="100%" id="patient_id" data-none-selected-text="Klik untuk mencari Pasien" data-live-search="true">
                                <option value=""></option>
                            </select>
                        </div>
                        <?php echo render_textarea('complaint', 'Keluhan'); ?>
                        <?php echo render_textarea('case_history', 'Diagnosa'); ?>
                        <?php echo render_textarea('action', 'Tindakan'); ?>
                        <?php echo render_textarea('response', 'Respon'); ?>
                        <?php echo render_textarea('lintah', 'Lintah'); ?>
                        <?php echo render_textarea('note', 'Kontrol'); ?>
                        <?php echo render_textarea('ramuan', 'Ramuan'); ?>
                        <?php echo render_textarea('rekomendasi', 'Rekomendasi'); ?>
                        <?php echo form_hidden('id'); ?>
                        <?php echo form_hidden('method'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="button" type="button" class="btn btn-info" onclick="saveData()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>