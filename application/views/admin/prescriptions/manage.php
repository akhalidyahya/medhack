<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <?php if(has_permission('prescriptions','','edit')): ?>
                        <a href="javascript:;" onclick="addData()" class="btn btn-info pull-right display-block"><?php echo _l('new_prescription'); ?></a>
                        <?php endif; ?>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('prescriptions_summary_heading'); ?></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="history_count"><?php echo $history_count; ?></h3>
                                <span class="text-info"><i class="fa fa-list"></i> Jumlah Total history</span>
                            </div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="appointment_count"><?php echo $history_patient; ?></h3>
                                <span class="text-info"><i class="fa fa-users"></i> Jumlah Pasien</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo render_date_input('from_date', 'from_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo render_date_input('to_date', 'to_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group select-placeholder">
                                    <label for="s_patient" class="control-label">Pilih Pasien</label>
                                    <select name="s_patient" class="selectpicker no-margin ajax-search" data-width="100%" id="s_patient" data-none-selected-text="Klik untuk mencari Pasien" data-live-search="true">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        $table_data = array(
                            'Tanggal',
                            'Waktu',
                            'Pasien',
                            'Paket',
                            'Cabang',
                            'Aksi',
                        );
                        render_datatable($table_data, 'all-prescription');
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/prescriptions/_form_modal'); ?>
<?php init_tail(); ?>
<script src="<?php echo base_url() ?>assets/plugins/datetimepicker/jquery.datetimepicker.full.js"></script>
<script>
    // $('#myTable').DataTable();
    $(function() {
        init_current_table();
        init_ajax_search('users', '#patient_id.ajax-search', undefined, admin_url + 'users/search');
        init_ajax_search('users', '#s_patient.ajax-search', undefined, admin_url + 'users/search');
        init_ajax_search('appointments', '#appointment_id.ajax-search', undefined, admin_url + 'appointments/search');

        $("body").on('change', 'select[name="s_patient"]', function() {
            init_current_table();
        });
        $("body").on('change', 'select[name="appointment_id"]', function() {
            let value = $(this).val();
            let data = value.split('#');
            $('select[name=patient_id]').append('<option value="' + data[0] + '">' + data[1] + '</option>');
            $('select[name=patient_id]').val(data[0]);
            $('#patient_id').selectpicker('val', data[0]);
            $('#patient_id').trigger('change');
            $('[name="timestamp"]').val(data[2]);
            $('[name="branch_id"]').val(data[3]);
        });

        $('input[name="from_date"]').on('change', function() {
            init_current_table();
        });

        $('input[name="to_date"]').on('change', function() {
            init_current_table();
        });

    });

    function init_current_table() {
        if ($.fn.DataTable.isDataTable('.table-all-prescription')) {
            $('.table-all-prescription').DataTable().destroy();
        }
        fnServerParams = {
            "from_date": '[name="from_date"]',
            "to_date": '[name="to_date"]',
            "s_patient": '[name="s_patient"]'
        };
        initDataTable('.table-all-prescription', window.location.href, [3], [3], fnServerParams, [0, 'desc']);
        
    }
</script>

<script>
    function addData() {
        $('.modal-title').text('Tambah Riwayat Pasien Baru');
        $('#id').val(0);
        $('#form_modal form')[0].reset();
        $('#patient_id').selectpicker('val', '');
        $('#patient_id').trigger('change');
        $('#appointment_id').selectpicker('val', '');
        $('#appointment_id').trigger('change');
        $('.selectpicker').attr('disabled',false);
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#submitBtn').show();
        $('#form_modal').modal('show');
    }

    function viewData(id, method) {
        var url = "<?= admin_url() ?>" + "prescriptions/getPrescriptionById/" + id;
        $('[name=method]').val('edit');

        if (method == 'view') {
            $('#submitBtn').hide();
            $('#field_appointment').hide();
            $('.selectpicker').attr('disabled',true);
        } else {
            $('#submitBtn').show();
            $('#field_appointment').show();
            $('.selectpicker').attr('disabled',false);
        }

        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                console.log(data);
                $('#form_modal').modal('show');
                $('.modal-title').text('Riwayat Pasien');
                $('[name=id]').val(data.prescription_id);
                $('[name="timestamp"]').val(data.timestamp);
                $('select[name=patient_id]').append('<option value="' + data.patient_id + '">' + data.full_name + '</option>');
                $('select[name=patient_id]').val(data.patient_id);
                $('#patient_id').selectpicker('val', data.patient_id);
                $('#patient_id').trigger('change');
                $('[name="complaint"]').val(data.complaint);
                $('[name="case_history"]').val(data.case_history);
                // $('[name="medication"]').val(data.medication);
                $('[name="action"]').val(data.action);
                $('[name="response"]').val(data.response);
                $('[name="lintah"]').val(data.lintah);
                $('[name="note"]').val(data.note);
                $('[name="ramuan"]').val(data.ramuan);
                $('[name="rekomendasi"]').val(data.rekomendasi);
                $('[name="keterangan_gambar_view"]').attr("src", "<?php echo site_url()?>" +'/uploads/prescriptions/' +data.keterangan_gambar);
            }
        })
    }

    function deleteData(id) {
        swal({
            title: "Yakin ingin hapus riwayat pasien?",
            text: "Data akan dihapus secara permanen",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Hapus!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "prescriptions/deletePrescription/" + id
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Delete Riwayat Pasien",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-all-prescription').DataTable().ajax.reload();
                        }
                    }
                })
            }
        })
    }

    function saveData() {
        var url = "<?= admin_url() ?>" + "prescriptions/savePrescription";

        $.ajax({
            url: url,
            type: 'POST',
            data: new FormData($("#prescription_form")[0]),
            processData: false,
            contentType: false,
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                        $('.table-all-prescription').DataTable().ajax.reload();
                        calendar.refetchEvents();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        // $('#form_modal').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    $('#form_modal').modal('hide');
                });
            }
        });
    }
</script>

</body>

</html>