<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('#', ['id' => 'prescription_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group select-placeholder" id="field_appointment">
                            <label for="appointment_id" class="control-label">Berdasarkan Jadwal</label>
                            <select name="appointment_id" class="selectpicker no-margin ajax-search" data-width="100%" id="appointment_id" data-none-selected-text="Ketik nama pasien atau tanggal" data-live-search="true">
                                <option value=""></option>
                            </select>
                        </div>

                        <div class="form-group">
                            atau
                        </div>

                        <?php echo render_datetime_input('timestamp', 'Tanggal & Waktu'); ?>
                        <!-- <div class="form-group" app-field-wrapper="patient_id">
                            <label for="patient_id" class="control-label"> Pasien</label>
                        </div> -->
                        <div class="form-group select-placeholder">
                            <label for="patient_id" class="control-label">Pasien</label>
                            <select name="patient_id" class="selectpicker no-margin ajax-search" data-width="100%" id="patient_id" data-none-selected-text="Klik untuk mencari Pasien" data-live-search="true">
                                <option value=""></option>
                            </select>
                        </div>
                        <?php echo render_textarea('complaint', 'Keluhan'); ?>
                        <?php echo render_textarea('case_history', 'Diagnosa'); ?>
                        <?php echo render_textarea('action', 'Tindakan'); ?>
                        <?php echo render_textarea('response', 'Respon'); ?>
                        <?php echo render_textarea('lintah', 'Lintah'); ?>
                        <?php echo render_textarea('note', 'Kontrol'); ?>
                        <?php echo render_textarea('ramuan', 'Ramuan'); ?>
                        <?php echo render_textarea('rekomendasi', 'Rekomendasi'); ?>
                        <?php //echo render_textarea('medication', 'Resep Obat'); 
                        ?>
                        <?php echo form_hidden('id'); ?>
                        <?php echo form_hidden('method'); ?>
                        <?php echo form_hidden('branch_id'); ?>
                        <label for="keterangan_gambar" class="control_label">Gambar</label> 
                        <input type="file" name="keterangan_gambar" id="keterangan_gambar" style="border: none" class="form-control">
                        <img src="" name="keterangan_gambar_view" id="keterangan_gambar" width="30%" height="30%"/>


                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button id="submitBtn" group="button" type="button" class="btn btn-info" onclick="saveData()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>