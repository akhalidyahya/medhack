<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="clearfix"></div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('terms_and_conditions'); ?></h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <textarea name="form-control" id="ckeditor" cols="30" rows="10"><?php echo $terms; ?></textarea>
                        <br>
                        <button class="btn btn-primary btn-icon pull-right" onclick="saveData()"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php init_tail(); ?>
<script>
    $(function() {
        CKEDITOR.replace( 'ckeditor' );
    });
</script>
<script>
    function saveData() {
        var url = "<?= admin_url() ?>" + "Terms_and_conditions/save";

        $.ajax({
            url: url,
            type: 'POST',
            data: {text:CKEDITOR.instances.ckeditor.getData()},
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                })
            }
        });
    }

</script>

</body>

</html>