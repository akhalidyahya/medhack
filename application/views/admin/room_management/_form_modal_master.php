<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'master_room_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('bed_number', 'Nomor Ruangan','','text',['placeholder'=>'Nomor Ruangan']); ?>
                        <div class="form-group select-placeholder">
                            <label for="type" class="control-label">Jenis Ruangan</label>
                            <select name="type" class="selectpicker no-margin" data-width="100%" id="type" data-none-selected-text="Pilih jenis ruangan" data-live-search="true">
                                <option value=""></option>
                                <?php foreach($bed_types as $bed_type): ?>
                                <option value="<?php echo $bed_type->id ?>"><?php echo $bed_type->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <?php echo render_textarea('description', 'Deskripsi Ruangan'); ?>
                        <?php echo form_hidden('id'); ?>
                        <?php echo form_hidden('method'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="button" type="button" class="btn btn-info" onclick="saveData()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
