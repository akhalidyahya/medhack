<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('room_allocation'); ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel_s">
                    <div class="panel-body">
                    <?php if(has_permission('room_management','','edit')): ?>
                    <a href="javascript:;" onclick="addData()" class="btn btn-info pull-right display-block">Buat Alokasi</a>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                    <hr class="hr-panel-heading" />
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo render_date_input('from_date', 'Tanggal Masuk'); ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo render_date_input('to_date', 'Tanggal Keluar'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group select-placeholder">
                                    <label for="s_patient" class="control-label">Pasien</label>
                                    <select name="s_patient" class="selectpicker no-margin ajax-search" data-width="100%" id="s_patient" data-none-selected-text="Klik untuk mencari Pasien" data-live-search="true">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group select-placeholder">
                                    <label for="s_room" class="control-label">Ruangan</label>
                                    <select name="s_room" class="selectpicker no-margin ajax-search" data-width="100%" id="s_room" data-none-selected-text="Klik untuk memilih ruangan" data-live-search="true">
                                        <option value=""></option>
                                        <?php foreach($beds as $bed): ?>
                                        <option value="<?php echo $bed->bed_id; ?>"><?php echo $bed->bed_number; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                            $table_data = array(
                                'Tanggal Masuk',
                                'Ruangan',
                                'Pasien',
                                'Status',
                                'Tanggal Keluar',
                                'Aksi',
                            );
                            render_datatable($table_data, 'bed_allotment');
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="clearfix"></div>
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="dt-loader hide"></div>
                        <?php $this->load->view('admin/utilities/calendar_filters'); ?>
                        <div id="calendar_room"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/room_management/_form_modal_allocation'); ?>
<?php init_tail(); ?>
<script src="<?php echo base_url(); ?>assets/js/admin_room_management_custom_calendar.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datetimepicker/jquery.datetimepicker.full.js"></script>
<script>
    // $('#myTable').DataTable();
    $(function() {
        init_current_table();
        init_ajax_search('users', '#patient_id.ajax-search', undefined, admin_url + 'users/search');
        init_ajax_search('users', '#s_patient.ajax-search', undefined, admin_url + 'users/search');
        
        $('input[name="from_date"]').on('change', function() {
            init_current_table();
        });

        $('input[name="to_date"]').on('change', function() {
            init_current_table();
        });

        $("body").on('change', 'select[name="s_patient"]', function() {
            init_current_table();
            selected_user = this.value;
            calendar.refetchEvents();
        });
        $("body").on('change', 'select[name="s_room"]', function() {
            init_current_table();
            selected_room = this.value;
            calendar.refetchEvents();
        });
    });

    function init_current_table() {
        if ($.fn.DataTable.isDataTable('.table-bed_allotment')) {
            $('.table-bed_allotment').DataTable().destroy();
        }
        fnServerParams = {
            "from_date" : '[name="from_date"]',
            "to_date"   : '[name="to_date"]',
            "s_patient" : '[name="s_patient"]',
            "s_room"    : '[name="s_room"]',
        };
        initDataTable('.table-bed_allotment', window.location.href, [5], [5], fnServerParams, [0, 'desc']);
    }
</script>
<script>
    function addData() {
        $('.modal-title').text('Tambah Alokasi Baru');
        $('#id').val(0);
        $('#form_modal form')[0].reset();
        $('[name="bed_id"]').val('').selectpicker('refresh');
        $('select[name=patient_id]').html('');
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#form_modal').modal('show');
    }

    // function getAvailableRoom()
    // {
    //     var url = "<?php //echo admin_url() ?>" + "room_management/getAvailableRoom";
    //     var dateValue = $('[name="allotment_timestamp"]').val();
    //     console.log(dateValue);
    //     $.ajax({
    //         url: url,
    //         method: 'GET',
    //         dataType: 'JSON',
    //         data: {date:dateValue},
    //         success: function(data) {
    //             console.log(data);
    //             if(data.length > 0) {
    //                 $('[name="bed_id"]').html('');
    //                 data.forEach((value,index)=>{
    //                     $('[name="bed_id"]').append('<option value="'+value.bed_id+'">'+value.bed_number+'</option>');
    //                 })
    //                 $('[name="bed_id"]').selectpicker('refresh');
    //                 // $('#patient_id').trigger('change');
    //             }
    //         },
    //         error: function(e) {
    //             alert('error happen');
    //         }
    //     })
    // }

    function editData(id) {
        $('#patient_id').find('option').remove();
        $('#patient_id').selectpicker('refresh');
        // $('#bed_id').find('option').remove();
        // $('#bed_id').selectpicker('refresh');
        var url = "<?= admin_url() ?>" + "room_management/getAllotment/" + id;
        $('[name=method]').val('edit');

        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                $('#form_modal').modal('show');
                $('.modal-title').text('Edit Alokasi');
                $('[name=id]').val(data.id);
                $('[name="allotment_timestamp"]').val(data.allotment_timestamp);
                $('[name="discharge_timestamp"]').val(data.discharge_timestamp);
                $('#patient_id').val(data.bed_id);
                $('select[name=patient_id]').append('<option value="' + data.patient_id + '">' + data.full_name + '</option>');
                $('select[name=patient_id]').selectpicker('val', data.patient_id).selectpicker('refresh');
                $('select[name=bed_id]').selectpicker('val', data.bed_id).selectpicker('refresh');
            }
        })
    }

    function saveData()
    {
        var url = "<?= admin_url() ?>" + "room_management/saveAllotment";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#allotment_room_form').serialize(),
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                        $('.table-bed_allotment').DataTable().ajax.reload();
                        calendar.refetchEvents();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    $('#form_modal').modal('hide');
                });
            }
        });
    }

    function deleteData(id) {
        swal({
            title: "Yakin ingin hapus alokasi ruangan?",
            text: "Data akan dihapus secara permanen",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Hapus!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "room_management/deleteAllotment/" + id
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Delete Alokasi Ruangan",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-bed_allotment').DataTable().ajax.reload();
                        }
                    }
                })
            }
        })
    }

</script>
</body>

</html>