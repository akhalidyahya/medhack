<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="clearfix"></div>
                        <!-- <hr class="hr-panel-heading" /> -->
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('room_management_master_summary_heading'); ?></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="room_total"><?php echo $room_total; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Ruangan</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-8">
                <div class="panel_s">
                    <div class="panel-body">
                        <a href="javascript:;" onclick="addData()" class="btn btn-info pull-right display-block"><?php echo _l('new_room'); ?></a>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php
                        $table_data = array(
                            'No Ruangan',
                            'Jenis',
                            // 'Status',
                            'Deskripsi',
                            'Aksi',
                        );
                        render_datatable($table_data, 'master-room');
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel_s">
                    <div class="panel-body">
                        <a href="javascript:;" onclick="addBedType()" class="btn btn-info pull-right display-block">Jenis Ruangan</a>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <?php
                        $table_data = array(
                            'Jenis',
                            'Aksi',
                        );
                        render_datatable($table_data, 'master-bed-type');
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/room_management/_form_modal_master'); ?>
<?php $this->load->view('admin/room_management/_form_modal_bed_type'); ?>
<?php init_tail(); ?>
<script>
    $(function() {
        init_current_table();
    });

    function init_current_table() {
        initDataTable('.table-master-room', window.location.href, [3], [3], undefined, [0, 'asc']);
        initDataTable('.table-master-bed-type', admin_url + 'room_management/bed_types_table', [1], [1], undefined, [0, 'asc']);
    }
</script>
<script>
    function addData() {
        $('.modal-title').text('Tambah Ruangan Baru');
        $('#id').val(0);
        $('#form_modal form')[0].reset();
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#form_modal').modal('show');
    }
    function saveData() {
        var url = "<?= admin_url() ?>" + "room_management/masterDataSave";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#master_room_form').serialize(),
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                        $('.table-master-room').DataTable().ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    $('#form_modal_bed_type').modal('hide');
                });
            }
        });
    }

    function editData(id) {
        $('label.error').remove();
        var url = "<?= admin_url() ?>" + "room_management/getMasterDataById/" + id;
        $('[name=method]').val('edit');
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                $('#form_modal').modal('show');
                $('.modal-title').text('Edit ruangan');
                $('[name=id]').val(data.bed_id);
                $('[name=bed_number]').val(data.bed_number);
                $('[name=description]').val(data.description);
                // $('select[name=type]').append('<option value="' + data.patient_id + '">' + data.full_name + '</option>');
                $('select[name=type]').val(data.type);
                $('#type').selectpicker('val', data.type);
            }
        })
    }

    function deleteData(id) {
        swal({
            title: "Yakin ingin hapus ruangan?",
            text: "Data akan dihapus secara permanen",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Hapus!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "room_management/deleteMasterData/" + id
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Delete Data",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-master-room').DataTable().ajax.reload();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '4000'
                        });
                    }
                })
            }
        })
    }

</script>
<script>
    function addBedType() {
        $('.modal-title').text('Tambah Jenis Ruangan');
        $('#id_bed_type').val(0);
        $('#form_modal_bed_type form')[0].reset();
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#form_modal_bed_type').modal('show');
    }

    function editBedType(id) {
        $('label.error').remove();
        var url = "<?= admin_url() ?>" + "room_management/getBedTypeById/" + id;
        $('[name=method]').val('edit');
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data) {
                $('#form_modal_bed_type').modal('show');
                $('.modal-title').text('Edit Jenis ruangan');
                $('[name=id_bed_type]').val(data.id);
                $('[name=name]').val(data.name);
            }
        })
    }

    function saveBedType() {
        var url = "<?= admin_url() ?>" + "room_management/bed_type_save";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#bed_type_form').serialize(),
            success: function(data) {
                // data = JSON.parse(data);
                if (data.success) {
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal_bed_type').modal('hide');
                        $('.table-master-bed-type').DataTable().ajax.reload();
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(() => {
                        $('#form_modal_bed_type').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(() => {
                    $('#form_modal_bed_type').modal('hide');
                });
            }
        });
    }

    function deleteBedType(id) {
        swal({
            title: "Yakin ingin hapus jenis ruangan?",
            text: "Data akan dihapus secara permanen",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Hapus!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "room_management/deleteBedType/" + id
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Delete Data",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-master-bed-type').DataTable().ajax.reload();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '4000'
                        }).then(() => {
                            $('#form_modal_bed_type').modal('hide');
                        });
                    }
                })
            }
        })
    }
</script>
</body>

</html>