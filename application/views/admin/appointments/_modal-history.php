<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('users'); ?>
                </h4>
            </div>
            <?php echo form_open('#', ['id' => 'historyForm']); ?>
            <input type="hidden" name="doctor_id" value="<?php echo get_staff_user_id() ?>">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_textarea('complaint', 'Keluhan'); ?>
                        <?php echo render_textarea('case_history', 'Diagnosa'); ?>
                        <?php echo render_textarea('action', 'Tindakan'); ?>
                        <?php echo render_textarea('response', 'Respon'); ?>
                        <?php echo render_textarea('lintah', 'Lintah'); ?>
                        <?php echo render_textarea('note', 'Kontrol'); ?>
                        <?php echo render_textarea('ramuan', 'Ramuan'); ?>
                        <?php echo render_textarea('rekomendasi', 'Rekomendasi'); ?>
                        <?php echo form_hidden('appointment_id'); ?>
                        <?php echo form_hidden('method'); ?>
                        <label for="keterangan_gambar" class="control_label">Gambar</label> 
                        <input type="file" name="keterangan_gambar" id="keterangan_gambar" style="border: none" class="form-control">
                        <img src="" name="keterangan_gambar_view" id="keterangan_gambar" width="30%" height="30%"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button id="submitBtn" group="button" type="button" class="btn btn-info" onclick="saveHistory()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>