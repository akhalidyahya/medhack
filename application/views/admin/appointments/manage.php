<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <?php if (has_permission('appointment', '', 'edit')): ?>
                        <a href="javascript:;" onclick="createAppointment()" class="btn btn-info pull-right display-block">Tambah Janji</a>
                        <?php endif; ?>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('appointment_summary_heading'); ?></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="appointment_count"><?php echo $appointment_count; ?></h3>
                                <span class="text-info"><i class="fa fa-circle"></i> Jumlah Total Janji</span>
                            </div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="appointment_count"><?php echo $appointment_pending_count; ?></h3>
                                <span class="text-info"><i class="fa fa-circle" style="color:orange"></i> Jumlah Janji Perlu Dikonfirmasi</span>
                            </div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="appointment_count"><?php echo $appointment_approved_count; ?></h3>
                                <span class="text-info"><i class="fa fa-circle" style="color:#0abb87"></i> Jumlah Janji Disetujui</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo render_date_input('from_date', 'from_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo render_date_input('to_date', 'to_date'); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group select-placeholder">
                                    <label for="s_patient" class="control-label">Pasien</label>
                                    <select name="s_patient" class="selectpicker no-margin ajax-search" data-width="100%" id="s_patient" data-none-selected-text="Klik untuk mencari Pasien" data-live-search="true">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        $table_data = array(
                            'Tanggal',
                            'Waktu',
                            'Pasien',
                            'Pesan',
                            'Terapis',
                            'Cabang',
                            'Status',
                            'Aksi',
                        );
                        render_datatable($table_data, 'all-appointment');
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="clearfix"></div>
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="dt-loader hide"></div>
                        <?php $this->load->view('admin/utilities/calendar_filters'); ?>
                        <div id="calendar_appointment"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
<!-- Modal pilih terapis -->
<div class="modal fade" id="doctorModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pilih Terapis</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form id="doctorForm">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="hidden" name="appointment_id">
                        <input type="hidden" name="patient_id">
                        <div class="form-group">
                            <label for="inputState">Cabang</label>
                            <select class="form-control" id="branch_id" name="branch_id">
                                <option selected>Pilih Cabang...</option>
                                <?php foreach ($branch as $key => $value) { ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->branch; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group list_doctor">

                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="submit" type="button" class="btn btn-primary" onclick="saveDoctor()">Kirim</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/appointments/_modal-add'); ?>
<?php $this->load->view('admin/appointments/_modal-history'); ?>
<?php init_tail(); ?>
<script src="<?php echo base_url(); ?>assets/js/admin_custom_calendar.js"></script>
<script>
    // $('#myTable').DataTable();
    $(function() {
        init_current_table();
        init_ajax_search('users', '#s_patient.ajax-search', undefined, admin_url + 'users/search');
        init_ajax_search('users', '#f_patient.ajax-search', undefined, admin_url + 'users/search');
        init_ajax_search('users', '#h_patient.ajax-search', undefined, admin_url + 'users/search');
        
        $('input[name="from_date"]').on('change', function() {
            init_current_table();
        });

        $('input[name="to_date"]').on('change', function() {
            init_current_table();
        });

        $("body").on('change', 'select[name="s_patient"]', function() {
            init_current_table();
            selected_user = this.value;
            calendar.refetchEvents();
        });

        $("#datetimepicker").datetimepicker({
            format: 'Y-m-d H:m',
            disabledTimeIntervals: [ [ moment().hour(0), moment().hour(7) ], [ moment().hour(21), moment().hour(24) ] ],
            stepping: 15,
            stepMinute : 15,
        });
    });

    function init_current_table() {
        if ($.fn.DataTable.isDataTable('.table-all-appointment')) {
            $('.table-all-appointment').DataTable().destroy();
        }
        fnServerParams = {
            "from_date": '[name="from_date"]',
            "to_date": '[name="to_date"]',
            "s_patient": '[name="s_patient"]',
        };
        initDataTable('.table-all-appointment', window.location.href, [3, 4, 5], [3, 4, 5], fnServerParams, [0, 'desc']);
    }
</script>
<script>
    function createAppointment() {
        $('label.error').remove();
        $('#appointmentModal form')[0].reset();
        $('#appointmentModal').modal('show');
    }

    function createHistory(id,name,view = null) {
        $('.modal-title').text('Riwayat Treatment Pasien '+name);
        $('#historyModal form')[0].reset();
        $('[name="appointment_id"]').val(id);
        $('.selectpicker').attr('disabled',false);
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#submitBtn').show();
        if(view) {
            $('#submitBtn').hide();
        }
        $.ajax({
            url: "<?= admin_url() ?>" + "prescriptions/getPrescriptionByAppointmentId/" + id,
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function() {
                swal({text:'Loading..',button:false,timer:1000});
            },
            success: function(data) {
                console.log(data);
                if(data) {
                    $('[name="complaint"]').val(data.complaint);
                    $('[name="case_history"]').val(data.case_history);
                    $('[name="action"]').val(data.action);
                    $('[name="response"]').val(data.response);
                    $('[name="lintah"]').val(data.lintah);
                    $('[name="note"]').val(data.note);
                    $('[name="keterangan_gambar_view"]').attr("src", "<?php echo site_url()?>" +'/uploads/prescriptions/' +data.keterangan_gambar);
                }
                $('#historyModal').modal('show');
            }
        })
    }

    function saveHistory() {
        $.ajax({
            url: "<?php echo admin_url(); ?>prescriptions/savePrescriptionByDoctor",
            method: "POST",
            data: new FormData($("#historyForm")[0]),
            processData: false,
            contentType: false,
            async: true,
            beforeSend: function() {
                swal({text:'Loading..',button:false,timer:1000});
            },
            success: function(data) {
                if(data.success) {
                    swal({icon:'success',title:'Berhasil disimpan!',text:data.message,timer:3000}).then(()=>{
                        $('.table-all-appointment').DataTable().ajax.reload();
                        $('#historyModal').modal('hide');
                    });
                } else {
                    swal({icon:'error',title:'Gagal disimpan!',text:data.message,timer:3000}).then(()=>{
                        $('#historyModal').modal('hide');
                    });
                }
            }
        });
    }

    function saveAppointment() {
        $.ajax({
            url: "<?php echo admin_url(); ?>appointments/save_appointment",
            method: "POST",
            data: $('#addForm').serialize(),
            async: true,
            beforeSend: function() {
                swal({text:'Loading..',button:false,timer:1000});
            },
            success: function(data) {
                if(data.status) {
                    swal({icon:'success',title:'Berhasil disimpan!',text:data.message,timer:3000}).then(()=>{
                        $('.table-all-appointment').DataTable().ajax.reload();
                        $('#appointmentModal').modal('hide');
                    });
                } else {
                    swal({icon:'error',title:'Gagal disimpan!',text:data.message,timer:3000}).then(()=>{
                        $('#appointmentModal').modal('hide');
                    });
                }
            }
        });
    }

    function changeStatus(id, status) {
        let statusList = [];
        <?php foreach (app\constants\CommonConstant::APPOINTMENT_STATUS_LIST as $key => $label) : ?>
            statusList["<?php echo $key; ?>"] = "<?php echo strtoupper($label); ?>"
        <?php endforeach; ?>
        
        swal({
            title: `Status akan diubah!`,
            text: `Status perjanjian akan dirubah menjadi ${statusList[status]}`,
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Confirm!',
                    closeModal: false,
                },
            },
        }).then(process => {
            if (process) {
                var url = "<?= admin_url() ?>" + "appointments/setStatus/" + id;
                var branch = $('#branch'+id).val();
                var patient_id = $('#patient_id'+id).val();
                var invoice_id = $('#invoice_id'+id).val();
                var redirect_done = "<?= base_url(); ?>"+ 'admin/invoices/invoice/';
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        status: status,
                        branch: branch,
                        patient_id: patient_id
                    },
                    success: function(data) {
                        if (data.success) {
                            swal({
                                title: "Berhasil Ubah Status",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            if (data.invoice_id) {
                                window.location.href = redirect_done+data.invoice_id;
                            } else {
                                $('.table-all-appointment').DataTable().ajax.reload();
                            }
                            
                        } else {
                            swal({
                                title: "Gagal Ubah Status",
                                text: `${data.message}`,
                                icon: "error",
                                timer: '3000'
                            });
                        }
                    }
                })
            }
        })
    }

    function chooseDoctor(id,patient) {
        $('label.error').remove();
        $('[name="appointment_id"]').val(id);
        $('[name="patient_id"]').val(patient);
        $('#doctorModal form')[0].reset();
        $('#doctorModal').modal('show');
    }

    $('#branch_id, #form_branch_id').change(function() {
        var id = $(this).val();
        $.ajax({
            url: "<?php echo base_url(); ?>appointment/list_doctor",
            method: "POST",
            data: {
                branch_id: id
            },
            async: true,
            dataType: 'json',
            beforeSend: function() {
                swal({text:'Loading..',button:false,timer:1000});
            },
            success: function(data) {
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i].staffid + '>' + data[i].doctor_firstname + ' ' + data[i].doctor_lastname + '</option>';
                }
                $('.list_doctor').replaceWith('<div class="form-group list_doctor"><label for="inputState">Terapis</label><select class="form-control" id="doctor" name="doctor_id">' + html + '</select></div>');
            }
        });
        return false;
    });

    function saveDoctor() {
        $.ajax({
            url: "<?php echo admin_url(); ?>appointments/save_doctor",
            method: "POST",
            data: $('#doctorForm').serialize(),
            async: true,
            beforeSend: function() {
                swal({text:'Loading..',button:false,timer:1000});
            },
            success: function(data) {
                if(data.status) {
                    swal({icon:'success',title:'Berhasil disimpan!',text:data.message,timer:3000}).then(()=>{
                        $('.table-all-appointment').DataTable().ajax.reload();
                        $('#doctorModal').modal('hide');
                    });
                } else {
                    swal({icon:'error',title:'Gagal disimpan!',text:data.message,timer:3000}).then(()=>{
                        $('#doctorModal').modal('hide');
                    });
                }
            }
        });
    }
</script>
</body>

</html>