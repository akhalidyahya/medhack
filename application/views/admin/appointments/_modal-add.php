<!-- Modal -->
<div class="modal fade" id="appointmentModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Formulir Membuat Janji Terapi</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form id="addForm">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <div class="col-md-12">
                            <div class="form-group select-placeholder">
                                <label for="f_patient" class="control-label">Pasien</label>
                                <select name="f_patient" class="selectpicker no-margin ajax-search" data-width="100%" id="f_patient" data-none-selected-text="Klik untuk mencari Pasien" data-live-search="true">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <?php echo render_datetime_input('form_waktu', 'Tanggal & Waktu'); ?>
                        <div class="form-group">
                            <label for="inputState">Cabang</label>
                            <select class="form-control" id="form_branch_id" name="form_branch_id">
                                <option selected>Pilih Cabang...</option>
                                <?php foreach ($branch as $key => $value) { ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->branch; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group list_doctor">

                        </div>
                        <div class="form-group">
                            <label>Pesan</label>
                            <input type="textarea" class="form-control" id="message" name="message">
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="submit" type="button" class="btn btn-primary" onclick="saveAppointment()">Kirim</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </form>
            </div>
        </div>
    </div>
</div>