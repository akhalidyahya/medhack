<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <a href="javascript:;" onclick="addData()" class="btn btn-info pull-right display-block"><?php echo _l('new_branch'); ?></a>
                        <div class="clearfix"></div>
                        <hr class="hr-panel-heading" />
                        <div class="row" id="contract_summary">
                            <div class="col-md-12">
                                <h4 class="no-margin text-success"><?php echo _l('branch_summary_heading'); ?></h4>
                            </div>
                            <div class="col-md-2 col-xs-6 border-right">
                                <h3 class="bold" id="branch_count"><?php echo $branch_count; ?></h3>
                                <span class="text-info"><?php echo _l('branch_summary'); ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="panel_s">
                    <?php echo form_hidden('custom_view'); ?>
                    <div class="panel-body">
                        <?php $this->load->view('admin/branchs/table_html'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/branchs/_form_modal'); ?>
<?php init_tail(); ?>
<script>
    // $('#myTable').DataTable();
    $(function() {
        
        initDataTable('.table-branchs', admin_url + 'branchs/table', undefined, undefined, {}, <?php echo hooks()->apply_filters('branchs_table_default_order', json_encode(array(0, 'asc'))); ?>);

    });
</script>
<script>
    function addData(){
        $('.modal-title').text('Tambah Cabang Baru');
        $('#id').val(0);
        $('#form_modal form')[0].reset();
        $('[name=method]').val('add');
        $('label.error').remove();
        $('#form_modal').modal('show');
    }

    function editData(id) 
    {
        $('label.error').remove();
        var url = "<?= admin_url()?>"+"branchs/getBranchById/"+id;
        $('[name=method]').val('edit');
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            success: function(data){
                $('#form_modal').modal('show');
                $('.modal-title').text('Edit Cabang');
                $('[name=id]').val(data.id);
                $('[name=branch]').val(data.branch);
                $('[name=code]').val(data.code);
                $('[name=address]').val(data.address);
                $('[name=city]').val(data.city);
            }
        })
    }

    function saveData()
    {
        var url = "<?= admin_url()?>"+"branchs/saveBranch";

        $.ajax({
            url: url,
            type: 'POST',
            data: $('#branch_form').serialize(),
            success: function(data){
                // data = JSON.parse(data);
                if(data.success){
                    swal({
                        title: 'Berhasil Simpan Data',
                        text: data.message,
                        icon: 'success',
                        timer: '3000'
                    }).then(()=>{
                        $('#form_modal').modal('hide');
                        $('.table-branchs').DataTable().ajax.reload();
                        if($('[name=method]').val() == 'add') {
                            let current_count = Number($('#branch_count').text());
                            current_count++;
                            $('#branch_count').text(current_count);
                        }
                    });
                } else {
                    swal({
                        title: 'Gagal Simpan Data',
                        text: data.message,
                        icon: 'error',
                        timer: '3000'
                    }).then(()=>{
                        $('#form_modal').modal('hide');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                swal({
                    title: 'System Error',
                    text: errorThrown,
                    icon: 'error',
                    timer: '4000'
                }).then(()=>{
                    $('#form_modal').modal('hide');
                    $('.table-branchs').DataTable().ajax.reload();
                });
            }
        });
    }

    function deleteData(id) 
    {
        swal({
            title: "Yakin ingin hapus cabang?",
            text: "Data akan dihapus secara permanen",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: 'Hapus!',
                    closeModal: false,
                },
            },
        }).then(process=>{
            if(process) {
                var url = "<?= admin_url()?>"+"branchs/deleteBranch/"+id
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data){
                        if(data.success) {
                            swal({
                                title: "Berhasil Delete Cabang",
                                text: `${data.message}`,
                                icon: "success",
                                timer: '3000'
                            });
                            $('.table-branchs').DataTable().ajax.reload();
                            let current_count = Number($('#branch_count').text());
                            current_count--;
                            $('#branch_count').text(current_count);
                        }
                    }
                })
            }
        })
    }
</script>
</body>

</html>