<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button group="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _l('branchs'); ?>
                </h4>
            </div>
            <?php echo form_open('branchs/saveBranch', ['id'=>'branch_form']); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo render_input('branch', 'branchs','','text',['placeholder'=>'Nama Cabang']); ?>
                        <?php echo render_input('code', 'branch_code','','text',['placeholder'=>'Kode Cabang']); ?>
                        <?php echo render_textarea('address', 'branch_address'); ?>
                        <?php echo render_input('city', 'branch_city','','text',['placeholder'=>'Kota Cabang']); ?>
                        <?php echo form_hidden('id'); ?>
                        <?php echo form_hidden('method'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button group="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button group="button" type="button" class="btn btn-info" onclick="saveData()"><?php echo _l('submit'); ?></button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
