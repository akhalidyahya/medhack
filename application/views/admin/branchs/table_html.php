<?php defined('BASEPATH') or exit('No direct script access allowed');

$table_data = array(
  'id',
  'branch',
  'code',
  'address',
  'city'
);
$custom_fields = get_custom_fields('branchs',array('show_on_table'=>1));

foreach($custom_fields as $field){
 	array_push($table_data,$field['name']);
}

$table_data = hooks()->apply_filters('branchs_table_columns', $table_data);

render_datatable($table_data, (isset($class) ? $class : 'branchs'),[],[
  'data-last-order-identifier' => 'branchs',
  'data-default-order'         => get_table_last_order('branchs'),
]);

?>
