<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    db_prefix() . 'bed_allotment.allotment_timestamp as allotment_timestamp',
    db_prefix().' bed.bed_number as bed_number',
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name',
    db_prefix() . 'bed_allotment.discharge_timestamp as discharge_timestamp',
];

$sIndexColumn = 'bed_allotment_id';
$sTable       = db_prefix() . 'bed_allotment';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'bed ON ' . db_prefix() . 'bed.bed_id=' . db_prefix() . 'bed_allotment.bed_id',
    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id=' . db_prefix() . 'bed_allotment.patient_id',
];

$join = hooks()->apply_filters('bed_allotment_table_sql_join', $join);

$where     = [];

$from_date = '';
$to_date   = '';
$s_patient = '';
$s_room    = '';

$additional_get = [
    db_prefix() . 'bed_allotment.bed_allotment_id',
];

if ($this->ci->input->post('from_date')) {
    $from_date = $this->ci->input->post('from_date');
    $from_date = to_sql_date($from_date);
}
if ($this->ci->input->post('to_date')) {
    $to_date = $this->ci->input->post('to_date');
    $to_date = to_sql_date($to_date);
}
if ($this->ci->input->post('s_patient')) {
    $s_patient = $this->ci->input->post('s_patient');
}
if ($this->ci->input->post('s_room')) {
    $s_room = $this->ci->input->post('s_room');
}

if ($s_patient != '') {
    array_push($where, 'AND '.db_prefix().'bed_allotment.patient_id = ' . $s_patient . '');
}

if ($s_room != '') {
    array_push($where, 'AND '.db_prefix().'bed_allotment.bed_id = ' . $s_room . '');
}

if ($from_date != '' && $to_date != '') {
    array_push($where, 'AND (DATE(allotment_timestamp) >= "' . $from_date . '" and DATE(discharge_timestamp) <= "' . $to_date . '")');
} elseif ($from_date != '') {
    array_push($where, 'AND (DATE(allotment_timestamp) >= "' . $from_date . '")');
} elseif ($to_date != '') {
    array_push($where, 'AND (DATE(discharge_timestamp) <= "' . $to_date . '")');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additional_get);

$output  = $result['output'];
$rResult = $result['rResult'];
// echo json_encode($rResult);exit;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = Date('d F Y H:i', strtotime($aRow['allotment_timestamp']));
    $row[] = $aRow['bed_number'];
    $row[] = $aRow['full_name'];

    $statusLabel = setAllotmentStatus($aRow['allotment_timestamp'],$aRow['discharge_timestamp'],true);
    
    $status = $statusLabel['status'];
    $label  = $statusLabel['label'];

    $row[] = "<span class='label $label s-status'><strong>" . CommonConstant::ALLOTMENT_STATUS_LIST[$status] . "</strong></span>";

    $row[] = Date('d F Y H:i', strtotime($aRow['discharge_timestamp']));

    $button = '-';
    if (has_permission('room_management', '', 'edit')) {
        $button = '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="editData(' . $aRow['bed_allotment_id'] . ')"><i class="fa fa-edit"></i></a>';
        $button .= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="deleteData(' . $aRow['bed_allotment_id'] . ')"><i class="fa fa-trash"></i></a>';
    }

    $row[] = $button;

    $output['aaData'][] = $row;
}
