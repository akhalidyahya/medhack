<?php

use Carbon\Carbon;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'whatsapp_dialogs.id as id',
    'whatsapp_dialogs.dialog_id as dialog_id',
    'whatsapp_dialogs.last_time as last_time',
    'whatsapp_dialogs.name as name',
    'CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as staff_followup',
    'CONCAT(' . db_prefix() . 'assignment.firstname, \' \', ' . db_prefix() . 'assignment.lastname) as staff_assignment',
    'branchs.branch as branch',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'whatsapp_dialogs';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'staff ON ' . db_prefix() . 'staff.staffid=' . db_prefix() . 'whatsapp_dialogs.followup_id',
    'LEFT JOIN ' . db_prefix() . 'whatsapp_dialog_staff ON ' . db_prefix() . 'whatsapp_dialog_staff.dialog_id=' . db_prefix() . 'whatsapp_dialogs.dialog_id',
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'whatsapp_dialogs.branch_id',
    'LEFT JOIN ' . db_prefix() . 'staff assignment ON ' . db_prefix() . 'assignment.staffid=' . db_prefix() . 'whatsapp_dialogs.assignment_to',
];
$where        = [];

$source = '';
if ($this->ci->input->post('source')) {
    $source = $this->ci->input->post('source');
}
if($source != '') {
    array_push($where, 'AND source = ' . $source);
}

$cs_page = '';
if ($this->ci->input->post('cs_page')) {
    $cs_page = $this->ci->input->post('cs_page');
}
if($cs_page != '') {
    array_push($where, 'AND whatsapp_dialog_staff.staff_id = ' . get_staff_user_id());
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row    = [];
    $row[]  = '<input type="checkbox" name="dialogs" class="form-control" value="'.$aRow['dialog_id'].'">';
    $row[]  = $aRow['id'];
    // $row[]  = _d(date('Y-m-d H:i:s',$aRow['last_time']));//Carbon::parse($aRow['last_time'])->format('Y-m-d H:i:s');
    $row[]  = '<span class="text-primary" style="font-weight:600;">'.str_replace('@c.us','',$aRow['dialog_id']).'</span><br/><small>Last chat: '._d(date('Y-m-d H:i:s',$aRow['last_time'])).'</small>';
    $row[]  = !empty($aRow['name']) ? $aRow['name']: $aRow['dialog_id'];
    $row[]  = (stripos($aRow['dialog_id'],'g.us') === false) ? 'Personal' : 'Group';
    $row[]  = $aRow['staff_followup'];
    $row[]  = $aRow['branch'];
    $row[]  = $aRow['staff_assignment'];
    $row[]  = '<a class="btn btn-default btn-icon" href="javascript:;" onclick="viewData(\'' . $aRow['dialog_id'] . '\')"><i class="fa fa-eye"></i> </a>
                <a class="btn btn-info btn-icon" href="javascript:;" onclick="editData(\'' . $aRow['dialog_id'] . '\')"><i class="fa fa-edit"></i> </a>
                ';

    $output['aaData'][] = $row;
}
