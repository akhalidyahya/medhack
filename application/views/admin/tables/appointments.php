<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'timestamp',
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name',
    'CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as doctor',
    'message',
    db_prefix().'branchs.branch as cabang',
    'status',
    'invoice_id',
    'patient_id'
];

$sIndexColumn = 'appointment_id';
$sTable       = db_prefix() . 'appointment';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id=' . db_prefix() . 'appointment.patient_id',
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'appointment.branch_id',
    'LEFT JOIN ' . db_prefix() . 'staff ON ' . db_prefix() . 'staff.staffid=' . db_prefix() . 'appointment.doctor_id',
];


$where      = [];

if (is_doctor() && !is_admin()) {
    array_push($where, 'AND doctor_id =' . get_staff_user_id());
}

if(has_permission('appointment', '', 'view_branch') && !is_admin()) {
    array_push($where, 'AND appointment.branch_id IN ('.getStaffBrachIdString(get_staff_user_id()).')' );
}

$from_date = '';
$to_date   = '';
$s_patient = '';

// $CI = &get_instance();
// $this->ci->load->model('accounting_model');

if ($this->ci->input->post('from_date')) {
    $from_date = $this->ci->input->post('from_date');
    $from_date = to_sql_date($from_date);
}

if ($this->ci->input->post('to_date')) {
    $to_date = $this->ci->input->post('to_date');
    $to_date = to_sql_date($to_date);
}

if ($this->ci->input->post('s_patient')) {
    $s_patient = $this->ci->input->post('s_patient');
}
if ($from_date != '' && $to_date != '') {
    array_push($where, 'AND (DATE(timestamp) >= "' . $from_date . '" and DATE(timestamp) <= "' . $to_date . '")');
} elseif ($from_date != '') {
    array_push($where, 'AND (DATE(timestamp) >= "' . $from_date . '")');
} elseif ($to_date != '') {
    array_push($where, 'AND (DATE(timestamp) <= "' . $to_date . '")');
}

if($s_patient != '') {
    array_push($where, 'AND patient_id = ' . $s_patient);
}

$join = hooks()->apply_filters('appointment_table_sql_join', $join);

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() . 'appointment.appointment_id', db_prefix() . 'appointment.branch_id', db_prefix() . 'appointment.doctor_id', db_prefix() . 'appointment.invoice_id']);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    $row[] = Date('d F Y', strtotime($aRow['timestamp']));
    $row[] = Date('H:i:s', strtotime($aRow['timestamp']));
    $row[] = $aRow['full_name'];
    $row[] = $aRow['message'];
    $doctorBtn = $aRow['doctor'];
    if(empty($aRow['doctor'])) {
        $doctorBtn = "<button onclick='chooseDoctor(".$aRow['appointment_id'].",".$aRow['patient_id'].")' class='btn btn-xs btn-primary'>Pilih Terapis</button>";
    }
    $row[] = $doctorBtn;
    $row[] = $aRow['cabang'];
    $label = 'default';
    if ($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_APPROVED) {
        $label = 'success';
    } else if ($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_REJECT) {
        $label = 'danger';
    } else if ($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_PENDING) {
        $label = 'warning';
    }
    $row[] = "<span class='label label-" . $label . " s-status'><strong>" . CommonConstant::APPOINTMENT_STATUS_LIST[$aRow['status']] . "</strong></span>";    

    $button = '-';

    // if (has_permission('appointment', '', 'approval')) {
        if ($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_PENDING) {
            $button = '<a class="btn btn-success btn-icon" href="javascript:;" onclick="changeStatus(' . $aRow['appointment_id'] . ',\''. CommonConstant::APPOINTMENT_STATUS_APPROVED .'\')"><i class="fa fa-check"></i></a>';
            $button .= "<input type='text' hidden value=".$aRow['branch_id'].">".'<a class="btn btn-danger btn-icon" href="javascript:;" onclick="changeStatus(' . $aRow['appointment_id'] . ',\''. CommonConstant::APPOINTMENT_STATUS_REJECT .'\')"><i class="fa fa-times"></i></a>';
        } else if ($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_APPROVED && $aRow['timestamp'] >= Date('Y-m-d H:i')) {
            $button = '<a class="btn btn-default btn-xs" style="font-size:10px" href="javascript:;" onclick="changeStatus(' . $aRow['appointment_id'] . ',\'' . CommonConstant::APPOINTMENT_STATUS_CANCEL . '\')">Batalkan</a>';
        } elseif($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_APPROVED){
            $button = '<a class="btn btn-warning btn-xs" style="font-size:10px" href="javascript:;" onclick="createHistory(' . $aRow['appointment_id'] . ',\''.$aRow['full_name'].'\')">Input Treatment</a>';
            $invText = 'Selesai';
            if (has_permission('appointment', '', 'approval')) {
               $invText = 'Buat Invoice';
            }
            $button .= "<input type='text' hidden id=patient_id".$aRow['appointment_id']." value=".$aRow['patient_id'].">"."<input type='text' hidden id=invoice_id".$aRow['appointment_id']." value=".$aRow['invoice_id'].">"."<input type='text' hidden id=branch".$aRow['appointment_id']." value=".$aRow['branch_id'].">".'<a class="btn btn-primary btn-xs" style="font-size:10px" href="javascript:;" onclick="changeStatus(' . $aRow['appointment_id'] . ',\'' . CommonConstant::APPOINTMENT_STATUS_DONE . '\')">'.$invText.'</a>';
        } elseif($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_DONE){
            $button = '<a class="btn btn-default btn-xs" style="font-size:10px" href="javascript:;" onclick="createHistory(' . $aRow['appointment_id'] . ',\''.$aRow['full_name'].'\',1)">Lihat Treatment</a>';
            if (has_permission('appointment', '', 'approval')) $button .= '<a class="btn btn-default btn-xs" style="font-size:10px" href="'.base_url().'admin/invoices/invoice/'.$aRow['invoice_id'].'" >Lihat Invoice</a>';
        }
    // }

    $row[] = $button;

    $output['aaData'][] = $row;
}
