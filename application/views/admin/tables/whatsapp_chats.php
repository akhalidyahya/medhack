<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'time',
    'body',
    'senderName',
    'type',
    'chatName',
    'fromMe',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'whatsapp_chats';
$join         = [];
$where        = [];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix().'whatsapp_chats.id']);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $row[]  = date('Y-m-d H:i',$aRow['time']);
    $row[]  = !empty($aRow['chatName']) ? $aRow['chatName'] : $aRow['senderName'];
    $body   = substr($aRow['body'],0,40);
    if(strlen($aRow['body']) > 40) {
        $body.=' ...';
    }
    $row[]  = $body;
    $row[]  = $aRow['senderName'];
    $row[]  = $aRow['type'];
    $row[]  = $aRow['fromMe'];
    $row[]  = '<a class="btn btn-default btn-icon" href="javascript:;" onclick="viewData(' . $aRow['id'] . ')"><i class="fa fa-eye"></i> </a>';

    $output['aaData'][] = $row;
}
