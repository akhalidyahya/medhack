<?php

use app\constants\CommonConstant;
use Carbon\Carbon;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    // 'dialog_id',
    // 'name',
    // 'followup_time',
    // 'followup_note',
    // 'label',
    // 'followup_note',
    db_prefix() . 'fu_summary_status.status as status',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'fu_summary_status';
$join         = [
    // 'LEFT JOIN ' . db_prefix() . 'fu_summary_status ON ' . db_prefix() . 'fu_summary_status.id=' . db_prefix() . 'whatsapp_dialogs.fu_status_id',
];
$where        = [];

// array_push($where, " AND label = '".CommonConstant::WA_DIALOG_STATUS_FU."' ");

if(!is_admin()) {
    array_push($where, " AND followup_id = ".get_staff_user_id()." ");
}

$from_date  = '';
$to_date    = '';
if ($this->ci->input->post('from_date')) {
    $from_date = $this->ci->input->post('from_date');
    $from_date = to_sql_date($from_date);
}
if ($this->ci->input->post('to_date')) {
    $to_date = $this->ci->input->post('to_date');
    $to_date = to_sql_date($to_date);
}
if ($from_date != '' && $to_date != '') {
    // array_push($where, 'AND followup_time !="" ');
    array_push($where, ' AND (DATE(followup_time) >= "' . $from_date . '" and DATE(followup_time) <= "' . $to_date . '") ');
} elseif ($from_date != '') {
    // array_push($where, 'AND followup_time !="" ');
    array_push($where, ' AND (DATE(followup_time) >= "' . $from_date . '") ');
} elseif ($to_date != '') {
    // array_push($where, 'AND followup_time !="" ');
    array_push($where, ' AND (DATE(followup_time) <= "' . $to_date . '") ');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row    = [];

    $row[]  = 'a';
    $row[]  = 'b';
    $row[]  = 'c';
    $row[]  = 'd';
    $row[]  = 'e';
    // $row[]  =   $aRow['dialog_id'];
    // $row[]  =   $aRow['name'];
    // $row[]  =   !empty($aRow['followup_time']) ? Carbon::parse($aRow['followup_time'])->format('d F Y H:i') : '-';
    // $row[]  =   CommonConstant::WA_DIALOG_STATUS_LISTS[$aRow['label']];
    // $row[]  =   $aRow['status'];
    // $row[]  =   cutString($aRow['followup_note'],20);
    // $row[]  =   '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="editData(\'' . $aRow['dialog_id'] . '\')"><i class="fa fa-edit"></i> </a>';

    $output['aaData'][] = $row;
}
