<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'date',    
    'total',
    'status',
    'image_bukti',
    db_prefix() . 'branchs.branch as branch',
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'invoices';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id=' . db_prefix() . 'invoices.patient_id',
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'invoices.branch_id',
];
$where        = [];

if(has_permission('orders', '', 'view_branch') && !is_admin()) {
    array_push($where, 'AND invoices.branch_id IN ('.getStaffBrachIdString(get_staff_user_id()).')' );
}

$from_date  = '';
$to_date    = '';
$status     = '';
$patient_id = '';

if ($this->ci->input->post('from_date')) {
    $from_date = $this->ci->input->post('from_date');
    $from_date = to_sql_date($from_date);
}

if ($this->ci->input->post('to_date')) {
    $to_date = $this->ci->input->post('to_date');
    $to_date = to_sql_date($to_date);
}

if ($this->ci->input->post('status')) {
    $status = $this->ci->input->post('status');
}

if ($this->ci->input->post('s_patient')) {
    $patient_id = $this->ci->input->post('s_patient');
}

if ($from_date != '' && $to_date != '') {
    array_push($where, 'AND (DATE(date) >= "' . $from_date . '" and DATE(date) <= "' . $to_date . '")');
} elseif ($from_date != '') {
    array_push($where, 'AND (DATE(date) >= "' . $from_date . '")');
} elseif ($to_date != '') {
    array_push($where, 'AND (DATE(date) <= "' . $to_date . '")');
}

if($status != '') {
    array_push($where, 'AND status = "' . $status . '"');
}

if($patient_id != '') {
    array_push($where, 'AND invoices.patient_id = "' . $patient_id . '"');
}

$additional_columns = [
    db_prefix() . 'invoices.id',
    db_prefix() . 'invoices.hash',
    db_prefix() . 'invoices.patient_id as patient_id',
];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additional_columns);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    $numberOutput = '<strong class="text-primary " target="_blank" href="'.base_url().'invoice/'.$aRow['id'].'/'.$aRow['hash'].'">'.format_invoice_number($aRow['id']).'</strong>';
    $numberOutput .= '<div class="row-options">';
    $numberOutput .= '<a href="' . site_url('invoice/' . $aRow['id'] . '/' . $aRow['hash']) . '" target="_blank">' . _l('view') . '</a>';
    $numberOutput .= ' | <a href="' . admin_url('invoices/invoice/' . $aRow['id']) . '">' . _l('edit') . '</a>';
    $numberOutput .= '</div>';

    $row[] = $aRow['date'];
    $row[] = $numberOutput;
    $row[] = $aRow['branch'];
    $row[] = $aRow['total'];
    $row[] = $aRow['full_name'];

    $img = '-';
    if(!empty($aRow['image_bukti'])) {
        $img = '<img onclick="detailInvoice('.$aRow['id'].')" height="50px" src="'.site_url().'uploads/bukti/'.$aRow['image_bukti'].'" style="cursor:pointer;" />';
    }
    $row[] = $img;

    $row[] = format_invoice_status($aRow['status']);
    
    $button = '-';
    if($aRow['status'] != CommonConstant::INVOICE_STATUS_PAID) {
        $button = '<a class="btn btn-success btn-icon" href="javascript:;" onclick="changeStatus(' . $aRow['id'] . ',' . CommonConstant::INVOICE_STATUS_PAID . ')"><i class="fa fa-check"></i></a>';
        $button .= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="changeStatus(' . $aRow['id'] . ',' . CommonConstant::INVOICE_STATUS_REJECT . ')"><i class="fa fa-times"></i></a>';
    }

    $row[] = $button;

    $output['aaData'][] = $row;
}
