<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'order_date',
    db_prefix() . 'branchs.branch as branch',   
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name',
    db_prefix() .'order_master.shipping_status as status_pengiriman',
    db_prefix() .'invoices.total as total',
    db_prefix() . 'invoices.hash as hash',  
    db_prefix() .'order_master.shipping_receipt_number as resi',
    db_prefix() .'order_master.id as id',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'order_master';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id=' . db_prefix() . 'order_master.patient_id',
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'order_master.branch_id',
    'LEFT JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id=' . db_prefix() . 'order_master.invoice_id',
];
$where        = [];

$from_date  = '';
$to_date    = '';
$status     = '';

if ($this->ci->input->post('from_date')) {
    $from_date = $this->ci->input->post('from_date');
    $from_date = to_sql_date($from_date);
}

if ($this->ci->input->post('to_date')) {
    $to_date = $this->ci->input->post('to_date');
    $to_date = to_sql_date($to_date);
}

if ($this->ci->input->post('status')) {
    $status = $this->ci->input->post('status');
}

if ($from_date != '' && $to_date != '') {
    array_push($where, 'AND (DATE(date) >= "' . $from_date . '" and DATE(date) <= "' . $to_date . '")');
} elseif ($from_date != '') {
    array_push($where, 'AND (DATE(date) >= "' . $from_date . '")');
} elseif ($to_date != '') {
    array_push($where, 'AND (DATE(date) <= "' . $to_date . '")');
}

if($status != '') {
    array_push($where, 'AND order_master.shipping_status = "' . $status . '"');
}

$additional_columns = [
    db_prefix() . 'order_master.invoice_id',
];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additional_columns);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    $numberOutput = '<strong class="text-primary " target="_blank" href="'.base_url().'invoice/'.$aRow['order_date'].'/'.$aRow['hash'].'">'.format_invoice_number($aRow['invoice_id']).'</strong>';
    $numberOutput .= '<div class="row-options">';
    $numberOutput .= '<a href="' . site_url('invoice/' . $aRow['invoice_id'] . '/'. $aRow['hash']) . '" target="_blank">' . _l('view') . '</a>';
    $numberOutput .= ' | <a href="' . admin_url('invoices/invoice/' . $aRow['invoice_id']) . '">' . _l('edit') . '</a>';
    $numberOutput .= '</div>';

    $row[] = $aRow['order_date'];
    $row[] = $numberOutput;
    $row[] = $aRow['branch'];
    $row[] = $aRow['full_name'];
    if($aRow['status_pengiriman'] != CommonConstant::SHIPPING_STATUS_SENT) {
        $shipping_status = '<span class="label label-danger">Belum Dikirim</span>';
    } else{
        $shipping_status = '<span class="label label-success">Sudah Dikirim</span>';
    }
    $row[] = $shipping_status;
    if ($aRow['resi'] == null) {
        $resi = "Belum ada";
    } else{
        $resi = $aRow['resi'];
    }
    $row[] = $resi;

    
    $button = '-';
    

    if($aRow['status_pengiriman'] != CommonConstant::SHIPPING_STATUS_SENT) {
        $button = '<a class="btn btn-success btn-icon" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal'.$aRow['id'].'"">+ No Resi</a>';
    }
    $row[] = $button;

    $row[] = '';
    $output['aaData'][] = $row;
}
