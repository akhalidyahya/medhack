<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'name'
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'bed_types';
$join         = [];

$where        = [];

$from_date = '';
$to_date   = '';

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() . 'bed_types.id']);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $aRow['name'];

    $button = '-';
    if (has_permission('room_management', '', 'edit')) {
        $button = '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="editBedType(' . $aRow['id'] . ')"><i class="fa fa-edit"></i></a>';
        $button .= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="deleteBedType(' . $aRow['id'] . ')"><i class="fa fa-trash"></i></a>';
    }

    $row[] = $button;

    $output['aaData'][] = $row;
}
