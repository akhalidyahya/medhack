<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'id',
    'status',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'fu_summary_status';
$join         = [];
$where        = [];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix().'fu_summary_status.id']);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $row[]  =   $aRow['id'];
    $row[]  =   $aRow['status'];

    $button = '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="editData(' . $aRow['id'] . ')"><i class="fa fa-edit"></i></a>';
    $button .= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="deleteData(' . $aRow['id'] . ')"><i class="fa fa-trash"></i></a>';
    $row[]  = $button;

    $output['aaData'][] = $row;
}
