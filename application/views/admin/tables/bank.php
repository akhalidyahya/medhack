<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'bank_name',    
    'acc_number',
    'person_name',
    db_prefix().'branchs.branch as cabang',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'bank';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'bank.branch_id',
];
$where        = [];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() . 'bank.id']);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $row[] = $aRow['bank_name'];
    $row[] = $aRow['acc_number'];
    $row[] = $aRow['person_name'];
    $row[] = $aRow['cabang'];

    $button = '-';
    $button = '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="editData(' . $aRow['id'] . ')"><i class="fa fa-edit"></i></a>';
    $button .= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="deleteData(' . $aRow['id'] . ')"><i class="fa fa-trash"></i></a>';

    $row[] = $button;

    $output['aaData'][] = $row;
}
