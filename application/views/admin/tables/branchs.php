<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'id',
    'branch',
    'code',
    'address',
    'city',
];
$sIndexColumn = 'id';
$sTable       = db_prefix().'branchs';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable);
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        $_data = $aRow[$aColumns[$i]];

        if($aColumns[$i] == 'branch') {
            $_data .= '<div class="row-options">';
            $_data .= '<a href="javascript:;" onclick="editData('.$aRow['id'].')">Edit</a>';
            $_data .= ' | ';
            $_data .= '<a href="#" class="text-danger" onclick="deleteData('.$aRow['id'].')">Delete</a>';
            $_data .= '</div>';
        }
        
        $row[] = $_data;
    }

    $output['aaData'][] = $row;
}
