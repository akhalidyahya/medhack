<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'prescriptions.timestamp as timestamp',
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name',
    '(SELECT itemable.description FROM itemable WHERE rel_id = invoices.id LIMIT 1) as item',
    db_prefix().'branchs.branch as cabang'
];

$sIndexColumn = 'prescription_id';
$sTable       = db_prefix() . 'prescriptions';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id=' . db_prefix() . 'prescriptions.patient_id',
    'LEFT JOIN ' . db_prefix() . 'appointment ON ' . db_prefix() . 'appointment.history_id=' . db_prefix() . 'prescriptions.prescription_id',
    'LEFT JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id=' . db_prefix() . 'appointment.invoice_id',
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'appointment.branch_id',
];

$where      = [];

if (is_doctor() && !is_admin()) {
    array_push($where, 'AND prescriptions.doctor_id =' . get_staff_user_id());
}

if(has_permission('prescriptions', '', 'view_branch') && !is_admin()) {
    array_push($where, 'AND contacts.branch_id IN ('.getStaffBrachIdString(get_staff_user_id()).')' );
    array_push($where, 'AND prescriptions.branch_id IN ('.getStaffBrachIdString(get_staff_user_id()).')' );
}

$from_date = '';
$to_date   = '';
$s_patient = '';

// $CI = &get_instance();
// $this->ci->load->model('accounting_model');

if ($this->ci->input->post('from_date')) {
    $from_date = $this->ci->input->post('from_date');
    $from_date = to_sql_date($from_date);
}

if ($this->ci->input->post('to_date')) {
    $to_date = $this->ci->input->post('to_date');
    $to_date = to_sql_date($to_date);
}

if ($this->ci->input->post('s_patient')) {
    $s_patient = $this->ci->input->post('s_patient');
}
if ($from_date != '' && $to_date != '') {
    array_push($where, 'AND (DATE(prescriptions.timestamp) >= "' . $from_date . '" and DATE(prescriptions.timestamp) <= "' . $to_date . '")');
} elseif ($from_date != '') {
    array_push($where, 'AND (DATE(prescriptions.timestamp) >= "' . $from_date . '")');
} elseif ($to_date != '') {
    array_push($where, 'AND (DATE(prescriptions.timestamp) <= "' . $to_date . '")');
}

if ($s_patient != '') {
    array_push($where, 'AND prescriptions.patient_id = ' . $s_patient . '');
}

$join = hooks()->apply_filters('prescriptions_table_sql_join', $join);

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() . 'prescriptions.prescription_id',db_prefix() . 'prescriptions.patient_id', db_prefix() . 'appointment.invoice_id']);

$output  = $result['output'];
$rResult = $result['rResult'];
// echo json_encode($rResult);exit;
foreach ($rResult as $aRow) {
    $row    = [];
    $row[]  = Date('d F Y', strtotime($aRow['timestamp']));
    $row[]  = Date('H:i:s', strtotime($aRow['timestamp']));
    $row[]  = $aRow['full_name'];
    $row[]  = '<a class="text-primary" href="'.admin_url('invoices/invoice/'.$aRow['invoice_id']).'">'.$aRow['item'].'</a>';
    $row[]  = $aRow['cabang'];

    $button  = '<a class="btn btn-default btn-icon" href="javascript:;" onclick="viewData(' . $aRow['prescription_id'] . ',\'view\')"><i class="fa fa-eye"></i> </a>';
    if (has_permission('prescriptions', '', 'edit')) {
        $button  .= '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="viewData(' . $aRow['prescription_id'] . ',\'edit\')"><i class="fa fa-edit"></i> </a>';
        $button  .= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="deleteData(' . $aRow['prescription_id'] . ')"><i class="fa fa-trash"></i> </a>';
    }

    $row[] = $button;

    $output['aaData'][] = $row;
}
