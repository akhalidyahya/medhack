<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'time',
    'body',
    'senderName',
    'type',
    'chatName',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'whatsapp_chats';
$join         = [];
$where        = [
    'AND type = "call_log"',
];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $row[]  = date('Y-m-d H:i',$aRow['time']);
    $row[]  = !empty($aRow['chatName']) ? $aRow['chatName'] : $aRow['senderName'];
    $row[]  = $aRow['body'];
    $row[]  = $aRow['senderName'];
    $row[]  = $aRow['type'];

    $output['aaData'][] = $row;
}
