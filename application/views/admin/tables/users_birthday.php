<?php

use Carbon\Carbon;

defined('BASEPATH') or exit('No direct script access allowed');

// $aColumns        = [ 'CONCAT(firstname, \' \', lastname) as full_name'];
$aColumns = [
    'tanggal_lahir',
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name',
    'email',    
    'phonenumber',
    'active',
    'branchs.branch as branch_name',
    // 'contacts.branch_id as branch_id',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'contacts';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'contacts.branch_id',
];
$where        = [];

if(has_permission('users', '', 'view_branch') && !is_admin() ) {
    $ids    = getStaffBrachIdString(get_staff_user_id());
    array_push($where, 'AND branch_id IN ('.$ids.')');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() . 'contacts.id as id', 'userid', 'is_primary']);

$output  = $result['output'];
$rResult = $result['rResult'];
// echo json_encode($rResult);exit;
foreach ($rResult as $aRow) {
    $row = [];

    $row[]  = !empty($aRow['tanggal_lahir']) ? Carbon::parse($aRow['tanggal_lahir'])->format('d F Y') : '-';
    $rowFullName = $aRow['full_name'];
    
    if (has_permission('users', '', 'edit')) {
        $rowFullName .= '<div class="row-options">';
        $rowFullName .= '<a href="javascript:;" onclick="editData('.$aRow['id'].')">Edit</a>';
        $rowFullName .= ' | ';
        $rowFullName .= '<a href="#" class="text-danger" onclick="deleteData('.$aRow['id'].')">Delete</a>';
        $rowFullName .= '</div>';
    }

    $row[] = $rowFullName;

    $row[] = '<a href="mailto:' . $aRow['email'] . '">' . $aRow['email'] . '</a>';

    $row[] = $aRow['phonenumber'];

    $row[] = $aRow['branch_name'];

    $row[] = '<a href="https://wa.me/62'.substr($aRow['phonenumber'],1).'" class="btn btn-success"><i class="fa fa-whatsapp"></i></a>';

    $output['aaData'][] = $row;
}
