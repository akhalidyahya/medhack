<?php

defined('BASEPATH') or exit('No direct script access allowed');

// $aColumns        = [ 'CONCAT(firstname, \' \', lastname) as full_name'];
$aColumns = [
    'datecreated',
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as full_name',
    'email',    
    'phonenumber',
    'active',
    'branchs.branch as branch_name',
    // 'contacts.branch_id as branch_id',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'contacts';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'branchs ON ' . db_prefix() . 'branchs.id=' . db_prefix() . 'contacts.branch_id',
];
$where        = [];

if(has_permission('users', '', 'view_branch') && !is_admin() ) {
    $ids    = getStaffBrachIdString(get_staff_user_id());
    array_push($where, 'AND branch_id IN ('.$ids.')');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() . 'contacts.id as id', 'userid', 'is_primary']);

$output  = $result['output'];
$rResult = $result['rResult'];
// echo json_encode($rResult);exit;
foreach ($rResult as $aRow) {
    $row = [];

    $row[]  = $aRow['datecreated'];
    $rowFullName = $aRow['full_name'];
    
    if (has_permission('users', '', 'edit')) {
        $rowFullName .= '<div class="row-options">';
        $rowFullName .= '<a href="javascript:;" onclick="editData('.$aRow['id'].')">Edit</a>';
        $rowFullName .= ' | ';
        $rowFullName .= '<a href="#" class="text-danger" onclick="deleteData('.$aRow['id'].')">Delete</a>';
        $rowFullName .= '</div>';
    }

    $row[] = $rowFullName;

    $row[] = '<a href="mailto:' . $aRow['email'] . '">' . $aRow['email'] . '</a>';

    $row[] = '<a href="tel:' . $aRow['phonenumber'] . '">' . $aRow['phonenumber'] . '</a>';

    $row[] = $aRow['branch_name'];

    $outputActive = '<div class="onoffswitch">
                <input type="checkbox"' . (total_rows(db_prefix() . 'clients', 'registration_confirmed=0 AND userid=' . $aRow['userid']) > 0 ? ' disabled' : '') . ' data-switch-url="' . admin_url() . 'clients/change_contact_status" name="onoffswitch" class="onoffswitch-checkbox" id="c_' . $aRow['id'] . '" data-id="' . $aRow['id'] . '"' . ($aRow['active'] == 1 ? ' checked': '') . '>
                <label class="onoffswitch-label" for="c_' . $aRow['id'] . '"></label>
            </div>';
    // For exporting
    $outputActive .= '<span class="hide">' . ($aRow['active'] == 1 ? _l('is_active_export') : _l('is_not_active_export')) . '</span>';
    $row[] = $outputActive;

    $output['aaData'][] = $row;
}
