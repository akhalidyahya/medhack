<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'CONCAT(' . db_prefix() . 'staff.firstname, \' \', ' . db_prefix() . 'staff.lastname) as staff_full_name',
    db_prefix().'whatsapp_dialogs.name as dialog',
    db_prefix().'whatsapp_dialogs.followup_time as time',
    'CONCAT(' . db_prefix() . 'assignment.firstname, \' \', ' . db_prefix() . 'assignment.lastname) as staff_assignment',
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'whatsapp_dialog_staff';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'staff ON ' . db_prefix() . 'staff.staffid=' . db_prefix() . 'whatsapp_dialog_staff.staff_id',
    'LEFT JOIN ' . db_prefix() . 'whatsapp_dialogs ON ' . db_prefix() . 'whatsapp_dialogs.dialog_id=' . db_prefix() . 'whatsapp_dialog_staff.dialog_id',
    'LEFT JOIN ' . db_prefix() . 'staff assignment ON ' . db_prefix() . 'assignment.staffid=' . db_prefix() . 'whatsapp_dialogs.assignment_to',
];

$s_staff = '';
if ($this->ci->input->post('s_staff')) {
    $s_staff = $this->ci->input->post('s_staff');
}

$where        = [];

if($s_staff != '') {
    array_push($where, 'AND '.db_prefix().'whatsapp_dialog_staff.staff_id'.' = ' . $s_staff);
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix().'whatsapp_dialog_staff.id',db_prefix().'whatsapp_dialog_staff.dialog_id',db_prefix().'whatsapp_dialog_staff.staff_id as staff_id']);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row    = [];
    $row[]  = '<input type="checkbox" name="dialogs" class="form-control" value="'.$aRow['dialog_id'].'">';
    $row[]  = $aRow['dialog_id'];
    $row[]  = $aRow['dialog'];
    $row[]  = $aRow['staff_full_name'];
    $row[]  = $aRow['time'];
    $row[]  = $aRow['staff_assignment'];
    $row[]  = '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="editData(' . $aRow['id'] . ')"><i class="fa fa-edit"></i> </a>';

    $output['aaData'][] = $row;
}
