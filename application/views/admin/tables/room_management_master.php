<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'bed_number',
    db_prefix() . 'bed_types.name as type',
    'description',
];

$sIndexColumn = 'bed_id';
$sTable       = db_prefix() . 'bed';
$join         = [
    'LEFT JOIN ' . db_prefix() . 'bed_types ON ' . db_prefix() . 'bed_types.id=' . db_prefix() . 'bed.type',
];

$join = hooks()->apply_filters('bed_table_sql_join', $join);

$where        = [];

$from_date = '';
$to_date   = '';

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix() . 'bed.bed_id',db_prefix() . 'bed.status']);

$output  = $result['output'];
$rResult = $result['rResult'];
// echo json_encode($rResult);exit;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $aRow['bed_number'];
    $row[] = $aRow['type'];
    // $row[] = CommonConstant::MASTER_ROOM_STATUS_LIST[$aRow['status']];
    $row[] = $aRow['description'];

    $button = '-';
    if (has_permission('room_management', '', 'edit')) {
        $button = '<a class="btn btn-primary btn-icon" href="javascript:;" onclick="editData(' . $aRow['bed_id'] . ')"><i class="fa fa-edit"></i></a>';
        $button .= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="deleteData(' . $aRow['bed_id'] . ')"><i class="fa fa-trash"></i></a>';
    }

    $row[] = $button;

    $output['aaData'][] = $row;
}
