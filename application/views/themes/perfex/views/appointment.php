<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datetimepicker/jquery.datetimepicker.min.css">
<div class="panel_s" id="asd">
    <div class="panel-body text-left">
        <h3><i class="fa fa-book" aria-hidden="true"></i> Janji Terapi</h3>
    </div>
    <div class="panel-body text-right">
        <button data-toggle="modal" data-target="#appointmentModal" type="button" class="btn btn-primary btn-sm"><i class="fa fa-envelope" aria-hidden="true"></i> Buat Janji Terapi</button>
    </div>
    <div class="panel-body">
        <div id="reload_tabel" class="table-responsive">
            <?php if ($appointment != null) { ?>
                <table class="table" id="table-appointment">
                    <thead>
                        <tr>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Waktu</th>
                            <th scope="col">Terapis</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($appointment as $key_a => $value_a) { ?>
                            <tr>
                                <td><?= date('d F Y', strtotime($value_a->date)); ?></td>
                                <td><?= date('H:i', strtotime($value_a->date)); ?></td>
                                <td><?= $value_a->doctor_firstname . ' ' . $value_a->doctor_lastname ?></td>
                                <td><?php if ($value_a->status == 'approved') { ?>
                                        Diterima
                                    <?php } elseif ($value_a->status == 'pending') { ?>
                                        Menunggu Konfirmasi
                                    <?php } elseif ($value_a->status == 'reject') { ?>
                                        Ditolak
                                    <?php } elseif ($value_a->status == 'cancel') { ?>
                                        Dibatalkan
                                    <?php } else { ?>
                                        Selesai
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <br><br>
        <div class="panel-body">
            <div id="calendar_a"></div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="appointmentModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Formulir Membuat Janji Terapi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <form>
                                <?php echo render_datetime_input('datetime', 'Tanggal'); ?>
                                <div class="form-group">
                                    <label for="inputState">Cabang</label>
                                    <select class="form-control" id="branch_id" name="branch_id">
                                        <option selected>Pilih Cabang...</option>
                                        <?php foreach ($branch as $key => $value) { ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['branch']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group list_doctor">

                                </div>
                                <div class="form-group">
                                    <label>Pesan</label>
                                    <input type="textarea" class="form-control" id="message">
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="submit" type="button" class="btn btn-primary" onclick="saveAppointment()">Kirim</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/plugins/datetimepicker/jquery.datetimepicker.full.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/moment.min.js"></script>
<script type="text/javascript">
    //TAMBAH APPOINTMENT
    function saveAppointment() {
        var datetime = $('[name="datetime"]').val();
        // var doctor = $('#doctor').val();
        var message = $('#message').val();
        var branch_id = $('#branch_id').val();
        if (datetime == "") {
            info();
        } else {
            $.ajax({
                url: "<?php echo site_url('appointment/add') ?>",
                type: "POST",
                data: {
                    'datetime': datetime,
                    // 'doctor': doctor,
                    'message': message,
                    'branch_id': branch_id
                },
                dataType: "JSON",
                success: function(data) {
                    success();
                    $('#appointmentModal').modal('hide');
                    location.reload(true);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    error();
                }
            });
        }
    }

    //DROPDOWN PILIH DOKTER
    $(document).ready(function() {
        //off
        // $('#branch_id').change(function() {
        //     var id = $(this).val();
        //     console.log(id);
        //     $.ajax({
        //         url: "<?php //echo base_url(); ?>appointment/list_doctor",
        //         method: "POST",
        //         data: {
        //             branch_id: id
        //         },
        //         async: true,
        //         dataType: 'json',
        //         success: function(data) {
        //             var html = '';
        //             var i;
        //             for (i = 0; i < data.length; i++) {
        //                 html += '<option value=' + data[i].staffid + '>' + data[i].doctor_firstname + ' ' + data[i].doctor_lastname + '</option>';
        //             }
        //             $('.list_doctor').replaceWith('<div class="form-group list_doctor"><label for="inputState">Dokter</label><select class="form-control" id="doctor">' + html + '</select></div>');
        //         }
        //     });
        //     return false;
        // });
    });
</script>