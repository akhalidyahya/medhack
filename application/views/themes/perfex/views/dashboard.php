<?php 
	defined('BASEPATH') or exit('No direct script access allowed'); 
	$is_agree = is_user_agreed();
?>
<div class="row">
	<div class="col-md-12 section-client-dashboard">
		<h3 id="greeting" class="no-mtop"></h3>

		<div class="panel_s">
			<div class="panel-body">
				<h3 class="text-success projects-summary-heading no-mtop mbot15">Halo <?php echo get_contact_full_name(get_contact_user_id()); ?>! Selamat Datang!</h3>
				<div class="row">

				</div>
			</div>
		</div>

		<div class="panel_s">
			<div class="panel-body">
				
				<?php				
				 if(!$is_agree): ?>
					<?php echo get_option('terms_and_conditions'); ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="checkbox text-center">
									<input type="checkbox" class="capability" name="term[]" id="term" value="1">
									<label for="term">Dengan ini saya setuju dengan syarat dan ketentuan yang berlaku</label>
								</div>
							</div>
							<div class="form-group">
								<div class="text-center">
									<button type="submit" autocomplete="off" class="btn btn-info" onclick="save()"> Setuju </button>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
	<script>
		function save() {
			var term            = $('input[name="term[]"]:checked').length;
			if(term < 1) {
				swal({
					title: 'Tidak dapat melanjutkan proses',
					text: 'Anda harus menyetujui syarat dan ketentuan yang berlaku',
					icon: 'warning',
				})
				return '';
			}
			$.ajax({
                url: "<?php echo site_url('authentication/userAgree') ?>",
                type: "POST",
                dataType: "JSON",
				beforeSend: function(data) {
					swal({ text: 'Memperbarui data...', button: false, });
				},
                success: function(data) {
                    if (!data.success) {
                        swal({
                            title: 'Terjadi kesalahan',
                            text: data.message,
                            icon: 'error',
                            timer: '3000'
                        })
                    } else {
                        swal({
                            title: 'Berhasil Disimpan',
                            text: data.message,
                            icon: 'success',
                            timer: '3000'
                        }).then(()=>{
							window.location.reload();
						})
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
						title: 'Terjadi kesalahan',
						text: errorThrown,
						icon: 'error',
						timer: '3000'
					})
                }
            });
		}
	</script>