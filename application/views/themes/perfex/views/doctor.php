<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s" id="asd">
    <div class="panel-body text-left">
        <h3><i class="fa fa-stethoscope" aria-hidden="true"></i> Terapis</h3>
    </div>
    <div class="panel-body">
        <div id="reload_tabel" class="table-responsive">
            <table class="table" id="table-list">
                <thead>
                    <tr>
                        <th scope="col">Foto</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Departemen</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($doctor as $key_a => $value_a) { ?>
                        <tr>
                            <td class="text-center"><img src="<?php echo staff_profile_image_url($value_a->staffid); ?>" style="width:60px;height:60px;" /></td>
                            <td><?= $value_a->firstname . ' ' . $value_a->lastname ?></td>
                            <td></td>
                            <td><a data-toggle="modal" data-target="#doctor_modal<?= $value_a->staffid ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Lihat</a></td>

                        </tr>
                    <?php } ?>

                </tbody>
            </table>

        </div>

        <!-- Modal -->
        <?php foreach ($doctor as $key_a => $value_a) { ?>
            <div class="modal fade" id="doctor_modal<?= $value_a->staffid ?>" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Detail Terapis</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                <table style="width:100%;">
                                    <tr style="height: 30px">
                                        <td style="width:30%;">Nama</td>
                                        <td style="width:70%;"><?= $value_a->firstname . ' ' . $value_a->lastname ?></td>
                                    </tr>
                                    <tr style="height: 30px">
                                        <td>Kontak</td>
                                        <td><?= $value_a->phonenumber ?></td>
                                    </tr>
                                    <tr style="height: 30px">
                                        <td>Cabang</td>
                                        <td><?php 
                                        $array = [];
                                        foreach ($staff_branch as $key_b => $value_b) { ?>
                                                <?php foreach ($branch as $key_c => $value_c) {
                                                    if ($value_b->staff_id == $value_a->staffid) {
                                                        if ($value_b->branch_id == $value_c->id) {
                                                            $array[] = $value_c->branch;
                                                        }
                                                    }
                                                } ?>                                               
                                            <?php }
                                            echo implode(', ', $array);?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>

                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <br><br>



    </div>
</div>