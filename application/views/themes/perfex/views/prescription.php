<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s">
    <div class="panel-body text-left">
        <h3><i class="fa fa-book" aria-hidden="true"></i> Riwayat Pasien</h3>
    </div>
    <div class="panel-body">
        <div id="reload_tabel" class="table-responsive">
            <table class="table" id="table-custom">
                <thead>
                    <tr>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Pasien</th>
                        <th scope="col">Terapis</th>
                        <th scope="col">Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($riwayat as $key_a => $value_a) { ?>
                        <tr>
                            <td><?= date('d F Y', strtotime($value_a->date)); ?></td>
                            <td><?= $value_a->patient_firstname . ' ' . $value_a->patient_lastname ?></td>
                            <td><?= $value_a->doctor_firstname . ' ' . $value_a->doctor_lastname ?></td>
                            <td><button data-toggle="modal" data-target="#preModal<?= $value_a->id ?>"  type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Lihat Detail</button></td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>

        </div>

        <br><br>

        <?php foreach ($riwayat as $key_b => $value_b) { ?>
            <!-- Modal -->
            <div class="modal fade" id="preModal<?= $value_b->id ?>"  role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Riwayat Pasien</h4>
                        </div>
                        <div class="modal-body">
                            <table style="width:100%;">
                                <tr style="height: 30px">
                                    <td style="width:30%;">Tanggal & Waktu</td>
                                    <td style="width:5%;">:</td>
                                    <td style="width:70%;"><?= date('d F Y', strtotime($value_b->date)); ?></td>
                                </tr>
                                <tr style="height: 30px">
                                    <td>Pasien</td>
                                    <td>:</td>
                                    <td><?= $value_b->patient_firstname . ' ' . $value_b->patient_lastname ?></td>
                                </tr>
                                <tr style="height: 30px">
                                    <td>Terapis</td>
                                    <td>:</td>
                                    <td><?= $value_b->doctor_firstname . ' ' . $value_b->doctor_lastname ?></td>
                                </tr>
                            </table>
                            <div class="panel-body">
                                <hr>
                                <p style="font-weight: bold;">Keluhan</p>
                                <p><?= $value_b->complaint ?></p>
                                <p style="font-weight: bold;">Diagnosa</p>
                                <p><?= $value_b->case_history ?></p>
                                <hr>
                                <p style="font-weight: bold;">Tindakan</p>
                                <p><?= $value_b->action ?></p>
                                <hr>
                                <p style="font-weight: bold;">Respon</p>
                                <p><?= $value_b->response ?></p>
                                <hr>
                                <p style="font-weight: bold;">Lintah</p>
                                <p><?= $value_b->lintah ?></p>
                                <hr>
                                <p style="font-weight: bold;">Kontrol</p>
                                <p><?= $value_b->note ?></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>

                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>



    </div>
</div>