<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s">
    <div class="panel-body text-left">
        <h3><i class="fa fa-bed" aria-hidden="true"></i> Riwayat Rawat Inap</h3>
    </div>
    <div class="panel-body">
        <div id="reload_tabel" class="table-responsive">
            <table class="table" id="table-custom-bed-allotment">
                <thead>
                    <tr>
                        <th scope="col">No. Tempat Tidur</th>
                        <th scope="col">Tipe</th>
                        <th scope="col">Waktu Masuk</th>
                        <th scope="col">Waktu Keluar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($riwayat as $key_a => $value_a) { ?>
                        <tr>
                            <td><?= $value_a->bed_no ?></td>
                            <td><?= $value_a->type ?></td>
                            <td><?= date('d F Y',strtotime($value_a->waktu_masuk)) ?></td>
                            <td><?= date('d F Y',strtotime($value_a->waktu_keluar)) ?></td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>

        </div>

        <br><br>

    </div>
</div>