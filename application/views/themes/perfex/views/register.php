<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    a {
        text-decoration: underline;
    }
</style>
<div class="col-md-10 row col-md-offset-1 mbot15">
    <h1 class="text-uppercase register-heading text-center">Pendaftaran Akun</h1>
    <div class="col-md-3 mtop15">
        <?php if (!is_language_disabled()) { ?>
            <div class="form-group select-placeholder">
                <select name="language" id="language" class="form-control selectpicker" onchange="change_contact_language(this)" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-live-search="true">
                    <?php $selected = (get_contact_language() != '') ? get_contact_language() : get_option('active_language'); ?>
                    <?php foreach ($this->app->get_available_languages() as $availableLanguage) {
                    ?>
                        <option value="<?php echo $availableLanguage; ?>" <?php echo ($availableLanguage == $selected) ? 'selected' : '' ?>>
                            <?php echo ucfirst($availableLanguage); ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        <?php } ?>
    </div>
</div>
<div class="col-md-10 col-md-offset-1">
    <div class="panel_s">
        <div class="panel-body">
            <div class="form-group">
                <label>* NIK</label><br>
                <input class="form-control" type="text" name="nik" id="nik" placeholder="contoh: 01234567898765">
            </div>
            <div class="form-group">
                <label>* Nama Depan</label><br>
                <input class="form-control" type="text" name="nama_depan" id="nama_depan" placeholder="contoh: Udin">
            </div>
            <div class="form-group">
                <label>* Nama Belakang</label><br>
                <input class="form-control" type="text" name="nama_belakang" id="nama_belakang" placeholder="contoh: Samsudin">
            </div>
            <div class="form-group">
                <label>* Tempat</label><br>
                <input class="form-control" type="text" name="tempat" id="tempat" placeholder="contoh: Jakarta">
            </div>
            <?php echo render_date_input('tanggal_lahir', 'Tanggal Lahir'); ?>
            <div class="form-group">
                <label>* Alamat Email</label><br>
                <input class="form-control" type="text" name="email" id="email">
            </div>
            <div class="form-group">
                <label>* Nomor Telepon</label><br>
                <input class="form-control" type="number" name="tlp" id="tlp" placeholder="Contoh: 081234567891">
            </div>
            <div class="form-group">
                <label>* Kata Sandi</label><br>
                <input class="form-control" type="password" name="password" id="password" placeholder="Minimal 6 Karakter">
            </div>
            <div class="form-group">
                <label>* Alamat</label><br>
                <textarea class="form-control" name="alamat" id="alamat"></textarea>
            </div>
            <div class="form-group">
                <label>* Jenis Kelamin</label><br>
                <select name="gender" id="gender" class="form-control">
                    <?php foreach (app\constants\CommonConstant::GENDER_LISTS as $value => $label) : ?>
                        <option value="<?= $value ?>"> <?= $label ?> </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>* Pernah Operasi</label><br>
                <select name="pernah_operasi" id="pernah_operasi" class="form-control">
                    <option value="0">Tidak</option>
                    <option value="1">Ya</option>
                </select>
            </div>
            <div class="form-group">
                <label>Riwayat Operasi </label><small> (Bila ada)</small> <br>
                <textarea class="form-control" name="riwayat_operasi" id="riwayat_operasi"></textarea>
            </div>
            <div class="form-group">
                <label>Nama CS </label><small> (Bila ada)</small> <br>
                <input class="form-control" type="text" name="nama_cs" id="nama_cs" placeholder="contoh: John Doe">
            </div>
            <div class="form-group">
                <label for="branch_id" class="control-label">Cabang</label>
                <select id="branch_id" name="branch_id" data-width="100%" class="form-control">
                    <?php foreach (getAllBranchs() as $branch) : ?>
                        <option value="<?php echo $branch->id ?>"><?php echo $branch->branch ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <br>
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox" class="capability" name="term[]" id="term" value="1">
                    <label for="term">Dengan ini saya setuju dengan <a target="_blank" href="<?php echo base_url() ?>terms-and-conditions">syarat dan ketentuan</a> yang berlaku</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="form-group">
                <button type="submit" autocomplete="off" class="btn btn-info" onclick="saveRegister()"><?php echo _l('clients_register_string'); ?></button>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
    function saveRegister() {
        var nama_depan = $('#nama_depan').val();
        var nama_belakang = $('#nama_belakang').val();
        var email = $('#email').val();
        var tlp = $('#tlp').val();
        var password = $('#password').val();
        var alamat = $('#alamat').val();
        var term = $('input[name="term[]"]:checked').length;
        if (term < 1) {
            swal({
                title: 'Tidak dapat mendaftar',
                text: 'Anda harus menyetujui syarat dan ketentuan yang berlaku',
                icon: 'warning',
            })
            return '';
        }
        if (nama_depan == "" || nama_belakang == "" || email == "" || tlp == "" || password == "" || alamat == "") {
            info();
            return '';
        } else {
            $.ajax({
                url: "<?php echo site_url('authentication/register_new') ?>",
                type: "POST",
                data: {
                    'nama_depan': nama_depan,
                    'nama_belakang': nama_belakang,
                    'email': email,
                    'tlp': tlp,
                    'password': password,
                    'alamat': alamat,
                    'nik': $('input[name="nik"]').val(),
                    'pernah_operasi': $('select[name="pernah_operasi"]').val(),
                    'is_agree': $('input[name="term[]"]:checked').val(),
                    'tempat': $('input[name="tempat"]').val(),
                    'tanggal_lahir': $('input[name="tanggal_lahir"]').val(),
                    'gender': $('select[name="gender"]').val(),
                    'riwayat_operasi': $('#riwayat_operasi').val(),
                    'nama_cs': $('#nama_cs').val(),
                    'branch_id': $('[name="branch_id"]').val()
                },
                dataType: "JSON",
                success: function(data) {
                    if (!data.success) {
                        let text = '';
                        let no = 0;
                        console.log(data);
                        Object.keys(data.error).forEach((key) => {
                            no++;
                            text += no + '. ';
                            text += data.error[key];
                            text += '\n';
                        })
                        swal({
                            title: 'Periksa Data Kembali',
                            text: text,
                            icon: 'error',
                            // timer: '3000'
                        })
                    } else {
                        success();
                        window.location.href = "<?php echo base_url(); ?>login"
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    error();
                }
            });
        }
    }
</script>