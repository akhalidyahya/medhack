<?php

use app\constants\CommonConstant;

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    'timestamp',
    'CONCAT('.db_prefix().'contacts.firstname, \' \', '.db_prefix().'contacts.lastname) as full_name',
    'message',
    'status',
];

$sIndexColumn = 'appointment_id';
$sTable       = db_prefix() . 'appointment';
$join         = [
    'LEFT JOIN '.db_prefix().'contacts ON '.db_prefix().'contacts.id='.db_prefix().'appointment.patient_id',
];

$where        = [
    'AND patient_id =' . get_staff_user_id(),
];

if(is_admin()) {
    $where      = [];
}

$join = hooks()->apply_filters('appointment_table_sql_join', $join);

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [db_prefix().'appointment.appointment_id']);

$output  = $result['output'];
$rResult = $result['rResult'];
// echo json_encode($rResult);exit;
foreach ($rResult as $aRow) {
    $row = [];
    $row[] = $aRow['timestamp'];
    $row[] = $aRow['full_name'];
    $row[] = $aRow['message'];
    $label = 'default';
    if($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_APPROVED) {
        $label = 'success';
    } 
    else if($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_REJECT) {
        $label = 'danger';
    }
    else if($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_PENDING) {
        $label = 'warning';
    }
    $row[] = "<span class='label label-".$label." s-status'><strong>".CommonConstant::APPOINTMENT_STATUS_LIST[$aRow['status']]."</strong></span>";
    
    $button = '-';
    
    if (has_permission('appointment', '', 'edit')) {
        if($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_PENDING) {
            $button = '<a class="btn btn-success btn-icon" href="javascript:;" onclick="changeStatus('.$aRow['appointment_id'].',\''.CommonConstant::APPOINTMENT_STATUS_APPROVED.'\')"><i class="fa fa-check"></i></a>';
            $button.= ' | ';
            $button.= '<a class="btn btn-danger btn-icon" href="javascript:;" onclick="changeStatus('.$aRow['appointment_id'].',\''.CommonConstant::APPOINTMENT_STATUS_REJECT.'\')"><i class="fa fa-times"></i></a>';
        }
        else if($aRow['status'] == CommonConstant::APPOINTMENT_STATUS_APPROVED) {
            $button = '<a class="btn btn-default btn-xs" style="font-size:10px" href="javascript:;" onclick="changeStatus('.$aRow['appointment_id'].',\''.CommonConstant::APPOINTMENT_STATUS_CANCEL.'\')">Batalkan</a>';
        }
    }

    $row[] = $button;

    $output['aaData'][] = $row;
}
