<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel_s">
          <div class="panel-body">
            <h4 class="no-margin">
              <?php echo $title; ?>
            </h4>
            <hr class="hr-panel-heading" />
            <?php echo form_open($this->uri->uri_string()); ?>
            <input type="hidden" name="clientid" value="1">
            <div class="row">
              <div class="col-md-7">
                <div class="form-group">
                  <label for="branch_id" class="control-label">Cabang</label>
                  <select id="branch_id" name="branch_id" data-width="100%" class="form-control">
                    <?php foreach (getAllBranchs() as $branch) : ?>
                      <option value="<?php echo $branch->id ?>"><?php echo $branch->branch ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="form-group select-placeholder">
                  <label for="patient_id" class="control-label">Pasien/Customer</label>
                  <select name="patient_id" class="selectpicker no-margin ajax-search" data-width="100%" id="patient_id" data-none-selected-text="Klik untuk mencari Pasien/Customer" data-live-search="true">
                    <option value=""></option>
                  </select>
                </div>
              </div>
            </div>
            <div class="product_row" id="product_row_0">
              <div class="row">
                <div class="col-md-7 col-sm-7">
                  <?php echo render_select('product_items[0][product_id]', $products, ['id', 'product_name', 'quantity_number'], 'Product', !empty(set_value('product_id')) ? set_value('product_id') : $products['product_name'] ?? '', ['required' => true, 'data-header' => 'Select a Product'], [], '', 'show-tick product_id'); ?>
                </div>
                <div class="col-md-3 col-sm-3">
                  <?php echo render_input('product_items[0][qty]', 'quantity', $products->quantity_number ?? '', 'number', ['required' => true], [], '', 'quantity'); ?>
                </div>
                <div class="col-md-2 col-sm-2">
                  <label>&nbsp;</label><br />
                  <button type="button" class="btn btn-sm btn-success add_row"><i class="fa fa-plus"></i> Add</button>
                </div>
              </div>
            </div>
            <div id="append_product_row"></div>
            <button type="submit" class="btn btn-info pull-right save_button"><?php echo _l('submit'); ?></button>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<?php if (!empty($message)) { ?>
  <script type="text/javascript">
    alert_float('warning', '<?php echo $message; ?>', 6000);
  </script>
<?php } ?>
<script type="text/javascript">
  init_ajax_search('users', '#patient_id.ajax-search', undefined, admin_url + 'users/search');

  jQuery(document).ready(function() {
    "use strict";
    var product_row = "";
    init_order_required_label();
    $(".add_row").click(function(event) {
      event.preventDefault();
      var total_element = $(".product_row").length;
      var last_id = $(".product_row:last").attr('id').split("_");
      var next_id = Number(last_id[2]) + 1;
      product_row = `<div class="product_row" id="product_row_${next_id}">
        <div class="row">
        <div class="col-md-7">
        <?php echo render_select('product_items[0][product_id]', $products, ['id', 'product_name', 'quantity_number'], 'Product', !empty(set_value('product_id')) ? set_value('product_id') : $products['product_name'] ?? '', ['required' => true, 'data-header' => 'Select a Product'], [], '', 'show-tick product_id'); ?>
        </div>
        <div class="col-md-3">
        <?php echo render_input('product_items[0][qty]', 'quantity', $products->quantity_number ?? '', 'number', ['required' => true], [], '', 'quantity'); ?>
        </div>
        <div class="col-md-2 col-sm-2">
        <label>&nbsp;</label><br/>
        <button type="button" class="btn btn-sm btn-danger" onclick="remove_div(${next_id})"><i class="fa fa-times"></i></button>
        </div>
        </div>
      </div>`;
      $("#append_product_row").append(product_row);
      $(`#product_row_${next_id}`).find('select').prop('name', `product_items[${next_id}][product_id]`);
      $(`#product_row_${next_id}`).find('input[type="number"]').prop('name', `product_items[${next_id}][qty]`);
      $(`#product_row_${next_id}`).find('input[type="number"]').on('change', function() {
        if ($(this).val() < 1 || !$.isNumeric($(this).val())) {
          alert_float("danger", "Quantity Must Be Greater Than 0 ");
          $(this).val(1);
        }
      });
      init_selectpicker();
      init_order_required_label();
    });

    appValidateForm($('form'), {
      clientid: "required",
    });

    $(document).on('change', 'select.product_id', function(event) {
      var select_element = $(this);
      select_element.parents('.product_row').find('.quantity').val("").prop('readonly', false);
      $(".save_button").prop('disabled', true);
      $.post(admin_url + "products/staff_order/get_product_data", {
        product_id: select_element.val()
      }, function(data, textStatus, xhr) {
        $(".save_button").prop('disabled', false);
        data = $.parseJSON(data);
        if (val.is_digital != 1) {
          select_element.parents('.product_row').find('.quantity').attr('qty_max', data.quantity_number);
        }
      });
    });
  });

  function init_order_required_label() {
    $(':input[required]').each(function(index, el) {
      label = $(this).parents('.form-group').find('label');
      if (label.length > 0 && label.find('.req').length === 0) {
        label.prepend('<small class="req"><span class=text-danger> * </<span></small>');
      }
    });
  }
  $(document).on('change', 'input[type="number"]', function() {
    var max = $(this).attr('qty_max');
    var quantity = $(this).val();
    $(".save_button").prop('disabled', false);
    if (quantity <= 0 || !$.isNumeric(quantity)) {
      $(".save_button").prop('disabled', true);
      alert_float("danger", "Quantity Must Be Greater Than 0 ");
      return false;
    }
    if (parseInt(quantity) > parseInt(max)) {
      $(".save_button").prop('disabled', true);
      alert_float("danger", `Only ${max} Items are in stock for this Product`);
      return false;
    }
  });

  function remove_div(id) {
    $('#product_row_' + id).remove();
  }
</script>