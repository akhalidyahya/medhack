ALTER TABLE `whatsapp_chats`
	CHANGE COLUMN `body` `body` TEXT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `senderName`;
