ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `followup_time` DATETIME NULL AFTER `followup_id`,
	ADD COLUMN `closing_time` DATETIME NULL AFTER `followup_time`;
ALTER TABLE `whatsapp_dialogs`
	ADD INDEX `idx_fu_status_id` (`fu_status_id`),
	ADD INDEX `idx_followup_id` (`followup_id`);
ALTER TABLE `whatsapp_dialogs`
	ADD INDEX `idx_fu_time` (`followup_time`),
	ADD INDEX `idx_closing_time` (`closing_time`);
ALTER TABLE `whatsapp_dialogs`
	CHANGE COLUMN `name` `name` VARCHAR(100) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `dialog_id`;
ALTER TABLE `whatsapp_chats`
	CHANGE COLUMN `chatName` `chatName` VARCHAR(50) NULL COLLATE 'utf8mb4_unicode_ci' AFTER `quotedMsgType`;
