CREATE TABLE `whatsapp_dialog_staff` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`staff_id` INT(10) UNSIGNED NOT NULL,
	`dialog_id` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

ALTER TABLE `staff`
	ADD COLUMN `priority` TINYINT NULL DEFAULT NULL AFTER `google_auth_secret`;

ALTER TABLE `whatsapp_dialogs`
	ADD INDEX `index_dialog_id` (`dialog_id`);

ALTER TABLE `whatsapp_dialog_staff`
	ADD INDEX `index_staff_id` (`staff_id`),
	ADD INDEX `index_dialog_id` (`dialog_id`);

ALTER TABLE `whatsapp_chats`
	ADD INDEX `index_dialog_id` (`dialog_id`);

ALTER TABLE `whatsapp_chats`
	ADD COLUMN `caption` TEXT NULL DEFAULT NULL AFTER `type`;

ALTER TABLE `branchs`
	ADD COLUMN `city` VARCHAR(150) NULL AFTER `address`;

