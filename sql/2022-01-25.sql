ALTER TABLE `prescriptions`
	CHANGE COLUMN `keterangan_gambar` `keterangan_gambar` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci' AFTER `rekomendasi`;
ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `assignment_to` INT(11) NULL DEFAULT NULL AFTER `branch_id`;
ALTER TABLE `whatsapp_dialogs`
	ADD INDEX `idx_assignment` (`assignment_to`);
-- Patch
UPDATE whatsapp_dialogs SET assignment_to = (SELECT staff_id FROM whatsapp_dialog_staff WHERE whatsapp_dialog_staff.dialog_id = whatsapp_dialogs.dialog_id ORDER BY whatsapp_dialog_staff.id DESC LIMIT 1)

