ALTER TABLE `staff`
	CHANGE COLUMN `priority` `staff_quota` SMALLINT NULL DEFAULT NULL AFTER `google_auth_secret`,
	ADD COLUMN `priority` SMALLINT NULL AFTER `staff_quota`;
ALTER TABLE `whatsapp_dialog_staff`
	ADD COLUMN `created_at` DATETIME NULL AFTER `dialog_id`;
