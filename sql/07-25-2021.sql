ALTER TABLE `invoices`
	ADD COLUMN `patient_id` INT(11) NOT NULL AFTER `clientid`;
ALTER TABLE `invoices`
	ADD INDEX `idx_patient_id` (`patient_id`);
