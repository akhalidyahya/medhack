ALTER TABLE `invoices`
	ADD COLUMN `image_bukti` VARCHAR(200) NULL AFTER `short_link`;
ALTER TABLE `invoices`
	ADD COLUMN `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `invoices`
	ADD INDEX `idx_branch_id` (`branch_id`);