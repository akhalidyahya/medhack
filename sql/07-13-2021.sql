CREATE TABLE `bed_types` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
	`key` VARCHAR(50) NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `bed` (
	`bed_id` INT(11) NOT NULL AUTO_INCREMENT,
	`bed_number` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`type` INT(11) NULL,
	`status` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`description` TEXT NULL COLLATE 'latin1_swedish_ci',
	PRIMARY KEY (`bed_id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
AUTO_INCREMENT=2
;

CREATE TABLE `bed_allotment` (
	`bed_allotment_id` INT(11) NOT NULL AUTO_INCREMENT,
	`bed_id` INT(11) NULL,
	`patient_id` INT(11) NULL,
	`allotment_timestamp` DATETIME NULL,
	`discharge_timestamp` DATETIME NULL,
	PRIMARY KEY (`bed_allotment_id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;


