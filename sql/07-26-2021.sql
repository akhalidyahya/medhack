ALTER TABLE `product_master`
	ADD COLUMN `branch_id` INT(11) NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `product_master`
	ADD INDEX `idx_branch_id` (`branch_id`);
CREATE TABLE `bank` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`bank_name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`acc_number` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`person_name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;