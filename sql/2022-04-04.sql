ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `phone_number` VARCHAR(200) NULL DEFAULT NULL AFTER `dialog_id`,
	ADD INDEX `idx_phone_number` (`phone_number`);
