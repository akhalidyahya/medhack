CREATE INDEX `idx_doctor_id`
    ON `appointment` (`doctor_id`);
ALTER TABLE `appointment`
	ADD INDEX `idx_patient_id` (`patient_id`),
	ADD INDEX `idx_branch_id` (`branch_id`),
	ADD INDEX `idx_status` (`status`);
ALTER TABLE `bed_allotment`
	ADD INDEX `idx_bed_id` (`bed_id`),
	ADD INDEX `idx_patient_id` (`patient_id`),
	ADD INDEX `idx_allotment_time` (`allotment_timestamp`),
	ADD INDEX `idx_discharge_time` (`discharge_timestamp`);
ALTER TABLE `prescriptions`
	ADD INDEX `idx_doctor_id` (`doctor_id`),
	ADD INDEX `idx_patient_id` (`patient_id`);
ALTER TABLE `whatsapp_chats`
	ADD INDEX `idx_type` (`type`);
