ALTER TABLE `order_master`
	ADD COLUMN `patient_id` INT(11) NULL AFTER `clientid`;
ALTER TABLE `order_master`
	ADD COLUMN `branch_id` INT(11) NULL AFTER `patient_id`;
ALTER TABLE `invoices`
	ADD COLUMN `jenis_pembelian` VARCHAR(50) NULL AFTER `image_bukti`;
ALTER TABLE `order_master`
	ADD INDEX `idx_patient_id` (`patient_id`),
	ADD INDEX `idx_branch_id` (`branch_id`);
ALTER TABLE `order_master`
	ADD COLUMN `jenis_pembelian` VARCHAR(50) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `order_master`
	ADD INDEX `idx_jenis_pembelian` (`jenis_pembelian`);