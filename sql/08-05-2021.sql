ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `label` VARCHAR(50) NULL AFTER `last_time`;
ALTER TABLE `whatsapp_dialogs`
	ADD INDEX `index_label` (`label`);