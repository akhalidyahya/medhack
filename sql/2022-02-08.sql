ALTER TABLE `prescriptions`
	CHANGE COLUMN `ramuan` `ramuan` TEXT NULL COLLATE 'utf8_unicode_ci' AFTER `note`,
	CHANGE COLUMN `rekomendasi` `rekomendasi` TEXT NULL COLLATE 'utf8_unicode_ci' AFTER `ramuan`,
	CHANGE COLUMN `keterangan_gambar` `keterangan_gambar` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci' AFTER `rekomendasi`,
	ADD COLUMN `branch_id` INT NULL AFTER `keterangan_gambar`;
