ALTER TABLE `invoices`
	ADD COLUMN `shipping_status` VARCHAR(50) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `invoices`
	ADD COLUMN `shipping_receipt_number` VARCHAR(50) NULL AFTER `shipping_status`,
	ADD COLUMN `notes` TEXT NULL DEFAULT NULL AFTER `shipping_receipt_number`;
ALTER TABLE `invoices`
	ADD INDEX `idx_shipping_status` (`shipping_status`);
ALTER TABLE `invoices`
	ADD INDEX `idx_shipping_receipt_number` (`shipping_receipt_number`);