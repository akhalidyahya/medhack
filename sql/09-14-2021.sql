ALTER TABLE `appointment`
	ADD COLUMN `invoice_id` INT(11) NULL AFTER `branch_id`;
ALTER TABLE `appointment`
	ADD INDEX `idx_invoice` (`invoice_id`);
ALTER TABLE `appointment`
	ADD COLUMN `history_id` INT(11) NULL AFTER `invoice_id`,
	ADD INDEX `idx_history` (`history_id`);
ALTER TABLE `contacts`
	CHANGE COLUMN `ttl` `tempat` VARCHAR(50) NULL COLLATE 'utf8_general_ci' AFTER `nik`,
	ADD COLUMN `tanggal_lahir` DATE NULL AFTER `tempat`;

