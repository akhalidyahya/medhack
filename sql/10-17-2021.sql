ALTER TABLE `bank`
	ADD COLUMN `branch_id` INT NULL AFTER `person_name`;
ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `branch_id` INT(11) NULL AFTER `closing_id`,
	ADD INDEX `idx_branch_id` (`branch_id`);