CREATE TABLE `whatsapp_chats` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`chat_id` VARCHAR(250) NULL COLLATE 'utf8mb4_general_ci',
	`dialog_id` VARCHAR(250) NULL COLLATE 'utf8mb4_general_ci',
	`sender` VARCHAR(25) NULL COLLATE 'utf8mb4_general_ci',
	`senderName` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`body` TEXT NULL COLLATE 'utf8mb4_general_ci',
	`fromMe` TINYINT(4) NULL,
	`self` TINYINT(4) NULL,
	`isForwarded` TINYINT(4) NULL,
	`time` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`messageNumber` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`type` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`quotedMsgBody` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`quotedMsgId` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`quotedMsgType` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`chatName` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`raw` TEXT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `whatsapp_dialogs` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`dialog_id` VARCHAR(200) NULL COLLATE 'utf8mb4_general_ci',
	`name` VARCHAR(100) NULL COLLATE 'utf8mb4_general_ci',
	`image` VARCHAR(100) NULL COLLATE 'utf8mb4_general_ci',
	`metadata` TEXT NULL COLLATE 'utf8mb4_general_ci',
	`last_time` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`raw` TEXT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;