ALTER TABLE `staff_branch`
	ADD INDEX `Index _staff_id` (`staff_id`),
	ADD INDEX `Index_branch_id` (`branch_id`);

ALTER TABLE `contacts`
	ADD COLUMN `address` TEXT NULL DEFAULT NULL AFTER `last_password_change`;
