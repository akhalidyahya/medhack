ALTER TABLE `contacts`
	ADD COLUMN `is_agree` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ticket_emails`;
ALTER TABLE `contacts`
	ADD COLUMN `nik` INT(50) NULL AFTER `id`,
	ADD COLUMN `ttl` VARCHAR(50) NULL AFTER `nik`,
	ADD COLUMN `gender` VARCHAR(10) NULL AFTER `ttl`,
	ADD COLUMN `pernah_operasi` TINYINT(1) NULL DEFAULT NULL AFTER `gender`,
	ADD COLUMN `riwayat_operasi` TEXT NULL DEFAULT NULL AFTER `pernah_operasi`,
	ADD COLUMN `nama_cs` TEXT NULL DEFAULT NULL AFTER `riwayat_operasi`;
CREATE TABLE `fu_summary_status` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`status` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `fu_status_id` INT NULL DEFAULT NULL AFTER `raw`;
ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `followup_id` INT(11) NULL AFTER `fu_status_id`;

