CREATE TABLE `branchs` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`branch` VARCHAR(50) NULL DEFAULT NULL,
	`code` VARCHAR(50) NULL DEFAULT NULL,
	`address` VARCHAR(150) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)COLLATE='utf8mb4_general_ci';

CREATE TABLE `staff_branch` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`staff_id` INT UNSIGNED NULL,
	`branch_id` INT UNSIGNED NULL,
	PRIMARY KEY (`id`)
)COLLATE='utf8mb4_general_ci';

CREATE TABLE `chats` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`chat_id` VARCHAR(250) NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
	`sender` VARCHAR(25) NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
	`senderName` VARCHAR(50) NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
	`body` TEXT NULL COLLATE 'utf8mb4_general_ci',
	`fromMe` TINYINT(4) NULL,
	`self` TINYINT(4) NULL,
	`isForwarded` TINYINT(4) NULL,
	`time` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`messageNumber` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`type` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`quotedMsgBody` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`quotedMsgId` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`quotedMsgType` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`chatName` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`raw` TEXT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
