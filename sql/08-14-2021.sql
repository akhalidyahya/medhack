ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `followup_note` TEXT NULL DEFAULT NULL AFTER `label`;
