ALTER TABLE `prescriptions`
	ADD COLUMN `complaint` TEXT NULL DEFAULT NULL AFTER `patient_id`,
	CHANGE COLUMN `case_history` `case_history` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `complaint`,
	ADD COLUMN `action` TEXT NULL DEFAULT NULL AFTER `case_history`,
	ADD COLUMN `response` TEXT NULL DEFAULT NULL AFTER `action`,
	CHANGE COLUMN `medication` `medication` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `response`,
	ADD COLUMN `lintah` TEXT NULL DEFAULT NULL AFTER `medication`,
	CHANGE COLUMN `note` `note` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `lintah`;
