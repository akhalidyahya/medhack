ALTER TABLE `whatsapp_dialogs`
	ADD COLUMN `closing_id` INT NULL AFTER `closing_time`,
	ADD COLUMN `source` VARCHAR(50) NULL DEFAULT NULL AFTER `closing_id`;
