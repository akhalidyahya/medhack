<?php
require('Webhook_constant.php');

$data = json_decode(file_get_contents('php://input'), true);

foreach ($data['messages'] as $message) { // Echo every message
	$link = mysqli_connect(Webhook_constant::DB_HOST, Webhook_constant::DB_USER, Webhook_constant::DB_PASS, Webhook_constant::DB_NAME);

	// Check connection
	if ($link === false) {
		die("ERROR: Could not connect. " . mysqli_connect_error());
	}

	// Attempt insert query execution
	// $sql = "INSERT INTO whatsapp_chats (raw) VALUES ('".json_encode($message)."')";
	$sql = "INSERT INTO whatsapp_chats (chat_id,dialog_id,sender,senderName,body,fromMe,self,isForwarded,time,messageNumber,type,caption,quotedMsgBody,quotedMsgId,quotedMsgType,chatName,raw) VALUES ('" . @$message['id'] . "','" . @$message['chatId'] . "','" . @$message['author'] . "','" . @$message['senderName'] . "','" . @$message['body'] . "','" . @$message['fromMe'] . "','" . @$message['self'] . "','" . @$message['isForwarded'] . "','" . @$message['time'] . "','" . @$message['messageNumber'] . "','" . @$message['type'] . "','" . @$message['caption'] . "','" . @$message['quotedMsgBody'] . "','" . @$message['quotedMsgId'] . "','" . @$message['quotedMsgType'] . "','" . @$message['chatName'] . "','" . json_encode($message) . "')";
	if (mysqli_query($link, $sql)) {
		echo "Chats Records inserted successfully.";

		//check if dialog eixist and add it into whatsapp_dialogs table
		$sql_dialog_exist = "SELECT id FROM whatsapp_dialogs WHERE dialog_id = '" . $message['chatId'] . "'";
		$exec_dialog_exist = mysqli_query($link, $sql_dialog_exist);
		$dialog_exist = mysqli_fetch_assoc($exec_dialog_exist);

		if (empty($dialog_exist)) {
			//add new dialog in whatsapp_dialogs table
			if (stripos(@$message['chatId'], 'g.us') === false) {
				$chatName = !empty(@$message['chatName']) ? @$message['chatName'] : @$message['senderName'];
			} else {
				$chatName = @$message['chatName'];
			}
			$sql_insert_dialog = "INSERT INTO whatsapp_dialogs VALUES('','" . @$message['chatId'] . "','" . @$chatName . "','" . Webhook_constant::BASE_URL . "assets/images/user-placeholder.jpg" . "','','" . @$message['time'] . "','LEAD','','" . json_encode($message) . "','','','','','','')";
			$exec_insert_dialog = mysqli_query($link, $sql_insert_dialog);
		} else {
			//update last time in whatsapp_dialogs table
			$sql_update_dialog = "UPDATE whatsapp_dialogs SET last_time = '" . @$message['time'] . "' WHERE dialog_id = '" . @$message['chatId'] . "'";
			$exec_update_dialog = mysqli_query($link, $sql_update_dialog);
		}

		//cek apakah chat grup atau personal. Kalau personal maka di assiggn ke staff
		if (stripos(@$message['chatId'], 'g.us') === false) {
			//get staff id berdasarkan beban
			$sql_staff_dialog = "SELECT staff_id, COUNT(staff_id) as jumlah FROM whatsapp_dialog_staff GROUP BY staff_id WHERE DATE(created_at) = '" . Date('Y-m-d') . "' ORDER BY jumlah ASC";
			$res = mysqli_query($link, $sql_staff_dialog);
			$staffDialog = mysqli_fetch_assoc($res);
			if (!empty($staffDialog)) {
				$sql_count_assigned_staff_ids = "SELECT COUNT(DISTINCT staff_id) AS jumlah FROM whatsapp_dialog_staff";
				$res_count_assigned_staff_ids = mysqli_query($link, $sql_count_assigned_staff_ids);
				$count_assigned_staff_ids = mysqli_fetch_assoc($res_count_assigned_staff_ids)['jumlah'];

				$sql_count_staff = "SELECT COUNT(*) AS jumlah FROM staff WHERE role = 3";
				$res_count_staff = mysqli_query($link, $sql_count_staff);
				$count_staff = mysqli_fetch_assoc($res_count_staff)['jumlah'];
				if ($count_assigned_staff_ids < $count_staff) {
					echo 'Get new staff id';
					//select staff id from whatsapp_dialogs
					$sql_assigned_staff_ids = "SELECT DISTINCT staff_id FROM whatsapp_dialog_staff";
					$res_assigned_staff_ids = mysqli_query($link, $sql_assigned_staff_ids);
					$ids = [];
					while ($assigned_staff_ids = mysqli_fetch_assoc($res_assigned_staff_ids)) {
						$ids[] = $assigned_staff_ids['staff_id'];
					}
					//convert to string. ex: 1,2,3,4
					$ids_string = implode(',', $ids);

					//select staff cs exclude existing staff
					$sql_select_staff = "SELECT staffid from staff WHERE role = 3 AND staffid NOT IN ($ids_string) ORDER BY priority ASC";
					$res_select_staff = mysqli_query($link, $sql_select_staff);
					$select_staff = mysqli_fetch_assoc($res_select_staff)['staffid'];
					$lowest = $select_staff;
				} else {
					$lowest = $staffDialog['staff_id'];
					$res = mysqli_query($link, $sql_staff_dialog);
					$ids = [];
					//memilih yang belum melewati kuota dan berdasarkan prioritas bila telah melewati kuota
					while ($staffDialogLocal = mysqli_fetch_assoc($res)) {
						$id = $staffDialogLocal['staff_id'];
						$sql_select_staff = "SELECT staffid,staff_quota from staff WHERE role = 3 AND staffid = $id ORDER BY priority ASC";
						$res_select_staff = mysqli_query($link, $sql_select_staff);
						$selected_staff = mysqli_fetch_assoc($res_select_staff);
						//validasi apakah sudah melewati kuota atau belum, kalau belum maka diisi
						if ($staffDialogLocal['jumlah'] <= $selected_staff['staff_quota']) {
							$lowest = $staffDialog['staff_id'];
						}
						//validasi apakah telah melewati batas dan prioritas, kalau iya maka tetap terisi.
						elseif ($staffDialogLocal['jumlah'] > $selected_staff['staff_quota'] && $selected_staff['priority'] == 1) {
							$lowest = $staffDialog['staff_id'];
							break;
						}
					}
				}
			} else {
				$lowest = mysqli_fetch_assoc(mysqli_query($link, "SELECT staffid FROM staff WHERE role = 3 ORDER BY priority ASC"))['staffid'];
			}

			//cek apakah sudah di assign atau belum
			$sql_check_staff_dialog_exists 		= "SELECT dialog_id FROM whatsapp_dialog_staff WHERE dialog_id = '" . @$message['chatId'] . "'";
			$exec_check_staff_dialog_exists		= mysqli_query($link, $sql_check_staff_dialog_exists);
			$result_check_staff_dialog_exists	= mysqli_fetch_assoc($exec_check_staff_dialog_exists);

			if (empty($result_check_staff_dialog_exists) && stripos(@$message['chatId'], 'g.us') === false) {
				$sql_insert = "INSERT 
							INTO whatsapp_dialog_staff (staff_id,dialog_id) 
							VALUES ($lowest,'" . $message['chatId'] . "','" . Date('Y-m-d H:i:s') . "')";
				$result		= mysqli_query($link, $sql_insert);

				$sql_update = "UPDATE whatsapp_dialogs
							SET assignment_to = $lowest
							WHERE dialog_id = '".$message['chatId']."'";
				$result2		= mysqli_query($link, $sql_update);

				if ($result) {
					echo "Assigning FU to CS successfull";
				}
				if ($result2) {
					echo "Assigning to CS successfull";
				}
			}
		}
	} else {
		echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
	}

	// Close connection
	mysqli_close($link);
}
